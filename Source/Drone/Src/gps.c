#include "gps.h"

#define GPS_FIX_OK 1
#define NO_GPS_LOCK 0
#define PI 3.1415926535897932384626433832795

extern UART_HandleTypeDef huart2;

uint8_t *gpsData; 
uint8_t initiate = 0, initiated = 0, dataAge = 0;
double initialEcef[3] = {0.0, 0.0, 0.0};
double cosLon = 0.0, cosLat = 0.0, sinLon = 0.0, sinLat = 0.0;
GPS_StatusDef currentStatus = GPS_LOCK_NOT_OK;

void gpsInit(){
	initiate = 1;
	
	if(!gpsIsInitiated()){
		gpsData = (uint8_t *)malloc(60);
		
		//set UBX in/out and 115200 baudrate
		uint8_t setUBXout[] = {0xB5, 0x62, 0x06, 0x00, 0x14, 0x00, 0x01, 0x00, 0x00, 0x00, 0xD0, 0x08, 0x00, 0x00,
													 0x00, 0xC2, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xB8, 0x42};
		HAL_UART_Transmit(&huart2, &setUBXout[0], sizeof(setUBXout), 100);
		
		HAL_UART_DeInit(&huart2);
		huart2.Init.BaudRate = 115200;
		HAL_UART_Init(&huart2);
		
		HAL_Delay(100);
															 
		//set NAV-SOL message out
		uint8_t setNavSol[] = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0x01, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x17, 0xDA};
		HAL_UART_Transmit(&huart2, &setNavSol[0], sizeof(setNavSol), 100);
		HAL_Delay(100);
		
		//set dynamic model to "Airbourne < 1g" and Accuracy mask = 7m
		uint8_t setModel[] = {0xB5, 0x62, 0x06, 0x24, 0x24, 0x00, 0xFF, 0xFF, 0x06, 0x03, 0x00, 0x00, 0x00, 0x00, 0x10, 0x27,
													0x00, 0x00, 0x05, 0x00, 0xFA, 0x00, 0xFA, 0x00, 0x0A, 0x00, 0x2C, 0x01, 0x00, 0x00, 0x00, 0x00,
													0x10, 0x27, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF3, 0x87};
		HAL_UART_Transmit(&huart2, &setModel[0], sizeof(setModel), 100);
		HAL_Delay(100);
		
		//set gps outputrate to 11.11 Hz (90ms)
		uint8_t gpsOutputRate[] = {0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0x5A, 0x00, 0x01, 0x00, 0x01, 0x00, 0x70, 0xD6};
		HAL_UART_Transmit(&huart2, &gpsOutputRate[0], sizeof(gpsOutputRate), 100);
		
		initiated = 1;
	}
}

uint8_t gpsIsInitiated(){
	return initiated;
}

void gpsUpdateData(uint8_t *data){
	uint8_t CKA = data[58];
	uint8_t CKB = data[59];
	uint8_t crcA = 0;
	uint8_t crcB = 0;
	uint8_t crcOK = 0;
	
	for (int i = 2; i < 58; i++){
		crcA += data[i];
		crcB += crcA;
	}
	
	if((crcA == CKA) && (crcB == CKB)){
		crcOK = 1;
		dataAge = 0;
		
		uint8_t flags = data[17];
		if (((flags & GPS_FIX_OK) == GPS_FIX_OK)){
			currentStatus = GPS_LOCK_OK;
		}
		else{
			currentStatus = DATA_INACURATE;
		}

		uint8_t gpsFix = data[16];
		if ((gpsFix == NO_GPS_LOCK) || (dataAge > 5)){
			currentStatus = GPS_LOCK_NOT_OK;
		}
	}
	else{
		dataAge++;
		currentStatus = OLDER_DATA;
	}
	
	if((currentStatus != GPS_LOCK_NOT_OK) && crcOK){
		memcpy(gpsData, data, 60);
	}
}

GPS_StatusDef gpsGetPos(double *px, double *py, double *pz){
	double ecef[3] = {0.0, 0.0, 0.0};
	
	if (currentStatus != GPS_LOCK_NOT_OK){
		HAL_NVIC_DisableIRQ(USART2_IRQn);
		ecef[0] = ((gpsData[21] << 24) | (gpsData[20] << 16) | (gpsData[19] << 8) | gpsData[18])/100.0;
		ecef[1] = ((gpsData[25] << 24) | (gpsData[24] << 16) | (gpsData[23] << 8) | gpsData[22])/100.0;
		ecef[2] = ((gpsData[29] << 24) | (gpsData[28] << 16) | (gpsData[27] << 8) | gpsData[26])/100.0;
		HAL_NVIC_EnableIRQ(USART2_IRQn);
		
		if ((initiate) && (currentStatus == GPS_LOCK_OK)){
			initialEcef[0] = ecef[0];
			initialEcef[1] = ecef[1];
			initialEcef[2] = ecef[2];
			
			double latitude = 0.0, longitude = 0.0;
			ecefToLLA(&latitude, &longitude, initialEcef[0], initialEcef[1], initialEcef[2]);
			sinLon = sin(longitude);
			sinLat = sin(latitude);
			cosLon = cos(longitude);
			cosLat = cos(latitude);
			
			initiate = 0;
		}
		
		ecef[0] = ecef[0] - initialEcef[0];
		ecef[1] = ecef[1] - initialEcef[1];
		ecef[2] = ecef[2] - initialEcef[2];
		ecefToENU(px, py, pz, ecef);
	}
	else{
		*px = 0;
		*py = 0;
		*pz = 0;
	}
	return currentStatus;
}

GPS_StatusDef gpsGetVel(double *vx, double *vy, double *vz){
	double ecef[3] = {0.0, 0.0, 0.0};
	
	HAL_NVIC_DisableIRQ(USART2_IRQn);
	ecef[0] = ((gpsData[37] << 24) | (gpsData[36] << 16) | (gpsData[35] << 8) | gpsData[34])/100.0;
	ecef[1] = ((gpsData[41] << 24) | (gpsData[40] << 16) | (gpsData[39] << 8) | gpsData[38])/100.0;
	ecef[2] = ((gpsData[45] << 24) | (gpsData[44] << 16) | (gpsData[43] << 8) | gpsData[42])/100.0;
	HAL_NVIC_EnableIRQ(USART2_IRQn);
	
	ecefToENU(vx, vy, vz, ecef);
	
	return currentStatus;
}

GPS_StatusDef gpsGetAccVel(double *vAcc){
	HAL_NVIC_DisableIRQ(USART2_IRQn);
	*vAcc = ((gpsData[49] << 24) | (gpsData[48] << 16) | (gpsData[47] << 8) | gpsData[46])/100.0;
	HAL_NVIC_EnableIRQ(USART2_IRQn);
	return currentStatus;
}

GPS_StatusDef gpsGetAccPos(double *pAcc){
	HAL_NVIC_DisableIRQ(USART2_IRQn);
	*pAcc = ((gpsData[33] << 24) | (gpsData[32] << 16) | (gpsData[31] << 8) | gpsData[30])/100.0;
	HAL_NVIC_EnableIRQ(USART2_IRQn);
	return currentStatus;
}

GPS_StatusDef gpsGetNumSat(uint8_t *numSat){
	HAL_NVIC_DisableIRQ(USART2_IRQn);
	*numSat = gpsData[53];
	HAL_NVIC_EnableIRQ(USART2_IRQn);	
	return currentStatus;
}

GPS_StatusDef gpsGetDOP(uint8_t *dop){
	HAL_NVIC_DisableIRQ(USART2_IRQn);
	*dop = ((gpsData[51] << 8) | gpsData[50])/100.0;
	HAL_NVIC_EnableIRQ(USART2_IRQn);	
	return currentStatus;
}

void ecefToLLA(double *latitude, double *longitude, double x_e, double y_e, double z_e){
	double r_e = 6378137;
	double r_p = 6356752.3142;
	double e = 8.1819190842622e-2;
	double lat = 0, prevLat = -1;
	double h = 0;
	double N = 0;
	
	double p = sqrt(pow(x_e, 2.0) + pow(y_e, 2.0));
	*longitude = atan2(y_e,x_e);
	lat = atan(z_e/(p*(1 - pow(e,2.0))));
	
	while(prevLat != lat){
		prevLat = lat;
		N = pow(r_e, 2.0)/sqrt(pow(r_e*cos(lat), 2.0) + pow(r_p*sin(lat), 2.0));
		h = p/cos(lat) - N;
		lat = atan(z_e/(p*(1 - pow(e, 2.0)*N/(N + h))));
	}
	*latitude = lat;
}

void ecefToENU(double *enuX, double *enuY, double *enuZ, double ecef[]){
	*enuX = -sinLon*ecef[0] + cosLon*ecef[1];
	*enuY = -cosLon*sinLat*ecef[0] - sinLon*sinLat*ecef[1] + cosLat*ecef[2];
	*enuZ = cosLon*cosLat*ecef[0] + sinLon*cosLat*ecef[1] + sinLat*ecef[2];
}
