#include "stm32f4xx_hal.h"
#include "PPMcontrol.h"
#include "sensors.h"
#include "flightController.h"
#include "pixy.h"
#include "gps.h"
#include "LED.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "FreeRTOS_CLI.h"

#ifndef __myTasks_H
#define __myTasks_H

typedef enum 
{
	IDLE,
	IDLE_GPS_LOCK_OK,
	ARMED_MANUAL_MODE,
	ARMED_GPS_MODE,
	ARMED_AUTONOMOUS_MODE,
	LOW_BAT,
	LOW_LOW_BAT,
	CAL_COMPASS_Z,
	CAL_COMPASS_XY,
	CAL_COMPASS_OK,
	CAL_REMOTE
} Drone_StatusDef;

struct console
{
	uint8_t printGpsStatus;
	uint8_t startFlightPlan;
	uint8_t isManual;
	double prop1;
	double prop2;
	double prop3;
	double prop4;
};

void initRTOS(void);
void hwInitTask(void *pvParameters);
void fliCtrlTask(void *pvParameters);
void txDebugTask(void *pvParameters);
void rxDebugTask(void *pvParameters);
void batVoltTask(void *pvParameters);
void ledCtrlTask(void *pvParameters);
void altTask(void *pvParameters);

#endif
