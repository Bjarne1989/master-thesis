
#ifndef __PPMcontrol_H
#define __PPMcontrol_H

#include "stm32f4xx_hal.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

typedef enum 
{
  PROP_FL,
  PROP_FR,
  PROP_RL,
  PROP_RR,
	PPM_SPARE1,
} PPM_OutDef;

typedef enum 
{
  RC_ROLL,
  RC_PITCH,
  RC_THROTTLE,
  RC_YAW,
	RC_AUX1,
	RC_AUX2,
	SPARE1,
	SPARE2
} PPM_inDef;

struct PPM_in {
	double roll;
	double pitch;
	double throttle;
	double yaw;
	double aux1;
	double aux2;
//	double spare1;
//	double spare2;
};


HAL_StatusTypeDef init_PPM_out(TIM_HandleTypeDef *t1, TIM_HandleTypeDef *t2, TIM_HandleTypeDef *t3);
void calPPMin(void);
void set_PPM_out(float power, PPM_OutDef ppmOut);
void update_PPM_in_timer(PPM_inDef input);
uint8_t get_PPM_in(struct PPM_in *inputs);

#endif
