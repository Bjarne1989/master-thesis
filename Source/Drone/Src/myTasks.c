#include "myTasks.h"

extern I2C_HandleTypeDef hi2c1;
extern SPI_HandleTypeDef hspi1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim5;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

extern uint8_t rxBuffer;
extern uint8_t gpsBuffer[60];
extern char rxString[100];

extern void vRegisterCLICommands(void);
extern double getBatVolt(void);
extern double getIR(void);

struct sensor_MotionDef sensors;

RT_MODEL_flightController_T *flightCtrl;

//Semphores
extern SemaphoreHandle_t xRxSem;
SemaphoreHandle_t xGetAlt;

//Mutexes
SemaphoreHandle_t xParamMutex;
SemaphoreHandle_t xGetBatMutex;
SemaphoreHandle_t xI2CMutex;

//Queues
QueueHandle_t xManualQueue;
QueueHandle_t xPrintfQueue;
QueueHandle_t xlogQueue;
QueueHandle_t xLEDQueue;
QueueHandle_t xAltitude;
QueueHandle_t xFlightPlanQueue;

void initRTOS()
{
	
	xParamMutex = xSemaphoreCreateMutex();
	xGetBatMutex = xSemaphoreCreateMutex();
	xI2CMutex = xSemaphoreCreateMutex();
	
	xRxSem = xSemaphoreCreateCounting(5, 0);
	xGetAlt = xSemaphoreCreateCounting(3, 0);
	
	xManualQueue = xQueueCreate(1, sizeof(struct console));
	xlogQueue = xQueueCreate(1, sizeof(uint8_t));
	xPrintfQueue = xQueueCreate(5, sizeof(char[300]));
	xLEDQueue = xQueueCreate(5, sizeof(Drone_StatusDef));
	xAltitude = xQueueCreate(1, sizeof(double));
	xFlightPlanQueue = xQueueCreate(20, sizeof(double[3]));
	
	xTaskCreate(hwInitTask, "hwInit", 350, (void*)NULL, 31, NULL);
	xTaskCreate(fliCtrlTask, "fliCtrl", 750, (void*)NULL, 30, NULL);
	xTaskCreate(txDebugTask, "txDebug", 200, (void*)NULL, 9, NULL);
	xTaskCreate(rxDebugTask, "rxDebug", 400, (void*)NULL, 10, NULL);
	xTaskCreate(batVoltTask, "batVolt", configMINIMAL_STACK_SIZE, (void*)NULL, 8, NULL);
	xTaskCreate(ledCtrlTask, "ledCtrl", configMINIMAL_STACK_SIZE, (void*)NULL, 0, NULL);
	xTaskCreate(altTask, "altTask", 300, (void*)NULL, 29, NULL);
	
	printf("Drone project powered by FreeRTOS+CLI, by Bjarne Kv�stad\n");
	
	vRegisterCLICommands();
	vTaskStartScheduler();
	
	printf("FAILED\n");
	while(1){
	}
}

void hwInitTask(void *pvParameters)
{
	printf("Starting");
	for(int i = 0; i < 4; i++){
		ledSetLED(LED_FL | LED_FR | LED_RL | LED_RR , LED_RED);
		printf(". ");
		HAL_Delay(250);
		ledResetAll();
		ledSetLED(LED_FL | LED_FR | LED_RL | LED_RR , LED_GREEN);
		printf(". ");
		HAL_Delay(250);
		ledResetAll();
		ledSetLED(LED_FL | LED_FR | LED_RL | LED_RR , LED_BLUE);
		printf(". ");
		HAL_Delay(250);
		ledResetAll();
	}
	printf("\n");
	
	gpsInit();
	
	flightCtrl = flightController();
	
	// ### Angle position controller PID parameters ###
	//Pitch PID
	flightCtrl->ModelData.inputs->PID_Phi[0] = 7.5;
	flightCtrl->ModelData.inputs->PID_Phi[1] = 0;
	flightCtrl->ModelData.inputs->PID_Phi[2] = 0;
	
	//Roll PID
	flightCtrl->ModelData.inputs->PID_Phi[3] = flightCtrl->ModelData.inputs->PID_Phi[0];
	flightCtrl->ModelData.inputs->PID_Phi[4] = flightCtrl->ModelData.inputs->PID_Phi[1];
	flightCtrl->ModelData.inputs->PID_Phi[5] = flightCtrl->ModelData.inputs->PID_Phi[2];
	
	//Yaw PID
	flightCtrl->ModelData.inputs->PID_Phi[6] = 4;
	flightCtrl->ModelData.inputs->PID_Phi[7] = 0;
	flightCtrl->ModelData.inputs->PID_Phi[8] = 0;
	
	// ### Anglular velocity controller PID parameters ###
	//Pitch PID
	flightCtrl->ModelData.inputs->PID_DPhi[0] = 12;
	flightCtrl->ModelData.inputs->PID_DPhi[1] = 1;
	flightCtrl->ModelData.inputs->PID_DPhi[2] = 2.3;
	
	//Roll PID
	flightCtrl->ModelData.inputs->PID_DPhi[3] = flightCtrl->ModelData.inputs->PID_DPhi[0];
	flightCtrl->ModelData.inputs->PID_DPhi[4] = flightCtrl->ModelData.inputs->PID_DPhi[1];
	flightCtrl->ModelData.inputs->PID_DPhi[5] = flightCtrl->ModelData.inputs->PID_DPhi[2];
	
	//Yaw PID
	flightCtrl->ModelData.inputs->PID_DPhi[6] = 55;
	flightCtrl->ModelData.inputs->PID_DPhi[7] = 0.5;
	flightCtrl->ModelData.inputs->PID_DPhi[8] = 0.1;
	
	// ### Linear position controller PID parameters ###
	//x PID
	flightCtrl->ModelData.inputs->PID_r[0] = 0.45;
	flightCtrl->ModelData.inputs->PID_r[1] = 0;
	flightCtrl->ModelData.inputs->PID_r[2] = 0;
	
	//y PID
	flightCtrl->ModelData.inputs->PID_r[3] = flightCtrl->ModelData.inputs->PID_r[0];
	flightCtrl->ModelData.inputs->PID_r[4] = flightCtrl->ModelData.inputs->PID_r[1];
	flightCtrl->ModelData.inputs->PID_r[5] = flightCtrl->ModelData.inputs->PID_r[2];
	
	//z PID
	flightCtrl->ModelData.inputs->PID_r[6] = 0.28;
	flightCtrl->ModelData.inputs->PID_r[7] = 0;
	flightCtrl->ModelData.inputs->PID_r[8] = 0;
	
	// ### Linear velocity controller PID parameters ###
	//x PID
	flightCtrl->ModelData.inputs->PID_v[0] = 0.06;
	flightCtrl->ModelData.inputs->PID_v[1] = 0.2;
	flightCtrl->ModelData.inputs->PID_v[2] = 0.025;
	
	//y PID
	flightCtrl->ModelData.inputs->PID_v[3] = flightCtrl->ModelData.inputs->PID_v[0];
	flightCtrl->ModelData.inputs->PID_v[4] = flightCtrl->ModelData.inputs->PID_v[1];
	flightCtrl->ModelData.inputs->PID_v[5] = flightCtrl->ModelData.inputs->PID_v[2];
	
	//z PID
	flightCtrl->ModelData.inputs->PID_v[6] = 58;
	flightCtrl->ModelData.inputs->PID_v[7] = 6;
	flightCtrl->ModelData.inputs->PID_v[8] = 2;
	
	flightController_initialize(flightCtrl);
	if (strcmp(flightCtrl->errorStatus, "ERR") == 0) printf("flightController memory allocation ___FAILED___\n");
	
	if (HAL_TIM_OC_Start(&htim3, TIM_CHANNEL_1) != HAL_OK) printf("Timer ___FAILED____\n");
	if(init_PPM_out(&htim2, &htim5, &htim3) != HAL_OK) printf("PMM out ___FAILED____\n");
	
	uint8_t status = init_sensors(&hi2c1);
	if (status == 1){
		printf("Sensor selftest OK\n");
	}
	else if (status == 0){
		printf("Sensor selftest ___FAILED___\n");
	}
	else if (status == HAL_TIMEOUT){
		printf("I2C lock\n");
		while(1){}
	}
	
	__HAL_UART_FLUSH_DRREGISTER(&huart2);
	if(HAL_UART_Receive_IT(&huart2, &gpsBuffer[0], 60) != HAL_OK) printf("GPS Receive interrupt ___FAILED___\n");
	
	__HAL_UART_FLUSH_DRREGISTER(&huart1);
	if (HAL_UART_Receive_IT(&huart1, &rxBuffer, 1) != HAL_OK) printf("Receive interrupt ___FAILED___\n");
	
	vTaskDelete(NULL);
}

void fliCtrlTask(void *pvParameters)
{
	printf("Starting Flight Controller Task...\n");
	
	const TickType_t xPeriod = 15 / portTICK_PERIOD_MS;
	TickType_t xLastWakeTime = xTaskGetTickCount() / portTICK_PERIOD_MS;
	uint32_t timer = 0, logging = 0, logTimer = 0;
	char *log = pvPortMalloc(200);
	struct console fromConsole;
	struct block object;
	struct PPM_in rcInputs;
	fromConsole.isManual = 0;
	fromConsole.printGpsStatus = 0;	
	fromConsole.startFlightPlan = 0;
	
	uint8_t flightMode = 0;
	uint8_t sampleCounter = 0;
	
	Drone_StatusDef droneStatus = IDLE;
	xQueueSend(xLEDQueue, &droneStatus, 0);

	flightCtrl->ModelData.inputs->D_orDataProcessing = 0;
	
	while(1){
		vTaskDelayUntil(&xLastWakeTime, xPeriod);
		
		xQueueReceive(xManualQueue, &fromConsole, 0); //receive parameters from rxTask
		if (xQueueReceive(xlogQueue, &logging, 0) == pdPASS){ //receive logging paramters from rxTask
			logTimer = HAL_GetTick();
		}
		
		uint8_t RCstatus = get_PPM_in(&rcInputs);
		if((rcInputs.throttle < 50) && (rcInputs.pitch > 450) && (rcInputs.roll < -450) && (rcInputs.yaw > 450)){
			if ((HAL_GetTick() - timer > 1000)){
				flightCtrl->ModelData.inputs->armed = !flightCtrl->ModelData.inputs->armed;
				if (flightCtrl->ModelData.inputs->armed){
					droneStatus = ARMED_MANUAL_MODE;
					xQueueSend(xLEDQueue, &droneStatus, 0);
					logTimer = HAL_GetTick();
				}
				else{
					droneStatus = IDLE;
					xQueueSend(xLEDQueue, &droneStatus, 0);
				}
				timer = HAL_GetTick();
			}
		}
		else{
			timer = HAL_GetTick();
		}
		
		if(flightCtrl->ModelData.inputs->armed && RCstatus){
			if ((rcInputs.aux1 > 1800) && (rcInputs.aux2 > 1800) && gpsIsInitiated() && (flightMode != 2)){
				Drone_StatusDef status = ARMED_AUTONOMOUS_MODE;
				xQueueSend(xLEDQueue, &status, 0);
				flightMode = 2;
				gpsInit();
			}
			else if((rcInputs.aux1 > 1800) && (rcInputs.aux2 < 1800) && gpsIsInitiated() && (flightMode != 1)){
				Drone_StatusDef status = ARMED_GPS_MODE;
				xQueueSend(xLEDQueue, &status, 0);
				flightMode = 1;
			}
			else if((rcInputs.aux1 < 1800) && (rcInputs.aux2 < 1800) && (flightMode != 0)){
				Drone_StatusDef status = ARMED_MANUAL_MODE;
				xQueueSend(xLEDQueue, &status, 0);
				flightMode = 0;
			}
		}
		
		flightCtrl->ModelData.inputs->mode = flightMode;
		
		flightCtrl->ModelData.inputs->RCok = RCstatus;
		flightCtrl->ModelData.inputs->RCctrl[0] = rcInputs.pitch;
		flightCtrl->ModelData.inputs->RCctrl[1] = rcInputs.roll;
		flightCtrl->ModelData.inputs->RCctrl[2] = rcInputs.yaw;
		flightCtrl->ModelData.inputs->RCctrl[3] = rcInputs.throttle;
		
		getMotion(&sensors);
		for (int i = 0; i < 3; i++){
			flightCtrl->ModelData.inputs->acc[i] = sensors.acc[i];
			flightCtrl->ModelData.inputs->com[i] = sensors.com[i];
			flightCtrl->ModelData.inputs->gyr[i] = sensors.gyr[i];
		}
		
		if (sampleCounter == 0){
			double vx, vy, vz, px, py, pz, altitude;
			gpsGetPos(&px, &py, &pz);
			gpsGetVel(&vx, &vy, &vz);
			
			xQueueReceive(xAltitude, &altitude, 0);

			flightCtrl->ModelData.inputs->mode = flightMode;
			flightCtrl->ModelData.inputs->z_r[0] = px;
			flightCtrl->ModelData.inputs->z_r[1] = py;
			flightCtrl->ModelData.inputs->z_r[2] = altitude;
			flightCtrl->ModelData.inputs->z_v[0] = vx;
			flightCtrl->ModelData.inputs->z_v[1] = vy;
			flightCtrl->ModelData.inputs->z_v[2] = vz;
			flightCtrl->ModelData.inputs->z_IR = getIR();
		}
		else if (sampleCounter == 3){
			xSemaphoreGive(xGetAlt);
		}
		
		if(flightMode == 2 && sampleCounter == 0){
			if (RCstatus == 0){
				flightMode = 1;
			}
			
			if(flightCtrl->ModelData.outputs->landed){
				flightCtrl->ModelData.inputs->armed = 0;
			}
			else if(flightCtrl->ModelData.outputs->spReached || fromConsole.startFlightPlan){
				fromConsole.startFlightPlan = 0;
				double r_SP[3];
				if (xQueueReceive(xFlightPlanQueue, &r_SP, 0) == pdPASS){
					flightCtrl->ModelData.inputs->r_SP[0] = r_SP[0];
					flightCtrl->ModelData.inputs->r_SP[1] = r_SP[1];
					flightCtrl->ModelData.inputs->r_SP[2] = r_SP[2];
				}
			}
		}
		else if(flightMode == 3 && sampleCounter == 0){
//			uint16_t objectDetected = getObject(&object);
//			flightCtrl->ModelData.inputs->obj_xy[0] = object.x;
//			flightCtrl->ModelData.inputs->obj_xy[1] = object.y;
//			flightCtrl->ModelData.inputs->obj_det = objectDetected;
//			flightCtrl->ModelData.inputs->obj_len = object.height;
//			flightCtrl->ModelData.inputs->obj_wid = object.width;
//			flightCtrl->ModelData.inputs->obj_ang = object.angle;
//			flightCtrl->ModelData.inputs->obj_SP[0] = 0;
//			flightCtrl->ModelData.inputs->obj_SP[1] = 0;
//			flightCtrl->ModelData.inputs->obj_SP[2] = 1;
			
			flightCtrl->ModelData.inputs->r_SP[0] = 0;
			flightCtrl->ModelData.inputs->r_SP[1] = 0;
			flightCtrl->ModelData.inputs->r_SP[2] = 0;
		}
		
		if(fromConsole.isManual == 0){
			
			xSemaphoreTake(xParamMutex, 10/portTICK_PERIOD_MS);
			flightController_step(flightCtrl);
			xSemaphoreGive(xParamMutex);
			
			set_PPM_out(flightCtrl->ModelData.outputs->FL_prop, PROP_FL);
			set_PPM_out(flightCtrl->ModelData.outputs->FR_prop, PROP_FR);
			set_PPM_out(flightCtrl->ModelData.outputs->RL_prop, PROP_RL);
			set_PPM_out(flightCtrl->ModelData.outputs->RR_prop, PROP_RR);

			
			if (logging && flightCtrl->ModelData.inputs->armed){
				
				sprintf(log, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f%c", // 
				flightCtrl->ModelData.outputs->x_hat_Phi_log[0],
				flightCtrl->ModelData.outputs->Phi_SP_log[0],
				flightCtrl->ModelData.outputs->x_hat_Phi_log[1],
				flightCtrl->ModelData.outputs->Phi_SP_log[1],
				flightCtrl->ModelData.outputs->x_hat_Phi_log[2],
				flightCtrl->ModelData.outputs->Phi_SP_log[2],
				flightCtrl->ModelData.outputs->x_hat_DPhi_log[0],
				flightCtrl->ModelData.outputs->DPhi_SP_log[0],
				flightCtrl->ModelData.outputs->x_hat_DPhi_log[1], 
				flightCtrl->ModelData.outputs->DPhi_SP_log[1],
				flightCtrl->ModelData.outputs->x_hat_DPhi_log[2],
				flightCtrl->ModelData.outputs->DPhi_SP_log[2],
				flightCtrl->ModelData.outputs->u_p[0],
				flightCtrl->ModelData.outputs->u_p[1],
				flightCtrl->ModelData.outputs->u_p[2],
				flightCtrl->ModelData.outputs->u_i[0],
				flightCtrl->ModelData.outputs->u_i[1],
				flightCtrl->ModelData.outputs->u_i[2],
				flightCtrl->ModelData.outputs->u_d[0],
				flightCtrl->ModelData.outputs->u_d[1],
				flightCtrl->ModelData.outputs->u_d[2],
				(double)(HAL_GetTick() - logTimer)/1000.0, 
				'\0');
				/*sprintf(log, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f%c",
				flightCtrl->ModelData.outputs->x_hat_r_log[0], 
				flightCtrl->ModelData.outputs->r_SP_log[0], 
				flightCtrl->ModelData.outputs->x_hat_r_log[1], 
				flightCtrl->ModelData.outputs->r_SP_log[1], 
				flightCtrl->ModelData.outputs->x_hat_r_log[2],
				flightCtrl->ModelData.outputs->r_SP_log[2], 
				flightCtrl->ModelData.outputs->x_hat_v_log[0],
				flightCtrl->ModelData.outputs->v_SP_log[0],
				flightCtrl->ModelData.outputs->x_hat_v_log[1],
				flightCtrl->ModelData.outputs->v_SP_log[1],
				flightCtrl->ModelData.outputs->x_hat_v_log[2],
				flightCtrl->ModelData.outputs->v_SP_log[2],
				flightCtrl->ModelData.outputs->x_hat_Phi_log[0], 
				flightCtrl->ModelData.outputs->Phi_SP_log[0], 
				flightCtrl->ModelData.outputs->x_hat_Phi_log[1], 
				flightCtrl->ModelData.outputs->Phi_SP_log[1], 
				flightCtrl->ModelData.outputs->x_hat_Phi_log[2],
				flightCtrl->ModelData.outputs->Phi_SP_log[2],
				flightCtrl->ModelData.outputs->x_hat_DPhi_log[0], 
				flightCtrl->ModelData.outputs->DPhi_SP_log[0], 
				flightCtrl->ModelData.outputs->x_hat_DPhi_log[1], 
				flightCtrl->ModelData.outputs->DPhi_SP_log[1], 
				flightCtrl->ModelData.outputs->x_hat_DPhi_log[2],
				flightCtrl->ModelData.outputs->DPhi_SP_log[2],
				(double)(HAL_GetTick() - logTimer)/1000.0, 
				'\0');*/
				
				xQueueSend(xPrintfQueue, log, 0);
			}
		}
		if((fromConsole.isManual == 0) && !flightCtrl->ModelData.inputs->armed && flightMode == 0){
			set_PPM_out(0, PROP_FL);
			set_PPM_out(0, PROP_FR);
			set_PPM_out(0, PROP_RL);
			set_PPM_out(0, PROP_RR);
			
			if(rcInputs.aux1 > 1900){
				droneStatus = CAL_REMOTE;
				xQueueSend(xLEDQueue, &droneStatus, 0);
				calPPMin();
				vTaskDelay(2000/portTICK_PERIOD_MS);
				droneStatus = IDLE;
				xQueueSend(xLEDQueue, &droneStatus, 0);
			}
			
			if(rcInputs.aux2 > 1900){
				droneStatus = CAL_COMPASS_XY;
				xQueueSend(xLEDQueue, &droneStatus, 0);
				calComXY();
				droneStatus = CAL_COMPASS_Z;
				xQueueSend(xLEDQueue, &droneStatus, 0);
				calComZ();
				droneStatus = CAL_COMPASS_OK;
				xQueueSend(xLEDQueue, &droneStatus, 0);
				vTaskDelay(2000/portTICK_PERIOD_MS);
				droneStatus = IDLE;
				xQueueSend(xLEDQueue, &droneStatus, 0);
			}
			
		}
		else if((fromConsole.isManual == 1) && !flightCtrl->ModelData.inputs->armed){
			set_PPM_out(fromConsole.prop1, PROP_FL);
			set_PPM_out(fromConsole.prop2, PROP_FR);
			set_PPM_out(fromConsole.prop3, PROP_RL);
			set_PPM_out(fromConsole.prop4, PROP_RR);
		}
		
		sampleCounter++;
		if (sampleCounter > 5){
			sampleCounter = 0;
			if(!flightCtrl->ModelData.inputs->armed && !logging){
				double Acc;
				GPS_StatusDef status = gpsGetAccPos(&Acc);
				if (status == GPS_LOCK_OK){
					Drone_StatusDef status = IDLE_GPS_LOCK_OK;
					xQueueSend(xLEDQueue, &status, 0);
				}
				
				if(fromConsole.printGpsStatus){
					fromConsole.printGpsStatus = 0;
					if (status == GPS_LOCK_OK){
						sprintf(log, "staus = GPS_LOCK_OK, Acc: %f, timer %f", Acc, (double)(HAL_GetTick() - logTimer)/1000.0);
						xQueueSend(xPrintfQueue, log, 0);
					}
					else if(status == OLDER_DATA){
						sprintf(log, "staus = OLD_DATA, Acc: %f, timer %f", Acc, (double)(HAL_GetTick() - logTimer)/1000.0);
						xQueueSend(xPrintfQueue, log, 0);
					}
					else if(status == DATA_INACURATE){
						sprintf(log, "staus = DATA_INACURATE, Acc: %f, timer %f", Acc, (double)(HAL_GetTick() - logTimer)/1000.0);
						xQueueSend(xPrintfQueue, log, 0);
					}
					else if(status == GPS_LOCK_NOT_OK){
						sprintf(log, "staus = GPS_LOCK_NOT_OK, Acc: %f, timer %f", Acc, (double)(HAL_GetTick() - logTimer)/1000.0);
						xQueueSend(xPrintfQueue, log, 0);
					}
				}
				
			}
		}
	}
}

void txDebugTask(void *pvParameters)
{
	printf("Starting txDebugTask...\n");
	char *toPrint = pvPortMalloc(300);
	
	while(1){
		xQueueReceive(xPrintfQueue, toPrint, portMAX_DELAY); //receive manual paramters from rxTask
		printf("%s\n", toPrint);
	}
}

void rxDebugTask(void *pvParameters)
{
	printf("Starting rxDebugTask...\n");	
	
	char *writeBuffer = pvPortMalloc(300);

	while(1){
		xSemaphoreTake(xRxSem, portMAX_DELAY);

		FreeRTOS_CLIProcessCommand(&rxString[0], writeBuffer, 300);
		
		xQueueSend(xPrintfQueue, writeBuffer, 0);
	}
}

void batVoltTask(void *pvParameters)
{
	printf("Starting batVoltTask...\n");	
	
	const TickType_t xPeriod = 1000 / portTICK_PERIOD_MS;
	TickType_t xLastWakeTime = xTaskGetTickCount() / portTICK_PERIOD_MS;
	double batteryVoltage = 0, lowLimit = 10.45, lowLowLimit = 10.32;
	double batteryLog[5] = {15.0, 15.0, 15.0, 15.0, 15.0};
	int lowCnt = 0, lowLowCnt = 0;
	
	while(1){
		vTaskDelayUntil(&xLastWakeTime, xPeriod);
		
		xSemaphoreTake(xGetBatMutex, portMAX_DELAY);
		batteryVoltage = getBatVolt();
		xSemaphoreGive(xGetBatMutex);
		
		for (int i = 1; i < 5; i++){
			batteryLog[i-1] = batteryLog[i];
		}
		batteryLog[4] = batteryVoltage;
		
		lowCnt = 0;
		lowLowCnt = 0;
		for (int i = 0; i < 5; i++){
			if (batteryLog[i] < lowLowLimit){
				lowLowCnt++;
			}
			if (batteryLog[i] < lowLimit){
				lowCnt++;
			}
		}
		
		if (lowLowCnt == 5){
			Drone_StatusDef status = LOW_LOW_BAT;
			xQueueSend(xLEDQueue, &status, 0);
		}
		else if (lowCnt == 5){
			Drone_StatusDef status = LOW_BAT;
			xQueueSend(xLEDQueue, &status, 0);
		}		
	}
}

void ledCtrlTask(void *pvParameters)
{
	printf("Starting ledCtrlTask...\n");
	Drone_StatusDef status;
	
	while(1){
		if (xQueueReceive(xLEDQueue, &status, 500) == pdPASS){
			if ((status != LOW_BAT) && (status != LOW_LOW_BAT)){
				ledResetAll();
			}
			if(status == ARMED_MANUAL_MODE){
				ledSetLED(LED_FL | LED_FR, LED_GREEN);
			}
			else if (status == ARMED_GPS_MODE){
				ledSetLED(LED_FL | LED_FR, LED_GREEN);
			}
			else if(status == LOW_LOW_BAT){
				ledSetLED(LED_RL | LED_RR, LED_RED);
			}
		}
		
		switch(status){
			case IDLE:
				ledSetLED(LED_FL, LED_BLUE);
				vTaskDelay(80);
				ledResetLED(LED_FL, LED_BLUE);
				ledSetLED(LED_FR, LED_BLUE);
				vTaskDelay(80);
				ledResetLED(LED_FR, LED_BLUE);
				ledSetLED(LED_RR, LED_BLUE);
				vTaskDelay(80);
				ledResetLED(LED_RR, LED_BLUE);
				ledSetLED(LED_RL, LED_BLUE);
				vTaskDelay(80);
				ledResetLED(LED_RL, LED_BLUE);
				vTaskDelay(500);
				break;
			case IDLE_GPS_LOCK_OK:
				ledSetLED(LED_FL, LED_GREEN);
				vTaskDelay(80);
				ledResetLED(LED_FL, LED_GREEN);
				ledSetLED(LED_FR, LED_GREEN);
				vTaskDelay(80);
				ledResetLED(LED_FR, LED_GREEN);
				ledSetLED(LED_RR, LED_GREEN);
				vTaskDelay(80);
				ledResetLED(LED_RR, LED_GREEN);
				ledSetLED(LED_RL, LED_GREEN);
				vTaskDelay(80);
				ledResetLED(LED_RL, LED_GREEN);
				vTaskDelay(500);
				break;
			case ARMED_MANUAL_MODE:
				ledSetLED(LED_RL | LED_RR, LED_GREEN | LED_RED);
				vTaskDelay(80);
				ledResetLED(LED_RL | LED_RR, LED_GREEN | LED_RED);
				vTaskDelay(120);
				ledSetLED(LED_RL | LED_RR, LED_GREEN | LED_RED);
				vTaskDelay(80);
				ledResetLED(LED_RL | LED_RR, LED_GREEN | LED_RED);
				vTaskDelay(500);
				break;
			case ARMED_GPS_MODE:
				ledSetLED(LED_RL | LED_RR, LED_BLUE);
				vTaskDelay(80);
				ledResetLED(LED_RL | LED_RR, LED_BLUE);
				vTaskDelay(120);
				ledSetLED(LED_RL | LED_RR, LED_BLUE);
				vTaskDelay(80);
				ledResetLED(LED_RL | LED_RR, LED_BLUE);
				vTaskDelay(500);
				break;
			case ARMED_AUTONOMOUS_MODE:
				ledSetLED(LED_FL, LED_RED | LED_GREEN | LED_BLUE);
				vTaskDelay(80);
				ledResetLED(LED_FL, LED_RED | LED_GREEN | LED_BLUE);
				ledSetLED(LED_FR, LED_RED | LED_GREEN | LED_BLUE);
				vTaskDelay(80);
				ledResetLED(LED_FR, LED_RED | LED_GREEN | LED_BLUE);
				ledSetLED(LED_RR, LED_RED | LED_GREEN | LED_BLUE);
				vTaskDelay(80);
				ledResetLED(LED_RR, LED_RED | LED_GREEN | LED_BLUE);
				ledSetLED(LED_RL, LED_RED | LED_GREEN | LED_BLUE);
				vTaskDelay(80);
				ledResetLED(LED_RL, LED_RED | LED_GREEN | LED_BLUE);
				vTaskDelay(500);
				break;
			case LOW_BAT:
				ledSetLED(LED_RL | LED_RR, LED_RED);
				vTaskDelay(500);
				ledResetLED(LED_RL | LED_RR, LED_RED);
				break;
			case LOW_LOW_BAT:
				vTaskDelay(500);
				break;
			case CAL_COMPASS_Z:
				ledSetLED(LED_FL | LED_RL, LED_RED | LED_GREEN);
				vTaskDelay(100);
				break;
			case CAL_COMPASS_XY:
				ledSetLED(LED_FL | LED_RR, LED_RED | LED_GREEN);
				vTaskDelay(100);
				break;
			case CAL_COMPASS_OK:
				ledSetLED(LED_FL | LED_FR | LED_RL | LED_RR, LED_RED | LED_GREEN);
				vTaskDelay(1500);
				break;
			case CAL_REMOTE:
				ledSetLED(LED_FL | LED_FR | LED_RL | LED_RR, LED_RED | LED_GREEN | LED_BLUE);
				break;
		}
	}
}

void altTask(void *pvParameters)
{
	while(1){
		xSemaphoreTake(xGetAlt, portMAX_DELAY);
		double altitude = getAlt(3);
		
		xQueueSend(xAltitude, &altitude, 0);
	}
}


