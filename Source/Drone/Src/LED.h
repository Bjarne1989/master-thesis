#ifndef __LED_H
#define __LED_H

#include "stm32f4xx_hal.h"

#define LED_FL 1
#define LED_FR 2
#define LED_RL 4
#define LED_RR 8

#define LED_RED 1
#define LED_GREEN 2
#define LED_BLUE 4

void ledSetLED(uint8_t leg, uint8_t colour);
void ledResetLED(uint8_t leg, uint8_t colour);
void ledSetAll(void);
void ledResetAll(void);
#endif
