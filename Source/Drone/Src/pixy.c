#include "pixy.h"

extern SPI_HandleTypeDef hspi1;

int sendBytes(uint8_t *byte, uint8_t length)
{
	return HAL_SPI_Transmit(&hspi1, byte, length, 10);
}

uint8_t getByte(uint8_t out)
{
	uint8_t byte;
	HAL_SPI_TransmitReceive(&hspi1, &out, &byte, 1, 10);
	
	return byte;
}

void getWords(uint16_t *words, uint8_t numWords)
{
  static uint8_t byte[14];
	static uint8_t out[14];
	
	for (int i = 0; i < numWords; i++){
		out[i*2] = PIXY_SYNC_BYTE;
		out[i*2 + 1] = 0;
	} 
	
	HAL_SPI_TransmitReceive(&hspi1, &out[0], &byte[0], numWords*2, 10);
	
	for (int i = 0; i < numWords; i++){
		words[i] = (byte[i*2] << 8) | byte[i*2 + 1];
	}
}


int getStart(void)
{
  uint16_t w, lastw;
  lastw = 0xffff;
	
	uint32_t timer = HAL_GetTick();
	
  while(1)
  {
		getWords(&w, 1);
    if (w==0 && lastw==0) return 0; // no start code  
		else if (HAL_GetTick() - timer > 25) return 0;
    else if (w==PIXY_START_WORD && lastw==PIXY_START_WORD) return 1; // code found!
    else if (w==PIXY_START_WORD_CC && lastw==PIXY_START_WORD) return 1;// found color code block  
    else if (w==PIXY_START_WORDX) getByte(0); // we're out of sync! (backwards)
    lastw = w; 
  }
}

uint16_t getObject(struct block *block){
	if (getStart()){
		
		uint16_t data[7];
		getWords(&data[0], 7);
		
		uint16_t checksum = 0;
		for (int i = 1; i < 7; i++){
			checksum += data[i];
		}
		
		if(checksum == data[0]){
			block->signature = data[1];
			block->x = data[2];
			block->y = data[3];
			block->width = data[4];
			block->height = data[5];
			block->angle = data[6];
			return 1;
		}
		else{
			return 0;
		}
	}
	else{
		return 0;
	}
}

int setBrightness(uint8_t brightness)
{
  uint8_t outBuf[3];

  outBuf[0] = 0x00;
  outBuf[1] = PIXY_CAM_BRIGHTNESS_SYNC; 
  outBuf[2] = brightness;

  return sendBytes(outBuf, 3);
}

int setLED(uint8_t r, uint8_t g, uint8_t b)
{
  uint8_t outBuf[5];

  outBuf[0] = 0x00;
  outBuf[1] = PIXY_LED_SYNC; 
  outBuf[2] = r;
  outBuf[3] = g;
  outBuf[4] = b;

  return sendBytes(outBuf, 5);
}
