#include "PPMcontrol.h"

//PPM input flags
#define ROLL_FLAG 1
#define PITCH_FLAG 2
#define THROTTLE_FLAG 4
#define YAW_FLAG 8
#define AUX1_FLAG 16
#define AUX2_FLAG 32
#define SPARE1_FLAG 64
#define SPARE2_FLAG 128

//PPM input pins
#define ROLL_PIN GPIO_PIN_4
#define PITCH_PIN GPIO_PIN_5
#define THROTTLE_PIN GPIO_PIN_6
#define YAW_PIN GPIO_PIN_7
#define AUX1_PIN GPIO_PIN_8
#define AUX2_PIN GPIO_PIN_9
#define SPARE1_PIN GPIO_PIN_10
#define SPARE2_PIN GPIO_PIN_11

TIM_HandleTypeDef *PPMtimer2;
TIM_HandleTypeDef *PPMtimer5;
TIM_HandleTypeDef *timer; //For timing PPM input

//Constants for calculating PPM input
uint16_t counterPeriod = 52499;
double ticsToSec = 1.0/21.0;
int32_t negativeNumberMask = 0x80000000;

//Variables for setting throttle if RC-signal lost more than specified duration
double lastThrottle = 2000;
uint16_t rcTimeout = 100; //(ms)
uint32_t rcTimestamp = 0;
uint8_t timedOut = 0;

//New value available
volatile uint8_t updateFlagsShared;

//New values shared with ISR
volatile int32_t rollInShared;
volatile int32_t pitchInShared;
volatile int32_t throttleInShared;
volatile int32_t yawInShared;
volatile int32_t aux1InShared;
volatile int32_t aux2InShared;
volatile int32_t spare1InShared;
volatile int32_t spare2InShared;

//Start time of PPM
uint16_t rollStart;
uint16_t pitchStart;
uint16_t throttleStart;
uint16_t yawStart;
uint16_t aux1Start;
uint16_t aux2Start;
uint16_t spare1Start;
uint16_t spare2Start;

//Start time of PPM
uint16_t rollIn;
uint16_t pitchIn;
uint16_t throttleIn;
uint16_t yawIn;
uint16_t aux1In;
uint16_t aux2In;
uint16_t spare1In;
uint16_t spare2In;

//Rc-controller offsets
double rollOffset = 1487;
double yawOffset = 1481;
double pitchOffset = 1484;

HAL_StatusTypeDef init_PPM_out(TIM_HandleTypeDef *t1, TIM_HandleTypeDef *t2, TIM_HandleTypeDef *counter){
	PPMtimer2 = t1;
	PPMtimer5 = t2;
	timer = counter;
	
	HAL_StatusTypeDef status;
	int initVal = 83999;
	
	// PA15
	PPMtimer2->Instance->CCR1 = initVal;
	status = HAL_TIM_OC_Start_IT(PPMtimer2, TIM_CHANNEL_1);
	if (status != HAL_OK) return status;
	
	// PB3
	PPMtimer2->Instance->CCR2 = initVal;
	status = HAL_TIM_OC_Start_IT(PPMtimer2, TIM_CHANNEL_2);
	if (status != HAL_OK) return status;
	
	// PA2
	PPMtimer2->Instance->CCR3 = initVal;
	status = HAL_TIM_OC_Start_IT(PPMtimer2, TIM_CHANNEL_3);
	if (status != HAL_OK) return status;
	
	// PA3
	PPMtimer2->Instance->CCR4 = initVal;
	status = HAL_TIM_OC_Start_IT(PPMtimer2, TIM_CHANNEL_4);
	if (status != HAL_OK) return status;
	
	// PA0
	PPMtimer5->Instance->CCR1 = initVal;
	status = HAL_TIM_OC_Start_IT(PPMtimer5, TIM_CHANNEL_1);
	if (status != HAL_OK) return status;
	
	return status;
}

void calPPMin(){
	struct PPM_in rcCtrl;
	get_PPM_in(&rcCtrl);
	
	rollOffset = rollOffset + rcCtrl.roll;
	pitchOffset = pitchOffset - rcCtrl.pitch;
	yawOffset = yawOffset - rcCtrl.yaw;
}


void set_PPM_out(float power, PPM_OutDef ppmOut){
	
	if (power < 0) power = 0;
	if (power > 1000) power = 1000;
	
	int counterValue = (int)(84000.0/1000.0*power + 83999.0);
	
	switch(ppmOut){
	// PA15
	case(PROP_FL):
		PPMtimer2->Instance->CCR1 = counterValue;
		break;
	// PB3
	case(PROP_FR):
		PPMtimer2->Instance->CCR2 = counterValue;
		break;
	// PA2
	case(PROP_RL):
		PPMtimer2->Instance->CCR3 = counterValue;
		break;
	// PA3
	case(PROP_RR):
		PPMtimer2->Instance->CCR4 = counterValue;
		break;
	// PA0
	case(PPM_SPARE1):
		PPMtimer5->Instance->CCR1 = counterValue;
		break;
	}
}

void update_PPM_in_timer(PPM_inDef input){
	uint16_t timeStop;
	
	switch(input){
		case(RC_ROLL):
			if(HAL_GPIO_ReadPin(GPIOE, ROLL_PIN) == GPIO_PIN_SET){
				rollStart = timer->Instance->CNT;
			}
			else{
				timeStop = timer->Instance->CNT;
				rollInShared = timeStop - rollStart;
				
				if (rollInShared & negativeNumberMask) rollInShared = timeStop + (counterPeriod - rollStart);
				
				updateFlagsShared |= ROLL_FLAG;
			}
			break;
		case(RC_PITCH):
			if(HAL_GPIO_ReadPin(GPIOE, PITCH_PIN) == GPIO_PIN_SET){
				pitchStart = timer->Instance->CNT;
			}
			else{
				timeStop = timer->Instance->CNT;
				pitchInShared = timeStop - pitchStart;
				
				if (pitchInShared & negativeNumberMask) pitchInShared = timeStop + (counterPeriod - pitchStart);
				
				updateFlagsShared |= PITCH_FLAG;
			}
			break;
		case(RC_THROTTLE):
			if(HAL_GPIO_ReadPin(GPIOE, THROTTLE_PIN) == GPIO_PIN_SET){
				throttleStart = timer->Instance->CNT;
			}
			else{
				timeStop = timer->Instance->CNT;
				throttleInShared = timeStop - throttleStart;
				
				if (throttleInShared & negativeNumberMask) throttleInShared = timeStop + (counterPeriod - throttleStart);
				
				updateFlagsShared |= THROTTLE_FLAG;
			}
			break;
		case(RC_YAW):
			if(HAL_GPIO_ReadPin(GPIOE, YAW_PIN) == GPIO_PIN_SET){
				yawStart = timer->Instance->CNT;
			}
			else{
				timeStop = timer->Instance->CNT;
				yawInShared = timeStop - yawStart;
				
				if (yawInShared & negativeNumberMask) yawInShared = timeStop + (counterPeriod - yawStart);
				
				updateFlagsShared |= YAW_FLAG;
			}
			break;
		case(RC_AUX1):
			if(HAL_GPIO_ReadPin(GPIOE, AUX1_PIN) == GPIO_PIN_SET){
				aux1Start = timer->Instance->CNT;
			}
			else{
				timeStop = timer->Instance->CNT;
				aux1InShared = timeStop - aux1Start;
				
				if (aux1InShared & negativeNumberMask) aux1InShared = timeStop + (counterPeriod - aux1Start);
				
				updateFlagsShared |= AUX1_FLAG;
			}
			break;
		case(RC_AUX2):
			if(HAL_GPIO_ReadPin(GPIOE, AUX2_PIN) == GPIO_PIN_SET){
				aux2Start = timer->Instance->CNT;
			}
			else{
				timeStop = timer->Instance->CNT;
				aux2InShared = timeStop - aux2Start;
				
				if (aux2InShared & negativeNumberMask) aux2InShared = timeStop + (counterPeriod - aux2Start);
				
				updateFlagsShared |= AUX2_FLAG;
			}
			break;
		case(SPARE1):
			if(HAL_GPIO_ReadPin(GPIOE, SPARE1_PIN) == GPIO_PIN_SET){
				spare1Start = timer->Instance->CNT;
			}
			else{
				timeStop = timer->Instance->CNT;
				spare1InShared = timeStop - spare1Start;
				
				if (spare1InShared & negativeNumberMask) spare1InShared = timeStop + (counterPeriod - spare1Start);
				
				updateFlagsShared |= SPARE1_FLAG;
			}
			break;
		case(SPARE2):
			if(HAL_GPIO_ReadPin(GPIOE, SPARE2_PIN) == GPIO_PIN_SET){
				spare2Start = timer->Instance->CNT;
			}
			else{
				timeStop = timer->Instance->CNT;
				spare2InShared = timeStop - spare2Start;
				
				if (spare2InShared & negativeNumberMask) spare2InShared = timeStop + (counterPeriod - spare2Start);
				
				updateFlagsShared |= SPARE2_FLAG;
			}
			break;
	}
	rcTimestamp = HAL_GetTick();
}

uint8_t get_PPM_in(struct PPM_in *inputs){
	if (updateFlagsShared){
		HAL_NVIC_DisableIRQ(EXTI4_IRQn);
		HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
		HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);
		
		if(updateFlagsShared & ROLL_FLAG){
			rollIn = rollInShared;
		}

		if(updateFlagsShared & PITCH_FLAG){
			pitchIn = pitchInShared;
		}

		if(updateFlagsShared & THROTTLE_FLAG){
			throttleIn = throttleInShared;
		}

		if(updateFlagsShared & YAW_FLAG){
			yawIn = yawInShared;
		}

		if(updateFlagsShared & AUX1_FLAG){
			aux1In = aux1InShared;
		}

		if(updateFlagsShared & AUX2_FLAG){
			aux2In = aux2InShared;
		}

		if(updateFlagsShared & SPARE1_FLAG){
			spare1In = spare1InShared;
		}

		if(updateFlagsShared & SPARE2_FLAG){
			spare2In = spare2InShared;
		}
		
		updateFlagsShared = 0;
		HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
		HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
		HAL_NVIC_EnableIRQ(EXTI4_IRQn);
	}
	
	if (rcTimeout < HAL_GetTick() - rcTimestamp){
		inputs->roll = 0.0;
		inputs->pitch = 0.0;
		inputs->throttle = 0.0;
		inputs->yaw = 0.0;

		timedOut = 1;
		return 0;
	}
	else{
		double throttle = ticsToSec * throttleIn - 1000.0;
		if ((lastThrottle < 100) && timedOut && (throttle > 100)){
			inputs->throttle = 140.0;
		}
		else{
			inputs->throttle = ticsToSec * throttleIn - 1000.0;
			lastThrottle = inputs->throttle;
			timedOut = 0;
		}
		
		inputs->roll = ticsToSec * rollIn  - rollOffset;
		inputs->pitch = pitchOffset - ticsToSec * pitchIn;
		inputs->yaw = yawOffset - ticsToSec * yawIn;
		inputs->aux1 = ticsToSec * aux1In;
		inputs->aux2 = ticsToSec * aux2In;
		
	//	inputs->spare1 = ticsToSec * spare1In;
	//	inputs->spare2 = ticsToSec * spare2In;
		return 1;
	}
}
