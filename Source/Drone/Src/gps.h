#include "stm32f4xx_hal.h"
#include <math.h>
#include <stdlib.h>
#include <string.h>

#ifndef __gps_H
#define __gps_H

typedef enum 
{
  GPS_LOCK_OK,
	OLDER_DATA,
	DATA_INACURATE,
	GPS_LOCK_NOT_OK
} GPS_StatusDef;

void gpsInit(void);
uint8_t gpsIsInitiated(void);
void gpsUpdateData(uint8_t *data);

GPS_StatusDef gpsGetPos(double *px, double *py, double *pz);
GPS_StatusDef gpsGetVel(double *vx, double *vy, double *vz);
GPS_StatusDef gpsGetAccVel(double *vAcc);
GPS_StatusDef gpsGetAccPos(double *pAcc);
GPS_StatusDef gpsGetNumSat(uint8_t *numSat);
GPS_StatusDef gpsGetDOP(uint8_t *dop);

void ecefToLLA(double *latitude, double *longitude, double x_e, double y_e, double z_e);
void ecefToENU(double *enuX, double *enuY, double *enuZ, double ecef[]);


#endif
