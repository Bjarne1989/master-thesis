
#ifndef __sensors_H
#define __sensors_H

#include "stm32f4xx_hal.h"
#include <math.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"

#define PI 3.1415926535897932384626433832795

struct sensor_MotionDef
{
	double acc[3];
	int com[3];
	double gyr[3];
};


uint8_t init_sensors(I2C_HandleTypeDef *hi2c);
void getMotion(struct sensor_MotionDef *motion);

/*---------------------------- Gyreoscope Methods -------------------*/
void initGyro(void);
void getGyrXYZ(double *xGyr, double *yGyr, double *zGyr);
HAL_StatusTypeDef isGyroReady(void);

/*---------------------------- Accelerometer Methods ----------------*/
void initAccelorometer(void);
void getAccXYZ(double *xAcc, double *yAcc, double *zAcc);
HAL_StatusTypeDef isAccReady(void);

/*---------------------------- Compass Methods ----------------------*/
uint8_t initCompass(void);
void getComXYZ(int16_t *xCom, int16_t *yCom, int16_t *zCom);
void calComXY(void);
void calComZ(void);
HAL_StatusTypeDef isComReady(void);

/*---------------------------- Barometer Methods --------------------*/
void initBaromoter(void);
int32_t getPres(uint8_t oss);
double getAlt(uint8_t oss);
double getTemp(void);
HAL_StatusTypeDef isBarReady(void);

#endif
