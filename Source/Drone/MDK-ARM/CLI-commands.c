#include "FreeRTOS.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include "flightController.h"
#include "semphr.h"

#include <string.h>
#include <ctype.h>

extern RT_MODEL_flightController_T *flightCtrl;
extern SemaphoreHandle_t xParamMutex;
extern SemaphoreHandle_t xGetBatMutex;
extern QueueHandle_t xManualQueue;
extern QueueHandle_t xlogQueue;
extern QueueHandle_t xFlightPlanQueue;

extern double getBatVolt(void);

static BaseType_t prvOrController(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvListOrController(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvPoController(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvListPoController(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvSetProp(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvManualMode(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvLogging(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvBatVolt(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvLoadFlightCoord(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvExeFlightPlan(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvGpsStatus(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvClearFlightPlan(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t prvGetFlightPlan(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);

struct console
{
	uint8_t printGpsStatus;
	uint8_t startFlightPlan;
	uint8_t isManual;
	double prop1;
	double prop2;
	double prop3;
	double prop4;
};
struct console setFromConsole;

static const CLI_Command_Definition_t xOrCtrlParam =
{
	"orctrlparam",
	"orctrlparam <param1> <param2> <param3>:\nSet controller parameters\nparam1: axis, ppitchroll, pyaw, vpitchroll or vyaw\nparam2: controller parameter, p, i or d\nparam3: value \r\n\r\n",
	prvOrController,
	3
};

static const CLI_Command_Definition_t xListOrCtrlParam =
{
	"listorctrlparam",
	"listorctrlparam:\r\nLists controller parameters\r\n\r\n",
	prvListOrController,
	0
};

static const CLI_Command_Definition_t xPoCtrlParam =
{
	"poctrlparam",
	"poctrlparam <param1> <param2> <param3>:\nSet controller parameters\nparam1: axis, pxy, pz, vxy or vz\nparam2: controller parameter, p, i or d\nparam3: value \r\n\r\n",
	prvPoController,
	3
};

static const CLI_Command_Definition_t xListPoCtrlParam =
{
	"listpoctrlparam",
	"listpoctrlparam:\r\nLists controller parameters\r\n\r\n",
	prvListPoController,
	0
};

static const CLI_Command_Definition_t xSetProp =
{
	"setprop",
	"setprop <param1> <param2> <param3> <param4>:\r\nSet propeller power, activated with 'manmode' command\nparam1: Propeller 1 power\nparam2: Propeller 2 power\nparam3: Propeller 3 power\nparam4: Propeller 4 power\r\n\r\n",
	prvSetProp,
	4
};

static const CLI_Command_Definition_t xManualMode =
{
	"manmode",
	"manmode <param1>:\r\nTurn manual mode on/off\nparam1: 'on'/'off'\r\n\r\n",
	prvManualMode,
	1
};

static const CLI_Command_Definition_t xLogging =
{
	"log",
	"log <param1>:\r\nTurn logging on/off\nparam1: 'on'/'off'\r\n\r\n",
	prvLogging,
	1
};

static const CLI_Command_Definition_t xBatVolt =
{
	"getbatvolt",
	"getbatvolt:\r\nGet the battery voltage\r\n\r\n",
	prvBatVolt,
	0
};

static const CLI_Command_Definition_t xLoadFlightCoord =
{
	"loadflight",
	"loadflight <param1> <param2> <param3>:\r\nLoad flight plan\nparam1: X coordinate [m] (East)\nparam2: Y coordinate [m] (North)\nparam3: Z coordinate [m] (Up)\r\n\r\n",
	prvLoadFlightCoord,
	3
};

static const CLI_Command_Definition_t xExeFlightPlan =
{
	"exeflightplan",
	"exeflightplan:\r\nExecutes the loaded flightplan\r\n\r\n",
	prvExeFlightPlan,
	0
};

static const CLI_Command_Definition_t xGetGpsStatus =
{
	"getgpsstatus",
	"getgpsstatus:\r\nCurrent GPS status\r\n\r\n",
	prvGpsStatus,
	0
};
static const CLI_Command_Definition_t xClearFlightPlan =
{
	"clearflightplan",
	"clearflightplan:\r\nClear current flight plan\r\n\r\n",
	prvClearFlightPlan,
	0
};

static const CLI_Command_Definition_t xGetFlightPlan =
{
	"getflightplan",
	"getflightplan:\r\nGet current flight plan\r\n\r\n",
	prvGetFlightPlan,
	0
};


void vRegisterCLICommands(void)
{
	FreeRTOS_CLIRegisterCommand(&xOrCtrlParam);
	FreeRTOS_CLIRegisterCommand(&xListOrCtrlParam);
	FreeRTOS_CLIRegisterCommand(&xPoCtrlParam);
	FreeRTOS_CLIRegisterCommand(&xListPoCtrlParam);
	FreeRTOS_CLIRegisterCommand(&xSetProp);
	FreeRTOS_CLIRegisterCommand(&xManualMode);
	FreeRTOS_CLIRegisterCommand(&xLogging);
	FreeRTOS_CLIRegisterCommand(&xBatVolt);
	FreeRTOS_CLIRegisterCommand(&xLoadFlightCoord);
	FreeRTOS_CLIRegisterCommand(&xExeFlightPlan);
	FreeRTOS_CLIRegisterCommand(&xGetGpsStatus);
	FreeRTOS_CLIRegisterCommand(&xClearFlightPlan);
	FreeRTOS_CLIRegisterCommand(&xGetFlightPlan);
}


static BaseType_t prvOrController(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	const char *param1, *param2, *param3;
	BaseType_t param1Len, param2Len, param3Len;
	double value;
	
	param1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &param1Len);
	param2 = FreeRTOS_CLIGetParameter(pcCommandString, 2, &param2Len);
	param3 = FreeRTOS_CLIGetParameter(pcCommandString, 3, &param3Len);
	
	if (isalpha(param3[0])){
		strncpy(pcWriteBuffer, "<param3> is not numeric", xWriteBufferLen);
	}
	else{
		value = atof(param3);
		
		if(strncmp(param1, "ppitchroll", param1Len) == 0){
			if(param2[0] == 'p'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_Phi[0] = value;
				flightCtrl->ModelData.inputs->PID_Phi[3] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. Pitch and Roll P = %f", value);
			}
			else if(param2[0] == 'i'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_Phi[1] = value;
				flightCtrl->ModelData.inputs->PID_Phi[4] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. Pitch and Roll I = %f", value);
			}
			else if(param2[0] == 'd'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_Phi[2] = value;
				flightCtrl->ModelData.inputs->PID_Phi[5] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. Pitch and Roll D = %f", value);
			}
			else{
				strncpy(pcWriteBuffer, "Invalid <param2>", xWriteBufferLen);
			}
		}
		else if(strncmp(param1, "pyaw", param1Len) == 0){
			if(param2[0] == 'p'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_Phi[6] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. Yaw P = %f", value);
			}
			else if(param2[0] == 'i'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_Phi[7] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. Yaw I = %f", value);
				
			}
			else if(param2[0] == 'd'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_Phi[8] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. Yaw D = %f", value);
				
			}
			else{
				strncpy(pcWriteBuffer, "Invalid <param2>", xWriteBufferLen);
			}
		}
		else if(strncmp(param1, "vpitchroll", param1Len) == 0){
			if(param2[0] == 'p'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_DPhi[0] = value;
				flightCtrl->ModelData.inputs->PID_DPhi[3] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. Pitch and Roll P = %f", value);
			}
			else if(param2[0] == 'i'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_DPhi[1] = value;
				flightCtrl->ModelData.inputs->PID_DPhi[4] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. Pitch and Roll I = %f", value);
			}
			else if(param2[0] == 'd'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_DPhi[2] = value;
				flightCtrl->ModelData.inputs->PID_DPhi[5] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. Pitch and Roll D = %f", value);
			}
			else{
				strncpy(pcWriteBuffer, "Invalid <param2>", xWriteBufferLen);
			}
		}
		else if(strncmp(param1, "vyaw", param1Len) == 0){
			if(param2[0] == 'p'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_DPhi[6] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. Yaw P = %f", value);
			}
			else if(param2[0] == 'i'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_DPhi[7] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. Yaw I = %f", value);
				
			}
			else if(param2[0] == 'd'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_DPhi[8] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. Yaw D = %f", value);
			}
			else{
				strncpy(pcWriteBuffer, "Invalid <param2>", xWriteBufferLen);
			}
		}		
		else{
			strncpy(pcWriteBuffer, "Invalid <param1>", xWriteBufferLen);
		}
	}
	
	return pdFALSE;
}
	
static BaseType_t prvListOrController(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	double prp, pri, prd, ppp, ppi, ppd, pyp, pyi, pyd, vrp, vri, vrd, vpp, vpi, vpd, vyp, vyi, vyd;
	
	xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
	ppp = flightCtrl->ModelData.inputs->PID_Phi[0];
	ppi = flightCtrl->ModelData.inputs->PID_Phi[1];
	ppd = flightCtrl->ModelData.inputs->PID_Phi[2];
	prp = flightCtrl->ModelData.inputs->PID_Phi[3];
	pri = flightCtrl->ModelData.inputs->PID_Phi[4];
	prd = flightCtrl->ModelData.inputs->PID_Phi[5];
	pyp = flightCtrl->ModelData.inputs->PID_Phi[6];
	pyi = flightCtrl->ModelData.inputs->PID_Phi[7];
	pyd = flightCtrl->ModelData.inputs->PID_Phi[8];
	vrp = flightCtrl->ModelData.inputs->PID_DPhi[0];
	vri = flightCtrl->ModelData.inputs->PID_DPhi[1];
	vrd = flightCtrl->ModelData.inputs->PID_DPhi[2];
	vpp = flightCtrl->ModelData.inputs->PID_DPhi[3];
	vpi = flightCtrl->ModelData.inputs->PID_DPhi[4];
	vpd = flightCtrl->ModelData.inputs->PID_DPhi[5];
	vyp = flightCtrl->ModelData.inputs->PID_DPhi[6];
	vyi = flightCtrl->ModelData.inputs->PID_DPhi[7];
	vyd = flightCtrl->ModelData.inputs->PID_DPhi[8];
	xSemaphoreGive(xParamMutex);
	
	sprintf(pcWriteBuffer, "\t\tP\t\tI\t\tD\nPos. Pitch:\t%f\t%f\t%f\nPos. Roll:\t%f\t%f\t%f\nPos. Yaw:\t%f\t%f\t%f\nVel. Pitch:\t%f\t%f\t%f\nVel. Roll:\t%f\t%f\t%f\nVel. Yaw:\t%f\t%f\t%f",
	ppp, ppi, ppd, prp, pri, prd, pyp, pyi, pyd, vrp, vri, vrd, vpp, vpi, vpd, vyp, vyi, vyd);
	
	return pdFALSE;
}

static BaseType_t prvPoController(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	const char *param1, *param2, *param3;
	BaseType_t param1Len, param2Len, param3Len;
	double value;
	
	param1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &param1Len);
	param2 = FreeRTOS_CLIGetParameter(pcCommandString, 2, &param2Len);
	param3 = FreeRTOS_CLIGetParameter(pcCommandString, 3, &param3Len);
	
	if (isalpha(param3[0])){
		strncpy(pcWriteBuffer, "<param3> is not numeric", xWriteBufferLen);
	}
	else{
		value = atof(param3);
		
		if(strncmp(param1, "pxy", param1Len) == 0){
			if(param2[0] == 'p'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_r[0] = value;
				flightCtrl->ModelData.inputs->PID_r[3] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. x and y P = %f", value);
			}
			else if(param2[0] == 'i'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_r[1] = value;
				flightCtrl->ModelData.inputs->PID_r[4] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. x and y I = %f", value);
			}
			else if(param2[0] == 'd'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_r[2] = value;
				flightCtrl->ModelData.inputs->PID_r[5] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. x and y D = %f", value);
			}
			else{
				strncpy(pcWriteBuffer, "Invalid <param2>", xWriteBufferLen);
			}
		}
		else if(strncmp(param1, "pz", param1Len) == 0){
			if(param2[0] == 'p'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_r[6] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. z P = %f", value);
			}
			else if(param2[0] == 'i'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_r[7] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. z I = %f", value);
				
			}
			else if(param2[0] == 'd'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_r[8] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Pos. z D = %f", value);
				
			}
			else{
				strncpy(pcWriteBuffer, "Invalid <param2>", xWriteBufferLen);
			}
		}
		else if(strncmp(param1, "vxy", param1Len) == 0){
			if(param2[0] == 'p'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_v[0] = value;
				flightCtrl->ModelData.inputs->PID_v[3] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. x and y P = %f", value);
			}
			else if(param2[0] == 'i'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_v[1] = value;
				flightCtrl->ModelData.inputs->PID_v[4] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. x and y I = %f", value);
			}
			else if(param2[0] == 'd'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_v[2] = value;
				flightCtrl->ModelData.inputs->PID_v[5] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. x and y D = %f", value);
			}
			else{
				strncpy(pcWriteBuffer, "Invalid <param2>", xWriteBufferLen);
			}
		}
		else if(strncmp(param1, "vz", param1Len) == 0){
			if(param2[0] == 'p'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_v[6] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. z P = %f", value);
			}
			else if(param2[0] == 'i'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_v[7] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. z I = %f", value);
				
			}
			else if(param2[0] == 'd'){
				xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
				flightCtrl->ModelData.inputs->PID_v[8] = value;
				xSemaphoreGive(xParamMutex);
				
				sprintf(pcWriteBuffer, "Vel. z D = %f", value);
			}
			else{
				strncpy(pcWriteBuffer, "Invalid <param2>", xWriteBufferLen);
			}
		}		
		else{
			strncpy(pcWriteBuffer, "Invalid <param1>", xWriteBufferLen);
		}
	}
	
	return pdFALSE;
}
	
static BaseType_t prvListPoController(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	double prp, pri, prd, ppp, ppi, ppd, pyp, pyi, pyd, vrp, vri, vrd, vpp, vpi, vpd, vyp, vyi, vyd;
	
	xSemaphoreTake(xParamMutex, 200/portTICK_PERIOD_MS);
	ppp = flightCtrl->ModelData.inputs->PID_r[0];
	ppi = flightCtrl->ModelData.inputs->PID_r[1];
	ppd = flightCtrl->ModelData.inputs->PID_r[2];
	prp = flightCtrl->ModelData.inputs->PID_r[3];
	pri = flightCtrl->ModelData.inputs->PID_r[4];
	prd = flightCtrl->ModelData.inputs->PID_r[5];
	pyp = flightCtrl->ModelData.inputs->PID_r[6];
	pyi = flightCtrl->ModelData.inputs->PID_r[7];
	pyd = flightCtrl->ModelData.inputs->PID_r[8];
	vrp = flightCtrl->ModelData.inputs->PID_v[0];
	vri = flightCtrl->ModelData.inputs->PID_v[1];
	vrd = flightCtrl->ModelData.inputs->PID_v[2];
	vpp = flightCtrl->ModelData.inputs->PID_v[3];
	vpi = flightCtrl->ModelData.inputs->PID_v[4];
	vpd = flightCtrl->ModelData.inputs->PID_v[5];
	vyp = flightCtrl->ModelData.inputs->PID_v[6];
	vyi = flightCtrl->ModelData.inputs->PID_v[7];
	vyd = flightCtrl->ModelData.inputs->PID_v[8];
	xSemaphoreGive(xParamMutex);
	
	sprintf(pcWriteBuffer, "\t\tP\t\tI\t\tD\nPos. x:\t%f\t%f\t%f\nPos. y:\t%f\t%f\t%f\nPos. z:\t%f\t%f\t%f\nVel. x:\t%f\t%f\t%f\nVel. y:\t%f\t%f\t%f\nVel. z:\t%f\t%f\t%f",
	ppp, ppi, ppd, prp, pri, prd, pyp, pyi, pyd, vrp, vri, vrd, vpp, vpi, vpd, vyp, vyi, vyd);
	
	return pdFALSE;
}

static BaseType_t prvSetProp(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	const char *param1, *param2, *param3, *param4;
	BaseType_t param1Len, param2Len, param3Len, param4Len;
	
	param1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &param1Len);
	param2 = FreeRTOS_CLIGetParameter(pcCommandString, 2, &param2Len);
	param3 = FreeRTOS_CLIGetParameter(pcCommandString, 3, &param3Len);
	param4 = FreeRTOS_CLIGetParameter(pcCommandString, 4, &param4Len);
	
	if ((isalpha(param1[0])) || (isalpha(param2[0])) || (isalpha(param3[0])) || (isalpha(param4[0]))){
		strncpy(pcWriteBuffer, "Paramters must be numeric", xWriteBufferLen);
	}
	else{
		setFromConsole.prop1 = atof(param1);
		setFromConsole.prop2 = atof(param2);
		setFromConsole.prop3 = atof(param3);
		setFromConsole.prop4 = atof(param4);
		sprintf(pcWriteBuffer, "Prop1 = %f, Prop2 = %f, Prop3 = %f, Prop4 = %f", setFromConsole.prop1, setFromConsole.prop2, setFromConsole.prop3, setFromConsole.prop4);
		xQueueSend(xManualQueue, &setFromConsole, 0);
	}
	return pdFALSE;
}

static BaseType_t prvManualMode(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	const char *param1;
	BaseType_t param1Len;
	
	param1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &param1Len);
	
	if(strncmp(param1, "on", 2) == 0){
		setFromConsole.isManual = 1;
		xQueueSend(xManualQueue, &setFromConsole, 0);
		strncpy(pcWriteBuffer, "Manual mode ON", xWriteBufferLen);
	}
	else if(strncmp(param1, "off", 3) == 0){
		setFromConsole.isManual = 0;
		xQueueSend(xManualQueue, &setFromConsole, 0);
		strncpy(pcWriteBuffer, "Manual mode OFF", xWriteBufferLen);
	}
	else{
		strncpy(pcWriteBuffer, "Invalid parameter", xWriteBufferLen);
	}
	return pdFALSE;
}

static BaseType_t prvLogging(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	const char *param1;
	BaseType_t param1Len;
	uint8_t logging = 0;
	
	param1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &param1Len);
	
	if(strncmp(param1, "on", 2) == 0){
		logging = 1;
		xQueueSend(xlogQueue, &logging, 0);
		strncpy(pcWriteBuffer, "Logging ON", xWriteBufferLen);
	}
	else if(strncmp(param1, "off", 3) == 0){
		logging = 0;
		xQueueSend(xlogQueue, &logging, 0);
		strncpy(pcWriteBuffer, "Logging OFF", xWriteBufferLen);
	}
	else{
		strncpy(pcWriteBuffer, "Invalid parameter", xWriteBufferLen);
	}
	
	return pdFALSE;
}

static BaseType_t prvBatVolt(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	
	xSemaphoreTake(xGetBatMutex, portMAX_DELAY);
	double batteryVoltage = getBatVolt();
	xSemaphoreGive(xGetBatMutex);
	
	sprintf(pcWriteBuffer, "Battery voltage: %fV\n", batteryVoltage);
	
	return pdFALSE;
}


static BaseType_t prvLoadFlightCoord(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
	{
	const char *param1, *param2, *param3;
	BaseType_t param1Len, param2Len, param3Len;
	double coord[3];
	
	param1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &param1Len);
	if (isalpha(param1[0])){
		strncpy(pcWriteBuffer, "<param1> is not numeric", xWriteBufferLen);
		return pdFALSE;
	}
	
	param2 = FreeRTOS_CLIGetParameter(pcCommandString, 2, &param2Len);
	if (isalpha(param2[0])){
		strncpy(pcWriteBuffer, "<param2> is not numeric", xWriteBufferLen);
		return pdFALSE;
	}
	
	param3 = FreeRTOS_CLIGetParameter(pcCommandString, 3, &param3Len);
	if (isalpha(param3[0])){
		strncpy(pcWriteBuffer, "<param3> is not numeric", xWriteBufferLen);
		return pdFALSE;
	}
	
	coord[0] = atof(param1);
	coord[1] = atof(param2);
	coord[2] = atof(param3);
	
	
	for (int i = 0; i < 3; i++){
		if (fabs(coord[i]) > 50){
			strncpy(pcWriteBuffer, "The flightPlan is limited to +/-[50, 50, 50]", xWriteBufferLen);
			return pdFALSE;
		}
	}
	
	if (coord[2] < 3 && coord[2] > 0){
			strncpy(pcWriteBuffer, "The altitude (z) must be greater than 3 meters", xWriteBufferLen);
			return pdFALSE;
	}
	
	if (xQueueSend(xFlightPlanQueue, &coord, 0) == pdFAIL){
		strncpy(pcWriteBuffer, "Flight plan is full!", xWriteBufferLen);
		return pdFALSE;
	}
	
	sprintf(pcWriteBuffer, "Added x = %f (East), y = %f (North), z = %f (Up)", coord[0], coord[1], coord[2]);
	return pdFALSE;
}

static BaseType_t prvExeFlightPlan(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
	{
	setFromConsole.printGpsStatus = 0;
	setFromConsole.isManual = 0;
	setFromConsole.startFlightPlan = 1;
	xQueueSend(xManualQueue, &setFromConsole, 0);
	strncpy(pcWriteBuffer, "Executing flight plan...", xWriteBufferLen);
	
	return pdFALSE;
}

static BaseType_t prvGpsStatus(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	setFromConsole.printGpsStatus = 1;
	setFromConsole.isManual = 0;
	setFromConsole.startFlightPlan = 0;
	xQueueSend(xManualQueue, &setFromConsole, 0);
	strncpy(pcWriteBuffer, "Acquiring status", xWriteBufferLen);
	
	return pdFALSE;
}

static BaseType_t prvClearFlightPlan(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	uint32_t flightPlanLen =  uxQueueMessagesWaiting(xFlightPlanQueue);
	
	xQueueReset(xFlightPlanQueue);
	sprintf(pcWriteBuffer, "Deleted %d coordinates from flight plan", flightPlanLen);
	
	return pdFALSE;
}

static BaseType_t prvGetFlightPlan(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	uint32_t flightPlanLen =  uxQueueMessagesWaiting(xFlightPlanQueue);
	
	if (flightPlanLen == 0){
		strncpy(pcWriteBuffer, "Flight plan is empty", xWriteBufferLen);
		return pdFALSE;
	}
	
	double coord[3];
	for (int i = 0; i < flightPlanLen; i++){
		xQueueReceive(xFlightPlanQueue, &coord, 0);
		sprintf(pcWriteBuffer, "x = %.1f, y = %.1f, z = %.1f\n", coord[0], coord[1], coord[2]);
		pcWriteBuffer += strlen(pcWriteBuffer);
		xQueueSend(xFlightPlanQueue, &coord, 0);
	}
	
	return pdFALSE;
}

