function output=getSimulinkOutput(SimModel,PortName,outputData)
%% getSimulinkOutput   Returns data from simulink out port
% inputs: [SimModel, PortName, outputData]
% outputs: [Output of portName in SimModel] 
% 
% Details:
% - SimModel: String with name of simulink model.
% - PortName: Name of output port in simulink we want data from
% - outputData: Struct of outputData from simulation.
%
% Assumes that outputData is a structure with time.
%
%Jon ?ge Stakvik (jas@kelda.no)
%Revision history:    
%   Date:       Who:    Revision:
%   4/16-2015   jas     1.0: Created. (based on work from Vinicius de Oliveira)
%   7/17-2015   jas     1.0: Small update with "lower" in ismember check.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ListOutPorts=find_system(SimModel,'SearchDepth',1, 'regexp', 'on', 'blocktype', 'Outport');
NamePort=strcat(SimModel,'/',PortName);
id= ismember(lower(ListOutPorts),lower(NamePort));

if id == 0
    error('getSimulinkOutput did not find any match. This will fail your test. It is possible that you have a newline in your simulink output port name, or just the wrong block name');
end
    
output = squeeze(outputData.signals(id).values); % Squeeze output to remove higher dimentions if possible.