clear

load('parameters.mat');
m = params.m;
L = params.L;
g = params.g;
I_x = params.I_x;
I_y = params.I_y;
I_z = params.I_z;
M_c = params.M_c;
T_c = params.T_c*3.5;
F_c = params.F_c;
t_delay = params.t_delay;
t_tau = params.t_tau;
u_offset = params.u_offset;
u_max = params.u_max;
%% variance
Q_phi = params.Q_phi;
Q_theta = params.Q_theta;
Q_psi = params.Q_psi;

R_phi = params.R_phi;
R_theta = params.R_theta;
R_psi = params.R_psi;

R_x = params.R_x;
R_y = params.R_y;
R_z = params.R_z;
Q_x = params.Q_x;
Q_y = params.Q_y;
Q_z = params.Q_z;

%% Orientation Data Processing
xPoly = params.xPoly;
yPoly = params.yPoly;
zPoly = params.zPoly;

%% Orientation controller values

%Velocity controller
Dphi_PID = params.cascade.Dphi_PID;
Dtheta_PID = params.cascade.Dtheta_PID;
Dpsi_PID = params.cascade.Dpsi_PID;

satPhi = params.cascade.satPhi;
satTheta = params.cascade.satTheta;
satPsi = params.cascade.satPsi;

%Position controller
phi_PID = params.cascade.phi_PID;
theta_PID = params.cascade.theta_PID;
psi_PID = params.cascade.psi_PID;

satDphi = params.cascade.satDphi;
satDtheta = params.cascade.satDtheta;
satDpsi = params.cascade.satDpsi;

upper_limit = params.cascade.upper_limit;
lower_limit = params.cascade.lower_limit;

%% Position data prosessing
dt_p = params.dt_p;
dt_p = 0.090;
tau_z = params.tau_z;

%% Position controller paramters
x_PID(1) = 0.4;
x_PID(2) = 0;
x_PID(3) = 0;

y_PID(1) = x_PID(1);
y_PID(2) = x_PID(2);
y_PID(3) = x_PID(3);

z_PID(1) = 0.28;
z_PID(2) = 0;
z_PID(3) = 0;

u_PID(1) = 0.18;
u_PID(2) = 0.1;
u_PID(3) = 0.04;

v_PID(1) = u_PID(1);
v_PID(2) = u_PID(2);
v_PID(3) = u_PID(3);

w_PID(1) = 58;
w_PID(2) = 6;
w_PID(3) = 2;
%%
dt = 0.015;
t_sim = 70;

%%
P_w = 0.075;
P = [0 10 0];
psi_io = 0;

%% Test data
u_temp = m*g/(4*F_c) + 100;
mode = timeseries([0 0 0 0]'*1, [0 1 1 t_sim]);

%Auto, mode = 2 or 3
ry_sp = timeseries([10  10  10  10  10 10 ]'*1, [0 1 1 30 30 t_sim]);
rx_sp = timeseries([10 10 10 10 10 10]'*0, [0 1 1 30 30 t_sim]);
rz_sp = timeseries([4 4 4 4 4 0.2 0.2 0.2]'*1, [0 1 1 15 15 50 50 t_sim]);
% rx_sp = timeseries([0 0 10 10 15 15 5 5]'*1, [0 1 1 15 15 35 35 t_sim]);
% ry_sp = timeseries([0 0 1 1 -12 -12 24 24]'*1, [0 1 1 15 15 35 35 t_sim]);
% rz_sp = timeseries([4 4 4 4 20 20 20 20]'*1, [0 1 1 15 15 35 35 t_sim]);
obj_r = [9; 1; 0];
obj_psi = 0*pi/180;

%SemiAuto, mode = 1
vx_sp = timeseries([0 0 0.1 0.1 0 0]'*0, [0 35 35 40 40 t_sim]);
vy_sp = timeseries([0 0 0.2 0.2 0 0]'*1, [0 31 31 41 41 t_sim]);
vz_sp = timeseries([0 0 1 1 0 0]'*1, [0 4 10 48 49 t_sim]);

%Man, mode = 0
phi_sp = timeseries([0 0 0.4 0.4 0 0]'*1, [0 9.8 10 12 12.8 t_sim]);
theta_sp = timeseries([0 0 0.4 0.4 0 0]'*1, [0 39.8 40 42 42.8 t_sim]);
Dpsi_sp = timeseries([0 0 2 2 0 0]'*0, [0 30 30 40 40 t_sim]);
throttle_man = timeseries([u_temp u_temp]'*1, [0 t_sim]);

Phi_0 = [0;0;0];
dist = timeseries([0 1 1]'*0, [0 5 t_sim]);

%% simulate
model = 'flightController_Harness';
simout = sim(model, 'Solver', 'ode4', 'StopTime', 't_sim', 'FixedStep', 'dt',...
    'SaveOutput', 'on', 'OutputSaveName', 'y', 'SaveFormat', 'StructureWithTime');
output = simout.get('y');

time = output.time;
r = getSimulinkOutput(model, 'r', output);
Dr = getSimulinkOutput(model, 'Dr', output);
Phi = getSimulinkOutput(model, 'Phi', output);
DPhi = getSimulinkOutput(model, 'DPhi', output);
v_SP = getSimulinkOutput(model, 'v_SP', output);
r_SP = getSimulinkOutput(model, 'r_SP', output);
u_Phi = getSimulinkOutput(model, 'u_Phi', output);
Phi_SP = getSimulinkOutput(model, 'Phi_SP', output);
vision = getSimulinkOutput(model, 'vision', output);

%% Plot
figure(1); clf(1); 
subplot(311);hold('on');
grid('on');grid('minor');
plot(time, r(:,1));
plot(time, r_SP(:,1));
ylabel('Position [m]');
legend('x', 'x_sp');

subplot(312);hold('on');
grid('on');grid('minor');
plot(time, r(:,2));
plot(time, r_SP(:,2));
ylabel('Position [m]');
legend('y', 'y_sp');

subplot(313);hold('on');
grid('on');grid('minor');
plot(time, r(:,3));
plot(time, r_SP(:,3));
ylabel('Position [m]');
legend('z', 'z_sp');
xlabel('time');

figure(2); clf(2); 
subplot(311);hold('on');
grid('on');grid('minor');
plot(time, Dr(:,1));
plot(time, v_SP(:,1));
ylabel('Velocity [m/s]');
legend('x', 'x_sp');

subplot(312);hold('on');
grid('on');grid('minor');
plot(time, Dr(:,2));
plot(time, v_SP(:,2));
ylabel('Velocity [m/s]');
legend('y', 'y_sp');

subplot(313);hold('on');
grid('on');grid('minor');
plot(time, Dr(:,3));
plot(time, v_SP(:,3));
ylabel('Velocity [m/s]');
legend('z', 'z_sp');
xlabel('time');

figure(3); clf(3);
subplot(311);hold('on');
grid('on');grid('minor');
plot(time, Phi(:,1));
plot(time, Phi_SP(:,1));
title('Angular position')
ylabel('Position [Rad]');
legend('mod. phi','SP');

subplot(312); hold('on');
grid('on');grid('minor');
plot(time, Phi(:,2));
plot(time, Phi_SP(:,2));
ylabel('Position [Rad]');
legend('mod. theta', 'SP');

subplot(313); hold('on');
grid('on');grid('minor');
plot(time, Phi(:,3));
plot(time, Phi_SP(:,3));
ylabel('Position [Rad]');
xlabel('time [s]');
legend('mod. psi', 'SP');

figure(4); clf(4);
subplot(311); hold('on');
grid('on');grid('minor');
plot(time,DPhi(:,1));
plot(time, u_Phi(:,1));
title('Angular velocity')
ylabel('Velocity [Rad/s]');
legend('mod. Dphi', 'mod. SP');

subplot(312); hold('on');
grid('on');grid('minor');
plot(time,DPhi(:,2));
plot(time, u_Phi(:,2));
ylabel('Velocity [Rad/s]');
legend('mod. Dtheta',  'mod. SP');

subplot(313); hold('on');
grid('on');grid('minor');
plot(time,DPhi(:,3));
plot(time, u_Phi(:,3));
ylabel('Velocity [Rad/s]');
xlabel('time [s]');
legend('mod. Dpsi', 'mod. SP');

datasize = size(r);
dist = sum(sqrt(diff(r(:,1)).^2 + diff(r(:,2)).^2 + diff(r(:,3)).^2))

figure(5); clf(5); hold('on');
axis('equal');grid('on');grid('minor');
plot(r(:, 1), r(:, 2))
scatter(r_SP(:, 1), r_SP(:, 2))
text(r(1, 1), r(1, 2),'\leftarrow Start','FontWeight', 'bold', 'FontSize', 12);
text(r(datasize(1), 1), r(datasize(1), 2),'\leftarrow Stop','FontWeight', 'bold', 'FontSize', 12);
ylabel('North \rightarrow', 'FontSize', 16);
xlabel('East \rightarrow', 'FontSize', 16);
legend(strcat('r: ', num2str(dist), 'm'), 'r_{SP}');

figure(6); clf(6);
subplot(311); hold('on');
grid('on');grid('minor');
plot(time,vision(:,4));
plot(time, vision(:,1));
ylabel('Position [m]');
legend('x^i_{ob}', 'mod. x^i_{ob}');
xlim([12 65]);

subplot(312); hold('on');
grid('on');grid('minor');
plot(time,vision(:,5));
plot(time, vision(:,2));
ylabel('Position [m]');
legend('y^i_{ob}', 'mod. y^i_{ob}');
xlim([12 65]);

subplot(313); hold('on');
grid('on');grid('minor');
plot(time, vision(:,6));
plot(time, vision(:,3));
ylabel('Position [m]');
xlabel('Time [s]');
legend('z^i_{ob}', 'mod. z^i_{ob}');
xlim([12 65]);
ylim([0 5]);