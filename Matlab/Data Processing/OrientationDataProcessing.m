%clear
dt = 0.015;
load('parameters.mat');

%% variance
load('noiseMes.mat');

sensorVar = var(noiseMes(1:990, :));
R_phi = diag([sensorVar(1) sensorVar(2)]);
R_theta = diag([sensorVar(3) sensorVar(4)]);
R_psi = diag([sensorVar(10) sensorVar(8)]);

%DPhi tuning
noiseMes(1070:2000,2) = noiseMes(1070:2000,2);%*0.6;
noiseMes(1070:2000,4) = noiseMes(1070:2000,4);%*0.6;
noiseMes(1070:2000,8) = noiseMes(1070:2000,8);%*0.6;

%Phi tuning
noiseMes(1070:2000,1) = noiseMes(1070:2000,1);%*0.15;
noiseMes(1070:2000,3) = noiseMes(1070:2000,3);%*0.15;
noiseMes(1070:2000,10) = noiseMes(1070:2000,10)*25;%*0.4;

systemVar = var(noiseMes(1070:2000,:));
Q_phi = diag([systemVar(1) systemVar(2)*0.02]);
Q_theta = diag([systemVar(3) systemVar(4)*0.02]);
Q_psi = diag([systemVar(10) systemVar(8)*0.02]);

params.R_phi = R_phi;
params.R_theta = R_theta;
params.R_psi = R_psi;
params.Q_phi = Q_phi;
params.Q_theta = Q_theta;
params.Q_psi = Q_psi;

%% Test data
load('orDataYawNoProp.mat');
timeSer = orDataYawNoProp;

%% Least square
load('compassThrottleRampTestWithProp2.mat');
leastSquere = compassThrottleRampTestWithProp2;

x = leastSquere(252:1313, 7);

cx = leastSquere(252:1313,1);
cy = leastSquere(252:1313,2);
cz = leastSquere(252:1313,3);

cx = cx - ones(size(cx))*leastSquere(202,1);
cy = cy - ones(size(cy))*leastSquere(202,2);
cz = cz - ones(size(cz))*leastSquere(202,3);

xPoly = polyfit(x, cx, 2);
yPoly = polyfit(x, cy, 2);
zPoly = polyfit(x, cz, 2);

params.xPoly = xPoly;
params.yPoly = yPoly;
params.zPoly = zPoly;

poly_x = polyval(xPoly,x);
poly_y = polyval(yPoly,x);
poly_z = polyval(zPoly,x);

%% Save filter parameters
save('D:\Dropbox\Skole\10.Semester\Matlab\Common\parameters.mat','params')

%% Ploting for thieses
% figure(1); clf(1); 
% subplot(211);hold('on');
% grid('on');grid('minor');
% plot(leastSquere(:,8), leastSquere(:, 7))
% ylabel('Motor throttle (0 - 1000)');
% subplot(212);hold('on');
% plot(leastSquere(:,8), leastSquere(:,1));
% plot(leastSquere(:,8), leastSquere(:,2));
% plot(leastSquere(:,8), leastSquere(:,3));
% ylabel('Compass raw values');
% xlabel('Time [s]');
% legend('X', 'Y', 'Z');
% 
% figure(2); clf(2); hold('on');
% grid('on');grid('minor');
% plot(leastSquere(:,8), leastSquere(:,1)- leastSquere(1,1));
% plot(leastSquere(:,8), leastSquere(:,2)- leastSquere(1,2));
% plot(leastSquere(:,8), leastSquere(:,3)- leastSquere(1,3));
% plot(leastSquere(252:1313,8),poly_x, 'r--', 'lineWidth', 2);
% plot(leastSquere(252:1313,8),poly_y, 'g--', 'lineWidth', 2);
% plot(leastSquere(252:1313,8),poly_z, 'b--', 'lineWidth', 2);
% ylabel('Compass raw values');
% xlabel('Time [s]');
% legend('X', 'Y', 'Z', 'X polyfit', 'Y polyfit', 'Z polyfit');

%%

input = timeseries([timeSer(:,1), timeSer(:,2), timeSer(:,3)...
    timeSer(:,4), timeSer(:,5), timeSer(:,6),...
    timeSer(:,7), timeSer(:,8), timeSer(:,9), timeSer(:,10)], timeSer(:,11));

%For Theies
% input = timeseries([timeSer(:,1), timeSer(:,2), timeSer(:,3)...
%     timeSer(:,5), timeSer(:,6), timeSer(:,7),...
%     timeSer(:,1), timeSer(:,2), timeSer(:,3), timeSer(:,7)], timeSer(:,9));

timeSize = input.Time(size(input.Time));
t_sim = timeSize(1);
model = 'OrientationDataProcessing_Harness';
simout = sim(model, 'Solver', 'ode4', 'StopTime', 't_sim', 'FixedStep', 'dt',...
    'SaveOutput', 'on', 'OutputSaveName', 'y', 'SaveFormat', 'StructureWithTime');
output = simout.get('y');

time = output.time;
data = output.signals.values;

%% For thieses
% figure(3); clf(3);
% subplot(311); hold on
% grid('on');grid('minor');
% plot(leastSquere(:,8), leastSquere(:,1));
% plot(time, data(:,7));
% legend('uncompensated', 'compensated');
% ylabel('X-axis');
% 
% subplot(312); hold on
% grid('on');grid('minor');
% plot(leastSquere(:,8), leastSquere(:,2));
% plot(time, data(:,8));
% legend('uncompensated', 'compensated');
% ylabel('Y-axis');
% 
% subplot(313); hold on
% grid('on');grid('minor');
% plot(leastSquere(:,8), leastSquere(:,3));
% plot(time, data(:,9));
% legend('uncompensated', 'compensated');
% ylabel('Z-axis');
% xlabel('Time [s]');

%% Plot
figure(1); clf(1);
subplot(311);hold('on');
grid('on');grid('minor');
plot(time, data(:,7));
plot(time, data(:,1));
title('Angular position')
ylabel('Position [Rad]');
legend('phi','phi, filtered');

subplot(312); hold('on');
grid('on');grid('minor');
plot(time, data(:,8));
plot(time, data(:,2));
ylabel('Position [Rad]');
legend('theta','theta, filtered');

subplot(313); hold('on');
grid('on');grid('minor');
plot(time, data(:,9));
plot(time, data(:,3));
ylabel('Position [Rad]');
xlabel('time [s]');
legend('psi','psi, filtered');

figure(2); clf(2);
subplot(311);hold('on');
grid('on');grid('minor');
plot(time, data(:,10));
plot(time, data(:,4));
title('Angular velocity')
ylabel('Velocity [Rad/s]');
legend('phi','phi, filtered');

subplot(312); hold('on');
grid('on');grid('minor');
plot(time, data(:,11));
plot(time, data(:,5));
ylabel('Velocity [Rad/s]');
legend('theta','theta, filtered');

subplot(313); hold('on');
grid('on');grid('minor');
plot(time, data(:,12));
plot(time, data(:,6));
ylabel('Velocity [Rad/s]');
xlabel('time [s]');
legend('psi','psi, filtered');

%% Report
% len = size(time);
% figure(3); clf(3); hold('on');
% grid('on');grid('minor');
% plot(time - 3.509*ones(len), data(:,13), 'linewidth', 1);
% plot(time - 3.509*ones(len), data(:,14), 'linewidth', 2);
% ylabel('Heading [Rad]');
% xlabel('Time [s]');
% legend('Heading w/ rollover','Absolute heading');
% xlim([0 20]);

