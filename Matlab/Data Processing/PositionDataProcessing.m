clear
dt_p = 0.09;
load('parameters.mat');

%% Parameters
tau_z = 0.6;
params.tau_z = tau_z;
params.dt_p = dt_p;

g = params.g;
F_c = params.F_c;
m = params.m;
%% variance
load('posNoiseMes.mat');

sensorVar = var(posNoiseMes(1:140, :));
R_x = diag([sensorVar(5) sensorVar(8)]);
R_y = diag([sensorVar(6) sensorVar(9)]);
R_z = diag([sensorVar(7) sensorVar(10)]);

%a tuning
posNoiseMes(160:300,8) = posNoiseMes(160:300,8)*0.05;%*0.005;
posNoiseMes(160:300,9) = posNoiseMes(160:300,9)*0.05;
posNoiseMes(160:300,10) = posNoiseMes(160:300,10)*0.003;%*0.6;

%v tuning
posNoiseMes(160:300,5) = posNoiseMes(160:300,5)*0.6;
posNoiseMes(160:300,6) = posNoiseMes(160:300,6)*0.6;%*0.6;
posNoiseMes(160:300,7) = posNoiseMes(160:300,7)*4;

systemVar = var(posNoiseMes(160:300,:));
Q_x = diag([systemVar(5) systemVar(8)]);
Q_y = diag([systemVar(6) systemVar(9)]);
Q_z = diag([systemVar(7) systemVar(10)]);

params.R_x = R_x;
params.R_y = R_y;
params.R_z = R_z;
params.Q_x = Q_x;
params.Q_y = Q_y;
params.Q_z = Q_z;

%% Save filter parameters
save('D:\Dropbox\Skole\10.Semester\Matlab\Common\parameters.mat','params')

%% Test data
load('GPSflight2.mat');
timeSer = GPSflight2;

%% Simulate

input = timeseries([timeSer(:,1), timeSer(:,2), timeSer(:,4)...
    timeSer(:,5), timeSer(:,6), timeSer(:,7),...
    timeSer(:,8), timeSer(:,9), timeSer(:,10),...
    timeSer(:,11), timeSer(:,12), timeSer(:,13), timeSer(:,14)], timeSer(:,15));

timeSize = input.Time(size(input.Time));
t_sim = timeSize(1);
model = 'PositionDataProcessing_Harness';
simout = sim(model, 'Solver', 'ode4', 'StopTime', 't_sim', 'FixedStep', 'dt_p',...
    'SaveOutput', 'on', 'OutputSaveName', 'y', 'SaveFormat', 'StructureWithTime');
output = simout.get('y');

time = output.time;
data = output.signals.values;

%% Plot
figure(3); clf(3);
subplot(311);hold('on');
grid('on');grid('minor');
plot(time, data(:,1));
ylabel('Position [m]');
legend('x');

subplot(312); hold('on');
grid('on');grid('minor');
plot(time, data(:,2));
ylabel('Position [m]');
legend('y');

subplot(313); hold('on');
grid('on');grid('minor');
plot(input.Time,input.Data(:,3));
plot(input.Time,timeSer(:,3));
plot(time, data(:,3));
xlabel('Time [s]');
ylabel('Position [m]');
legend('z Barometer', 'z GPS', 'z filtered');

figure(4); clf(4);
subplot(311);hold('on');
grid('on');grid('minor');
plot(input.Time,input.Data(:,4));
plot(time, data(:,4));
ylabel('Velocity [m/s]');
legend('x', 'x filtered');

subplot(312); hold('on');
grid('on');grid('minor');
plot(input.Time,input.Data(:,5));
plot(time, data(:,5));
ylabel('Velocity [m/s]');
legend('y', 'y filtered');

subplot(313); hold('on');
grid('on');grid('minor');
plot(input.Time,input.Data(:,6));
plot(time, data(:,6));
xlabel('Time [s]');
ylabel('Velocity [m/s]');
legend('z', 'z filtered');

figure(5); clf(5);
subplot(311);hold('on');
grid('on');grid('minor');
plot(time, data(:,10));
plot(time, data(:,7));
ylabel('Acceleration [m/s^2]');
legend('x', 'x filtered');

subplot(312); hold('on');
grid('on');grid('minor');
plot(time, data(:,11));
plot(time, data(:,8));
ylabel('Acceleration [m/s^2]');
legend('y', 'y filtered');

subplot(313); hold('on');
grid('on');grid('minor');
plot(time, data(:,12));
plot(time, data(:,9));
xlabel('Time [s]');
ylabel('Acceleration [m/s^2]');
legend('z', 'z filtered');

% figure(6); clf(6);
% grid('on');grid('minor');
% scatter3(input.Data(:,1), input.Data(:,2), input.Data(:,4));

figure(7); clf(7);
%subplot(311); 
hold('on');
grid('on');grid('minor');
stairs(input.Time,input.Data(:,3));
stairs(time, data(:,3), 'lineWidth', 2);
ylabel('Position [m]');
legend('z', 'z, filtered');
xlim([0 30]);
% set(gcf, 'Units', 'normal');
% set(gca, 'Position', [0.09 0.13 0.89 0.85]);

% subplot(312);
% hold('on');
% grid('on');grid('minor');
% stairs(input.Time,input.Data(:,5));
% stairs(time, data(:,5), 'lineWidth', 2);
% ylabel('Velocity [m/s]');
% legend('y, GNSS', 'y, filtered');
% xlim([0 30]);
% set(gcf, 'Units', 'normal');
% set(gca, 'Position', [0.09 0.13 0.89 0.85]);

% subplot(313); hold('on');
% grid('on');grid('minor');
% stairs(time, data(:,2));
% ylabel('Position [m]');
% legend('y, GNSS');
% xlim([0 30]);
% set(gcf, 'Units', 'normal');
% set(gca, 'Position', [0.09 0.13 0.89 0.85]);
% xlabel('time [s]');

% figure(6); clf(6);
% subplot(311);hold('on')
% plot(input.Time,input.Data(:,4));
% plot(time, data(:,4));
% ylabel('Velocity [m/s]');
% legend('x, unfiltered','x, filtered');
% 
% subplot(312); 
% hold('on');
% plot(input.Time,input.Data(:,5));
% plot(time, data(:,5));
% ylabel('Velocity [m/s]');
% legend('y, unfiltered','y, filtered');
% 
% subplot(313); 
% hold('on');
% plot(input.Time,input.Data(:,6));
% plot(time, data(:,6));
% ylabel('Velocity [m/s]');
% xlabel('time [s]');
% legend('z, unfiltered','z, filtered');
% 
% figure(7); clf(7);
% subplot(311);hold('on')
% plot(input.Time,input.Data(:,7));
% plot(time, data(:,7));
% ylabel('Acceleration [m/s^2]');
% legend('x, unfiltered','x, filtered');
% 
% subplot(312); 
% hold('on');
% plot(input.Time,input.Data(:,8)*g);
% plot(time, data(:,8));
% ylabel('Acceleration [m/s^2]');
% legend('y, unfiltered','y, filtered');
% 
% subplot(313); 
% hold('on');
% plot(input.Time,input.Data(:,9));
% plot(time, data(:,9));
% ylabel('Acceleration [m/s^2]');
 xlabel('Time [s]');
% legend('z, unfiltered','z, filtered');