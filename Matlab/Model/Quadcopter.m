clear
load('parameters.mat');
%% Rotation matrix

phi = sym('phi');
theta = sym('theta');
psi = sym('psi');

R_x(phi) = [1 0 0; 0 cos(phi) -sin(phi); 0 sin(phi) cos(phi)];
R_y(theta) = [cos(theta) 0 sin(theta); 0 1 0; -sin(theta) 0 cos(theta)];
R_z(psi) = [cos(psi) -sin(psi) 0; sin(psi) cos(psi) 0; 0 0 1];

Ri_b(phi, theta, psi) = R_z(psi)*R_x(phi)*R_y(theta);
Rb_i(phi, theta, psi) = Ri_b(phi, theta, psi)';

params.R_x = R_x;
params.R_y = R_y;
params.R_z = R_z;
params.Ri_b = Ri_b;
params.Rb_i = Rb_i;
%% Euler Angles p246
Dphi = sym('Dphi');
Dtheta = sym('Dtheta');
Dpsi = sym('Dpsi');

wb_ib = R_y(-theta)*R_x(-phi)*[0; 0; Dpsi] + R_y(-theta)*[Dphi; 0; 0] + [0; Dtheta; 0];
E_b = [cos(theta) 0 -cos(phi)*sin(theta); 0 1 sin(phi); sin(theta) 0 cos(phi)*cos(theta)];

E_b_inv = inv(E_b); %DPhi = simplify(inv(E_b)*[Dphi Dtheta Dpsi]');

params.E_b_inv = E_b_inv;
%% Equation of motion for a rigid body p269
% Constants
m = 1.066;          %Total mass [kg]
m_p = 0.118;        %Mass of motor and propellar [kg]
m_c = m - 4*m_p;    %Center mass [kg]

L = 0.226;      %Length from center mass to out propellers, and outer masses
g = 9.81;       %Gravity [m/s^2]

% Intertia matrix p14 (quadrotor paper), p274, wikipedia; List of interta
% Point mass at a distance, cylinder
r_cylinder = 0.06;
h_cylinder = 0.08;
I_z = L^2*m_p*4 + (m_c*r_cylinder^2)/2;

params.m = m;
params.L = L;
params.g = g;
%% Motor model (Force and Torque paramters)
%Test paramteters:
test_m = 0.970;
test_m_p = 0.108;
test_m_c = m - 4*m_p;
test_r_cylinder = 0.06;
test_h_cylinder = 0.05;

test_I_z = L^2*test_m_p*4 + (test_m_c*test_r_cylinder^2)/2;

u_offset = 100;
u_max = 1000;
u_torque = 200;
t_tau = 0.01;
t_delay = 0.07;

t_avg = (2.73 + 2.66 + 3.14 + 3.12)/4;

T_c = test_I_z*2*2*pi/(t_avg^2*u_torque*2);
F_c = 0.320/(350 - 100)*g;

avg_t = (4.88+4.76+4.77+4.50+4.55+5.39+5.14+5.79+5.52+4.82+4.34)/11;
I_x = F_c*L*avg_t^2/(2*sqrt(2)*2*pi)*8;
I_y = I_x;
M_c = [I_x 0 0; 0 I_y 0; 0 0 I_z];

params.I_x = I_x;
params.I_y = I_y;
params.I_z = I_z;
params.M_c = M_c;
params.F_c = F_c;
params.T_c = T_c;
params.t_tau = t_tau;
params.t_delay = t_delay;
params.u_offset = u_offset;
params.u_max = u_max;
%%
dt = 0.015;
t_sim = 30;
%% Test data
u_temp = m*g/(4*F_c);
u1 = timeseries([u_temp u_temp u_temp u_temp], [0 2 3 t_sim]);
u2 = timeseries([u_temp u_temp u_temp u_temp], [0 2 3 t_sim]);
u3 = timeseries([u_temp u_temp u_temp u_temp], [0 2 3 t_sim]);
u4 = timeseries([u_temp u_temp u_temp u_temp], [0 2 3 t_sim]);

Phi_0 = [0;0;0];

%% Save model paramters
save('D:\Dropbox\Skole\10.Semester\Matlab\Common\parameters.mat','params')

%% simulate
params = 'Quadcopter_Harness';
simout = sim(params, 'Solver', 'ode4', 'StopTime', 't_sim', 'FixedStep', 'dt',...
    'SaveOutput', 'on', 'OutputSaveName', 'y', 'SaveFormat', 'StructureWithTime');
output = simout.get('y');

time = output.time;
Dr = getSimulinkOutput(params, 'Dr', output);
Phi = getSimulinkOutput(params, 'Phi', output);
DPhi = getSimulinkOutput(params, 'DPhi', output);

%% Plot
figure(1); clf(1);
plot(time, Dr(:,1),'b',time, Dr(:,2), 'r', time, Dr(:,3), 'm');
title('Velocity')
ylabel('Velocity [m/s]');
legend('x','y','z');

figure(2); clf(2);
plot(time, Phi(:,1),'b',time, Phi(:,2), 'r', time, Phi(:,3), 'm');
title('Orientation')
ylabel('Position [Rad]');
legend('phi','theta', 'psi');

figure(3); clf(3);
plot(time, DPhi(:,1),'b',time, DPhi(:,2), 'r', time, DPhi(:,3), 'm');
title('Angular velocity')
ylabel('Velocity [Rad/s]');
legend('Dphi','Dtheta', 'Dpsi');