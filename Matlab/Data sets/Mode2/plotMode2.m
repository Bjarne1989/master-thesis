%clear

load('mode1report.mat')
data = mode1report;

figure(1); clf(1); 
subplot(311);hold('on');
grid('on');grid('minor');
plot(data(:, 25), data(:, 1));
plot(data(:, 25), data(:, 2));
ylabel('Position [m]');
legend('x', 'x_{SP}');
xlim([3 70]);
ylim([-5 15]);

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, 25), data(:, 3));
plot(data(:, 25), data(:, 4));
ylabel('Position [m]');
legend('y', 'y_{SP}');
xlim([3 70]);
ylim([-5 15]);

subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, 25), data(:, 5));
plot(data(:, 25), data(:, 6));
ylabel('Position [m]');
legend('z', 'z_{SP}');
xlabel('Time [s]');
xlim([3 70]);
ylim([-2 5]);

figure(2); clf(2); 
subplot(311);hold('on');
grid('on');grid('minor');
plot(data(:, 25), data(:, 7));
plot(data(:, 25), data(:, 8));
ylabel('Velocity [m/s]');
legend('x', 'x_{SP}');
xlim([0 50]);
ylim([-3 2]);

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, 25), data(:, 9));
plot(data(:, 25), data(:, 10));
ylabel('Velocity [m/s]');
legend('y', 'y_{SP}');
xlim([0 50]);
ylim([-3 2]);

subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, 25), data(:, 11));
plot(data(:, 25), data(:, 12));
ylabel('Velocity [m/s]');
legend('z', 'z_{SP}');
xlabel('Time [s]');
xlim([0 50]);

figure(3); clf(3); 
% subplot(311);
hold('on');
% grid('on');grid('minor');
% plot(data(:, 25), data(:, 13));
% plot(data(:, 25), data(:, 14));
% ylabel('Pos. [Rad]');
% legend('phi', 'phi_{SP}');
% 
% subplot(312);hold('on');
% grid('on');grid('minor');
% plot(data(:, 25), data(:, 15));
% plot(data(:, 25), data(:, 16));
% ylabel('Pos. [Rad]');
% legend('theta', 'theta_{SP}');

%subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, 25), data(:, 17));
plot(data(:, 25), data(:, 18));
ylabel('Pos. [Rad]');
legend('psi', 'psi_{SP}');
xlabel('Time [s]');
ylim([-12 -5]);
xlim([3 70]);

figure(4); clf(4); 
subplot(311);hold('on');
grid('on');grid('minor');
plot(data(:, 25), data(:, 19));
plot(data(:, 25), data(:, 20));
ylabel('Vel. [Rad/s]');
legend('Dphi', 'Dphi_{SP}');

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, 25), data(:, 21));
plot(data(:, 25), data(:, 22));
ylabel('Vel. [Rad/s]');
legend('Dtheta', 'Dtheta_{SP}');

subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, 25), data(:, 23));
plot(data(:, 25), data(:, 24));
ylabel('Vel. [Rad/s]');
legend('Dpsi', 'Dpsi_{SP}');
xlabel('Time [s]');

% figure(5); clf(5);
% title('Propellar power')
% grid('on');grid('minor');
% stairs(data(:,29), data(:,25:28));
% ylabel('Power 0-1000');
% xlabel('Time [s]');
% legend('FL', 'FR', 'RL', 'RR');

datasize = size(data);

dist = sum(sqrt(diff(data(:,1)).^2 + diff(data(:,3)).^2 + diff(data(:,5)).^2))

figure(5); clf(5); hold('on');
axis('equal');grid('on');grid('minor');
plot(data(:, 1), data(:, 3))
scatter(data(:, 2), data(:, 4))
text(data(1, 1), data(1, 3),'\leftarrow Start','FontWeight', 'bold', 'FontSize', 12);
text(data(datasize(1), 1), data(datasize(1), 3),'\leftarrow Stop','FontWeight', 'bold', 'FontSize', 12);
ylabel('North \rightarrow', 'FontSize', 16);
xlabel('East \rightarrow', 'FontSize', 16);
legend('r', 'r_{SP}');
ylim([-1 11]);
