load('batTestAndAcc.mat');
figure(1); clf(1); hold('on');

sizeOf = 328;%3287;

limBat = zeros(sizeOf,1);
limBatT = limBat;

for i=1:1:sizeOf-1
    limBat(i) = batTestAndAcc(i*100,1);
    limBatT(i) = batTestAndAcc(i*100,5);
end

dataPoints = ones(sizeOf-1,1);

plot(limBatT(1:size(limBatT)-1), limBat(1:size(limBatT)-1));
plot(limBatT(1:size(limBatT)-1), dataPoints*10.45, 'y', 'LineWidth', 2);
plot(limBatT(1:size(limBatT)-1), dataPoints*10.32, 'r', 'LineWidth', 2);
ylim([9.5 11.5]);
set(gca, 'Ytick', [9.5 9.75 10 10.25 10.5 10.75 11 11.25 11.5]);
ylabel('Voltage [V]');
xlabel('Time [s]');
legend('Voltage', 'Low', 'Low Low')