%clear

load('mode0benchtest.mat')
data = unnamed;
timeData = 22;

figure(1); clf(1); 
subplot(311);
hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 1));
plot(data(:, timeData), data(:, 2));
ylabel('Pos. [Rad]');
legend('phi', 'phi_{sp}');
xlim([2 22]);

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 3));
plot(data(:, timeData), data(:, 4));
ylabel('Pos. [Rad]');
legend('theta', 'theta_{sp}');
xlim([2 22]);

subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 5));
plot(data(:, timeData), data(:, 6));
ylabel('Pos. [Rad]');
legend('psi', 'psi_{sp}');
 xlabel('Time [s]');

figure(2); clf(2); 
subplot(311);
hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 7));
plot(data(:, timeData), data(:, 8));
ylabel('Vel. [Rad/s]');
legend('phi', 'phi_{sp}');

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 9));
plot(data(:, timeData), data(:, 10));
ylabel('Vel. [Rad/s]');
legend('theta', 'theta_{sp}');

subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 11));
plot(data(:, timeData), data(:, 12));
ylabel('Vel. [Rad/s]');
legend('psi', 'psi_{sp}');
 xlabel('Time [s]');

figure(3); clf(3); 
subplot(311);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 13));
plot(data(:, timeData), data(:, 16));
plot(data(:, timeData), data(:, 19));
ylabel('Proporsional');
legend('phi', 'theta', 'psi');

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 16));
plot(data(:, timeData), data(:, 17));
plot(data(:, timeData), data(:, 18));
ylabel('Integral');
legend('phi', 'theta', 'psi');

subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 19));
plot(data(:, timeData), data(:, 20));
plot(data(:, timeData), data(:, 21));
ylabel('D');
legend('phi', 'theta', 'psi');
xlabel('Time [s]');

figure(4); clf(4); 
subplot(311);
hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 13) + data(:, 16) + data(:, 19));
ylabel('Power');
legend('u_{phi}');

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 14) + data(:, 17) + data(:, 20));
ylabel('Power');
legend('u_{theta}');

subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 15) + data(:, 18) + data(:, 21));
ylabel('Power');
legend('u_{psi}');
xlabel('Time [s]');

figure(5); clf(5); 
subplot(311);
hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 1));
plot(data(:, timeData), data(:, 2));
ylabel('Pos. [Rad]');
legend('phi', 'phi_{sp}');

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 7));
plot(data(:, timeData), data(:, 8));
ylabel('Vel. [Rad/s]');
legend('phi', 'phi_{sp}');


subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, timeData), data(:, 13) + data(:, 16) + data(:, 19));
ylabel('Power');
legend('u_{phi}');
 xlabel('Time [s]');