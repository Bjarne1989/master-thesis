load('compassXYcalibrated.mat');
load('compassXYuncalibrated.mat');

figure(1); clf(1); hold('on');
axis equal
scatter(compassXYuncalibrated(:,1),compassXYuncalibrated(:,2));
scatter(compassXYcalibrated(:,1),compassXYcalibrated(:,2));
plot([0 0], [-900, 400], 'k--');
plot([-1400 200], [0, 0], 'k--');
xlim([-1400 200]);
ylim([-700 300]);
ylabel('y');
xlabel('x');
legend('not calibrated','calibrated');