%clear
time = 23;
load('longFlight.mat')
data = longFlight;

figure(1); clf(1); 
subplot(311);hold('on');
grid('on');grid('minor');
plot(data(:, time), data(:, 1));
plot(data(:, time), data(:, 2));
ylabel('Position [m]');
legend('x', 'x_{SP}');

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, time), data(:, 3));
plot(data(:, time), data(:, 4));
ylabel('Position [m]');
legend('y', 'y_{SP}');

subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, time), data(:, 5));
plot(data(:, time), data(:, 6));
ylabel('Position [m]');
legend('z', 'z_{SP}');
xlabel('Time [s]');

figure(2); clf(2); 
subplot(311);hold('on');
grid('on');grid('minor');
plot(data(:, time), data(:, 7));
plot(data(:, time), data(:, 8));
ylabel('Velocity [m/s]');
legend('x', 'x_{SP}');

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, time), data(:, 9));
plot(data(:, time), data(:, 10));
ylabel('Velocity [m/s]');
legend('y', 'y_{SP}');

subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, time), data(:, 11));
plot(data(:, time), data(:, 12));
ylabel('Velocity [m/s]');
legend('z', 'z_{SP}');
xlabel('Time [s]');

figure(3); clf(3); 
subplot(311);hold('on');
grid('on');grid('minor');
plot(data(:, time), data(:, 13));
plot(data(:, time), data(:, 14));
ylabel('Pos. [Rad]');
legend('phi', 'phi_{SP}');

subplot(312);hold('on');
grid('on');grid('minor');
plot(data(:, time), data(:, 15));
plot(data(:, time), data(:, 16));
ylabel('Pos. [Rad]');
legend('theta', 'theta_{SP}');

subplot(313);hold('on');
grid('on');grid('minor');
plot(data(:, time), data(:, 17));
plot(data(:, time), data(:, 18));
ylabel('Pos. [Rad]');
legend('psi', 'psi_{SP}');
xlabel('Time [s]');

% figure(4); clf(4); 
% subplot(311);hold('on');
% grid('on');grid('minor');
% plot(data(:, time), data(:, 19));
% plot(data(:, time), data(:, 20));
% ylabel('Vel. [Rad/s]');
% legend('Dphi', 'Dphi_{SP}');
% 
% subplot(312);hold('on');
% grid('on');grid('minor');
% plot(data(:, time), data(:, 21));
% plot(data(:, time), data(:, 22));
% ylabel('Vel. [Rad/s]');
% legend('Dtheta', 'Dtheta_{SP}');
% 
% subplot(313);hold('on');
% grid('on');grid('minor');
% plot(data(:, time), data(:, 23));
% plot(data(:, time), data(:, 24));
% ylabel('Vel. [Rad/s]');
% legend('Dpsi', 'Dpsi_{SP}');
% xlabel('Time [s]');

figure(4); clf(4);
title('Propellar power')
grid('on');grid('minor');
stairs(data(:,time), data(:,19:22));
ylabel('Power 0-1000');
xlabel('Time [s]');
legend('FL', 'FR', 'RL', 'RR');

datasize = size(data);

dist = sum(sqrt(diff(data(:,1)).^2 + diff(data(:,3)).^2 + diff(data(:,5)).^2))

figure(5); clf(5); hold('on');
axis('equal');grid('on');grid('minor');
plot(data(:, 1), data(:, 3))
scatter(data(:, 2), data(:, 4))
text(data(1, 1), data(1, 3),'\leftarrow Start','FontWeight', 'bold', 'FontSize', 12);
text(data(datasize(1), 1), data(datasize(1), 3),'\leftarrow Stop','FontWeight', 'bold', 'FontSize', 12);
ylabel('North \rightarrow', 'FontSize', 16);
xlabel('East \rightarrow', 'FontSize', 16);
legend(strcat('r: ', num2str(dist), 'm'), 'r_{SP}');
