/*
 * flightController.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "flightController".
 *
 * Model version              : 1.1032
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Wed Jun 01 12:19:36 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "flightController.h"
#include "flightController_private.h"

static void rate_scheduler(RT_MODEL_flightController_T *const flightController_M);

/*
 *   This function updates active task flag for each subrate.
 * The function is called at model base rate, hence the
 * generated code self-manages all its subrates.
 */
static void rate_scheduler(RT_MODEL_flightController_T *const flightController_M)
{
  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (flightController_M->Timing.TaskCounters.TID[1])++;
  if ((flightController_M->Timing.TaskCounters.TID[1]) > 5) {/* Sample time: [0.09s, 0.0s] */
    flightController_M->Timing.TaskCounters.TID[1] = 0;
  }
}

/*
 * Initial conditions for atomic system:
 *    '<S22>/filterPhi'
 *    '<S22>/filterTheta'
 */
void flightController_filterPhi_Init(DW_filterPhi_flightController_T *localDW)
{
  /* '<S27>:1:7' x_prior_estimate = [0; 0]; */
  localDW->x_prior_estimate[0] = 0.0;
  localDW->x_prior_estimate[1] = 0.0;

  /* '<S27>:1:8' P_prior = [0 0; 0 0]; */
  localDW->P_prior[0] = 0.0;
  localDW->P_prior[1] = 0.0;
  localDW->P_prior[2] = 0.0;
  localDW->P_prior[3] = 0.0;
}

/*
 * Output and update for atomic system:
 *    '<S22>/filterPhi'
 *    '<S22>/filterTheta'
 */
void flightController_filterPhi(real_T rtu_u, real_T rtu_u_i, const real_T
  rtu_Q[4], const real_T rtu_R[4], real_T rtu_dt, B_filterPhi_flightController_T
  *localB, DW_filterPhi_flightController_T *localDW)
{
  real_T A[4];
  real_T K[4];
  real_T a[4];
  real_T r;
  real_T t;
  static const int8_T b[4] = { 1, 0, 0, 1 };

  real_T b_0[4];
  int32_T i;
  real_T c_idx_0;
  real_T c_idx_1;
  real_T c_idx_2;

  /* MATLAB Function 'orDataProcessing/Filter/filterPhi': '<S27>:1' */
  /* '<S27>:1:3' A = [1 dt; 0 1]; */
  A[0] = 1.0;
  A[2] = rtu_dt;
  A[1] = 0.0;
  A[3] = 1.0;

  /* '<S27>:1:4' H = eye(2,2); */
  /* '<S27>:1:6' if isempty(P_prior) */
  /* '<S27>:1:11' K = P_prior*H'*(H*P_prior*H' + R)^-1; */
  for (i = 0; i < 2; i++) {
    b_0[i] = 0.0;
    b_0[i] += (real_T)b[i] * localDW->P_prior[0];
    b_0[i] += (real_T)b[i + 2] * localDW->P_prior[1];
    b_0[i + 2] = 0.0;
    b_0[i + 2] += (real_T)b[i] * localDW->P_prior[2];
    b_0[i + 2] += (real_T)b[i + 2] * localDW->P_prior[3];
  }

  for (i = 0; i < 2; i++) {
    a[i] = (b_0[i + 2] * 0.0 + b_0[i]) + rtu_R[i];
    a[i + 2] = (b_0[i] * 0.0 + b_0[i + 2]) + rtu_R[i + 2];
  }

  if (fabs(a[1]) > fabs(a[0])) {
    r = a[0] / a[1];
    t = 1.0 / (r * a[3] - a[2]);
    c_idx_0 = a[3] / a[1] * t;
    c_idx_1 = -t;
    c_idx_2 = -a[2] / a[1] * t;
    t *= r;
  } else {
    r = a[1] / a[0];
    t = 1.0 / (a[3] - r * a[2]);
    c_idx_0 = a[3] / a[0] * t;
    c_idx_1 = -r * t;
    c_idx_2 = -a[2] / a[0] * t;
  }

  for (i = 0; i < 2; i++) {
    b_0[i] = 0.0;
    b_0[i] += localDW->P_prior[i];
    b_0[i] += localDW->P_prior[i + 2] * 0.0;
    b_0[i + 2] = 0.0;
    b_0[i + 2] += localDW->P_prior[i] * 0.0;
    b_0[i + 2] += localDW->P_prior[i + 2];
  }

  for (i = 0; i < 2; i++) {
    K[i] = 0.0;
    K[i] += b_0[i] * c_idx_0;
    K[i] += b_0[i + 2] * c_idx_1;
    K[i + 2] = 0.0;
    K[i + 2] += b_0[i] * c_idx_2;
    K[i + 2] += b_0[i + 2] * t;
  }

  /* SignalConversion: '<S27>/TmpSignal ConversionAt SFunction Inport1' */
  /* 1 */
  /* '<S27>:1:12' x_estimate = x_prior_estimate + K*(u - H*x_prior_estimate); */
  r = rtu_u - (0.0 * localDW->x_prior_estimate[1] + localDW->x_prior_estimate[0]);
  t = rtu_u_i - (0.0 * localDW->x_prior_estimate[0] + localDW->x_prior_estimate
                 [1]);
  localB->y[0] = (K[0] * r + K[2] * t) + localDW->x_prior_estimate[0];
  localB->y[1] = (K[1] * r + K[3] * t) + localDW->x_prior_estimate[1];

  /* 2 */
  /* '<S27>:1:13' P = (eye(2,2) - K*H)*P_prior; */
  a[1] = 0.0;
  a[2] = 0.0;
  a[0] = 1.0;
  a[3] = 1.0;

  /* 3 */
  /* '<S27>:1:14' x_prior_estimate = A*x_estimate; */
  localDW->x_prior_estimate[0] = 0.0;
  localDW->x_prior_estimate[0] += localB->y[0];
  localDW->x_prior_estimate[0] += rtu_dt * localB->y[1];
  localDW->x_prior_estimate[1] = 0.0;
  localDW->x_prior_estimate[1] += 0.0 * localB->y[0];
  localDW->x_prior_estimate[1] += localB->y[1];

  /* 4 */
  /* '<S27>:1:15' P_prior = A*P*A' + Q; */
  for (i = 0; i < 2; i++) {
    b_0[i] = a[i] - (K[i + 2] * 0.0 + K[i]);
    b_0[i + 2] = a[i + 2] - (K[i] * 0.0 + K[i + 2]);
  }

  for (i = 0; i < 2; i++) {
    a[i] = 0.0;
    a[i] += b_0[i] * localDW->P_prior[0];
    a[i] += b_0[i + 2] * localDW->P_prior[1];
    a[i + 2] = 0.0;
    a[i + 2] += b_0[i] * localDW->P_prior[2];
    a[i + 2] += b_0[i + 2] * localDW->P_prior[3];
  }

  for (i = 0; i < 2; i++) {
    b_0[i] = 0.0;
    b_0[i] += A[i] * a[0];
    b_0[i] += A[i + 2] * a[1];
    b_0[i + 2] = 0.0;
    b_0[i + 2] += A[i] * a[2];
    b_0[i + 2] += A[i + 2] * a[3];
  }

  for (i = 0; i < 2; i++) {
    localDW->P_prior[i] = (b_0[i + 2] * rtu_dt + b_0[i]) + rtu_Q[i];
    localDW->P_prior[i + 2] = (b_0[i] * 0.0 + b_0[i + 2]) + rtu_Q[i + 2];
  }

  /* 4 */
  /* '<S27>:1:17' y = x_estimate; */
}

/*
 * Termination for atomic system:
 *    '<S22>/filterPhi'
 *    '<S22>/filterTheta'
 */
void flightController_filterPhi_Term(void)
{
}

/*
 * Initial conditions for atomic system:
 *    '<S7>/filterX'
 *    '<S7>/filterZ'
 */
void flightController_filterX_Init(DW_filterX_flightController_T *localDW)
{
  /* '<S51>:1:7' x_prior_estimate = [0; 0]; */
  localDW->x_prior_estimate[0] = 0.0;
  localDW->x_prior_estimate[1] = 0.0;

  /* '<S51>:1:8' P_prior = [0 0; 0 0]; */
  localDW->P_prior[0] = 0.0;
  localDW->P_prior[1] = 0.0;
  localDW->P_prior[2] = 0.0;
  localDW->P_prior[3] = 0.0;
}

/*
 * Output and update for atomic system:
 *    '<S7>/filterX'
 *    '<S7>/filterZ'
 */
void flightController_filterX(real_T rtu_z, real_T rtu_z_c, const real_T rtu_Q[4],
  const real_T rtu_R[4], real_T rtu_dt, B_filterX_flightController_T *localB,
  DW_filterX_flightController_T *localDW)
{
  real_T A[4];
  real_T K[4];
  real_T a[4];
  real_T r;
  real_T t;
  static const int8_T b[4] = { 1, 0, 0, 1 };

  real_T b_0[4];
  int32_T i;
  real_T c_idx_0;
  real_T c_idx_1;
  real_T c_idx_2;

  /* MATLAB Function 'poDataProcessing/filterX': '<S51>:1' */
  /* '<S51>:1:3' A = [1 dt; 0 1]; */
  A[0] = 1.0;
  A[2] = rtu_dt;
  A[1] = 0.0;
  A[3] = 1.0;

  /* '<S51>:1:4' H = eye(2,2); */
  /* '<S51>:1:6' if isempty(P_prior) */
  /* '<S51>:1:11' K = P_prior*H'*(H*P_prior*H' + R)^-1; */
  for (i = 0; i < 2; i++) {
    b_0[i] = 0.0;
    b_0[i] += (real_T)b[i] * localDW->P_prior[0];
    b_0[i] += (real_T)b[i + 2] * localDW->P_prior[1];
    b_0[i + 2] = 0.0;
    b_0[i + 2] += (real_T)b[i] * localDW->P_prior[2];
    b_0[i + 2] += (real_T)b[i + 2] * localDW->P_prior[3];
  }

  for (i = 0; i < 2; i++) {
    a[i] = (b_0[i + 2] * 0.0 + b_0[i]) + rtu_R[i];
    a[i + 2] = (b_0[i] * 0.0 + b_0[i + 2]) + rtu_R[i + 2];
  }

  if (fabs(a[1]) > fabs(a[0])) {
    r = a[0] / a[1];
    t = 1.0 / (r * a[3] - a[2]);
    c_idx_0 = a[3] / a[1] * t;
    c_idx_1 = -t;
    c_idx_2 = -a[2] / a[1] * t;
    t *= r;
  } else {
    r = a[1] / a[0];
    t = 1.0 / (a[3] - r * a[2]);
    c_idx_0 = a[3] / a[0] * t;
    c_idx_1 = -r * t;
    c_idx_2 = -a[2] / a[0] * t;
  }

  for (i = 0; i < 2; i++) {
    b_0[i] = 0.0;
    b_0[i] += localDW->P_prior[i];
    b_0[i] += localDW->P_prior[i + 2] * 0.0;
    b_0[i + 2] = 0.0;
    b_0[i + 2] += localDW->P_prior[i] * 0.0;
    b_0[i + 2] += localDW->P_prior[i + 2];
  }

  for (i = 0; i < 2; i++) {
    K[i] = 0.0;
    K[i] += b_0[i] * c_idx_0;
    K[i] += b_0[i + 2] * c_idx_1;
    K[i + 2] = 0.0;
    K[i + 2] += b_0[i] * c_idx_2;
    K[i + 2] += b_0[i + 2] * t;
  }

  /* SignalConversion: '<S51>/TmpSignal ConversionAt SFunction Inport1' */
  /* 1 */
  /* '<S51>:1:12' x_estimate = x_prior_estimate + K*(z - H*x_prior_estimate); */
  r = rtu_z - (0.0 * localDW->x_prior_estimate[1] + localDW->x_prior_estimate[0]);
  t = rtu_z_c - (0.0 * localDW->x_prior_estimate[0] + localDW->x_prior_estimate
                 [1]);
  localB->y[0] = (K[0] * r + K[2] * t) + localDW->x_prior_estimate[0];
  localB->y[1] = (K[1] * r + K[3] * t) + localDW->x_prior_estimate[1];

  /* 2 */
  /* '<S51>:1:13' P = (eye(2,2) - K*H)*P_prior; */
  a[1] = 0.0;
  a[2] = 0.0;
  a[0] = 1.0;
  a[3] = 1.0;

  /* 3 */
  /* '<S51>:1:14' x_prior_estimate = A*x_estimate; */
  localDW->x_prior_estimate[0] = 0.0;
  localDW->x_prior_estimate[0] += localB->y[0];
  localDW->x_prior_estimate[0] += rtu_dt * localB->y[1];
  localDW->x_prior_estimate[1] = 0.0;
  localDW->x_prior_estimate[1] += 0.0 * localB->y[0];
  localDW->x_prior_estimate[1] += localB->y[1];

  /* 4 */
  /* '<S51>:1:15' P_prior = A*P*A' + Q; */
  for (i = 0; i < 2; i++) {
    b_0[i] = a[i] - (K[i + 2] * 0.0 + K[i]);
    b_0[i + 2] = a[i + 2] - (K[i] * 0.0 + K[i + 2]);
  }

  for (i = 0; i < 2; i++) {
    a[i] = 0.0;
    a[i] += b_0[i] * localDW->P_prior[0];
    a[i] += b_0[i + 2] * localDW->P_prior[1];
    a[i + 2] = 0.0;
    a[i + 2] += b_0[i] * localDW->P_prior[2];
    a[i + 2] += b_0[i + 2] * localDW->P_prior[3];
  }

  for (i = 0; i < 2; i++) {
    b_0[i] = 0.0;
    b_0[i] += A[i] * a[0];
    b_0[i] += A[i + 2] * a[1];
    b_0[i + 2] = 0.0;
    b_0[i + 2] += A[i] * a[2];
    b_0[i + 2] += A[i + 2] * a[3];
  }

  for (i = 0; i < 2; i++) {
    localDW->P_prior[i] = (b_0[i + 2] * rtu_dt + b_0[i]) + rtu_Q[i];
    localDW->P_prior[i + 2] = (b_0[i] * 0.0 + b_0[i + 2]) + rtu_Q[i + 2];
  }

  /* 4 */
  /* '<S51>:1:17' y = x_estimate; */
}

/*
 * Termination for atomic system:
 *    '<S7>/filterX'
 *    '<S7>/filterZ'
 */
void flightController_filterX_Term(void)
{
}

real_T rt_atan2d_snf(real_T u0, real_T u1)
{
  real_T y;
  int32_T u0_0;
  int32_T u1_0;
  if (rtIsNaN(u0) || rtIsNaN(u1)) {
    y = (rtNaN);
  } else if (rtIsInf(u0) && rtIsInf(u1)) {
    if (u0 > 0.0) {
      u0_0 = 1;
    } else {
      u0_0 = -1;
    }

    if (u1 > 0.0) {
      u1_0 = 1;
    } else {
      u1_0 = -1;
    }

    y = atan2(u0_0, u1_0);
  } else if (u1 == 0.0) {
    if (u0 > 0.0) {
      y = RT_PI / 2.0;
    } else if (u0 < 0.0) {
      y = -(RT_PI / 2.0);
    } else {
      y = 0.0;
    }
  } else {
    y = atan2(u0, u1);
  }

  return y;
}

/* Model step function */
void flightController_step(RT_MODEL_flightController_T *const flightController_M)
{
  P_flightController_T *flightController_P = ((P_flightController_T *)
    flightController_M->ModelData.defaultParam);
  B_flightController_T *flightController_B = ((B_flightController_T *)
    flightController_M->ModelData.blockIO);
  DW_flightController_T *flightController_DW = ((DW_flightController_T *)
    flightController_M->ModelData.dwork);
  PrevZCX_flightController_T *flightController_PrevZCX =
    ((PrevZCX_flightController_T *) flightController_M->ModelData.prevZCSigState);
  ExtU_flightController_T *flightController_U = (ExtU_flightController_T *)
    flightController_M->ModelData.inputs;
  ExtY_flightController_T *flightController_Y = (ExtY_flightController_T *)
    flightController_M->ModelData.outputs;
  real_T A[4];
  real_T K[4];
  real_T a[4];
  static const int8_T b[4] = { 1, 0, 0, 1 };

  static const int8_T b_0[4] = { 1, 0, 0, 1 };

  boolean_T rtb_LogicalOperator_l;
  real_T rtb_Divide1;
  real_T rtb_Divide;
  real_T rtb_Polynomial2;
  real_T rtb_Divide2;
  real_T rtb_Switch_l[3];
  real_T rtb_DiscreteTimeIntegrator2;
  int32_T rtb_SPreached;
  int32_T rtb_landed;
  real_T rtb_TmpSignalConversionAtDelayI[3];
  boolean_T rtb_Compare_gs;
  boolean_T rtb_Compare_gt;
  boolean_T rtb_Compare_cy;
  int32_T i;
  real_T tmp[3];
  real_T b_1[4];
  real_T a_0[4];
  real_T c[9];
  real_T tmp_0[9];
  real_T c_0[9];
  real_T tmp_1[9];
  int32_T i_0;
  real_T tmp_2[3];
  real_T rtb_MultiportSwitch_idx_0;
  real_T rtb_MultiportSwitch_idx_1;
  real_T rtb_MultiportSwitch_idx_2;
  real_T rtb_Switch_idx_2;
  real_T rtb_Switch2_idx_1;
  real_T rtb_Switch2_idx_2;
  real_T rtb_Switch2_idx_0;
  real_T rtb_Delay_idx_1;
  real_T rtb_Delay_idx_0;
  real_T rtb_Switch_idx_0;
  real_T rtb_Switch_idx_1;
  real_T rtb_Switch1_idx_0;
  real_T rtb_Switch1_idx_1;
  real_T rtb_Switch1_idx_2;
  real_T rtb_y_n_idx_0;
  real_T rtb_y_n_idx_1;
  real_T c_idx_1;
  real_T c_idx_2;
  real_T rtb_SP_idx_0;
  real_T rtb_SP_idx_1;
  real_T rtb_SP_idx_2;

  /* Product: '<S3>/Divide' incorporates:
   *  Constant: '<S3>/Constant2'
   *  Inport: '<Root>/RCctrl'
   */
  rtb_Divide = flightController_U->RCctrl[0] /
    flightController_P->Constant2_Value_i;

  /* Product: '<S3>/Divide1' incorporates:
   *  Constant: '<S3>/Constant2'
   *  Inport: '<Root>/RCctrl'
   */
  rtb_Divide1 = flightController_U->RCctrl[1] /
    flightController_P->Constant2_Value_i;

  /* Product: '<S3>/Divide2' incorporates:
   *  Constant: '<S3>/Constant3'
   *  Inport: '<Root>/RCctrl'
   */
  rtb_Divide2 = flightController_U->RCctrl[2] /
    flightController_P->Constant3_Value;

  /* MultiPortSwitch: '<S3>/Multiport Switch' incorporates:
   *  Constant: '<S3>/Constant4'
   *  Constant: '<S3>/Constant5'
   *  Inport: '<Root>/mode'
   */
  switch ((int32_T)flightController_U->mode) {
   case 0:
    rtb_MultiportSwitch_idx_0 = rtb_Divide;
    rtb_MultiportSwitch_idx_1 = rtb_Divide1;
    rtb_MultiportSwitch_idx_2 = rtb_Divide2;
    break;

   case 1:
    rtb_MultiportSwitch_idx_0 = flightController_P->Constant4_Value;
    rtb_MultiportSwitch_idx_1 = flightController_P->Constant4_Value;
    rtb_MultiportSwitch_idx_2 = rtb_Divide2;
    break;

   default:
    rtb_MultiportSwitch_idx_0 = flightController_P->Constant5_Value[0];
    rtb_MultiportSwitch_idx_1 = flightController_P->Constant5_Value[1];
    rtb_MultiportSwitch_idx_2 = flightController_P->Constant5_Value[2];
    break;
  }

  /* End of MultiPortSwitch: '<S3>/Multiport Switch' */

  /* RateTransition: '<S1>/Rate Transition1' incorporates:
   *  Delay: '<S1>/Delay1'
   */
  if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
    flightController_B->RateTransition1 = flightController_DW->Delay1_DSTATE;
  }

  /* End of RateTransition: '<S1>/Rate Transition1' */

  /* Delay: '<S23>/Delay1' incorporates:
   *  Inport: '<Root>/com'
   */
  if (flightController_DW->icLoad != 0) {
    flightController_DW->Delay1_DSTATE_m[0] = flightController_U->com[0];
    flightController_DW->Delay1_DSTATE_m[1] = flightController_U->com[1];
    flightController_DW->Delay1_DSTATE_m[2] = flightController_U->com[2];
  }

  /* Delay: '<S23>/Delay2' incorporates:
   *  Inport: '<Root>/com'
   */
  if (flightController_DW->icLoad_d != 0) {
    flightController_DW->Delay2_DSTATE[0] = flightController_U->com[0];
    flightController_DW->Delay2_DSTATE[1] = flightController_U->com[1];
    flightController_DW->Delay2_DSTATE[2] = flightController_U->com[2];
  }

  /* Switch: '<S23>/Switch2' incorporates:
   *  Abs: '<S23>/Abs'
   *  Constant: '<S30>/Constant'
   *  Delay: '<S23>/Delay1'
   *  Delay: '<S23>/Delay2'
   *  Inport: '<Root>/com'
   *  RelationalOperator: '<S30>/Compare'
   *  Sum: '<S23>/Sum2'
   */
  if (fabs(flightController_U->com[0] - flightController_DW->Delay2_DSTATE[0]) >=
      flightController_P->CompareToConstant_const_m) {
    rtb_Switch2_idx_0 = flightController_DW->Delay1_DSTATE_m[0];
  } else {
    rtb_Switch2_idx_0 = flightController_U->com[0];
  }

  if (fabs(flightController_U->com[1] - flightController_DW->Delay2_DSTATE[1]) >=
      flightController_P->CompareToConstant_const_m) {
    rtb_Switch2_idx_1 = flightController_DW->Delay1_DSTATE_m[1];
  } else {
    rtb_Switch2_idx_1 = flightController_U->com[1];
  }

  if (fabs(flightController_U->com[2] - flightController_DW->Delay2_DSTATE[2]) >=
      flightController_P->CompareToConstant_const_m) {
    rtb_Switch2_idx_2 = flightController_DW->Delay1_DSTATE_m[2];
  } else {
    rtb_Switch2_idx_2 = flightController_U->com[2];
  }

  /* End of Switch: '<S23>/Switch2' */

  /* Switch: '<S21>/Switch' incorporates:
   *  Constant: '<S21>/Constant'
   *  Constant: '<S24>/Constant'
   *  Delay: '<S1>/Delay1'
   *  Polyval: '<S21>/Polynomial'
   *  Polyval: '<S21>/Polynomial1'
   *  Polyval: '<S21>/Polynomial2'
   *  RelationalOperator: '<S24>/Compare'
   */
  if (flightController_DW->Delay1_DSTATE <=
      flightController_P->CompareToConstant1_const_b) {
    rtb_Switch_l[0] = flightController_P->Constant_Value_g[0];
    rtb_Switch_l[1] = flightController_P->Constant_Value_g[1];
    rtb_Switch_l[2] = flightController_P->Constant_Value_g[2];
  } else {
    rtb_Switch_l[0] = (flightController_P->zPoly[0] *
                       flightController_DW->Delay1_DSTATE +
                       flightController_P->zPoly[1]) *
      flightController_DW->Delay1_DSTATE + flightController_P->zPoly[2];
    rtb_Switch_l[1] = (flightController_P->yPoly[0] *
                       flightController_DW->Delay1_DSTATE +
                       flightController_P->yPoly[1]) *
      flightController_DW->Delay1_DSTATE + flightController_P->yPoly[2];
    rtb_Switch_l[2] = (flightController_P->xPoly[0] *
                       flightController_DW->Delay1_DSTATE +
                       flightController_P->xPoly[1]) *
      flightController_DW->Delay1_DSTATE + flightController_P->xPoly[2];
  }

  /* End of Switch: '<S21>/Switch' */

  /* Sum: '<S21>/Sum1' */
  rtb_Divide2 = rtb_Switch2_idx_1 - rtb_Switch_l[1];

  /* Sum: '<S21>/Sum3' */
  rtb_Polynomial2 = rtb_Switch2_idx_2 - rtb_Switch_l[0];

  /* Delay: '<S5>/Delay' */
  rtb_Delay_idx_0 = flightController_DW->Delay_DSTATE[0];
  rtb_Delay_idx_1 = flightController_DW->Delay_DSTATE[1];

  /* MATLAB Function: '<S21>/CompassTiltCompensation' incorporates:
   *  Sum: '<S21>/Sum2'
   */
  /* MATLAB Function 'orDataProcessing/DataProcessing/CompassTiltCompensation': '<S25>:1' */
  /* '<S25>:1:3' phi = Phi(1); */
  /* '<S25>:1:4' theta = Phi(2); */
  /* '<S25>:1:8' x_psi = c_x*cos(theta) + c_y*sin(theta)*sin(phi) + c_z*sin(theta)*cos(phi); */
  /* '<S25>:1:9' y_psi = c_y*cos(phi) - c_z*sin(phi); */
  /* '<S25>:1:10' psi = atan2(x_psi, y_psi); */
  rtb_Polynomial2 = rt_atan2d_snf(((rtb_Switch2_idx_0 - rtb_Switch_l[2]) * cos
    (rtb_Delay_idx_1) + rtb_Divide2 * sin(rtb_Delay_idx_1) * sin(rtb_Delay_idx_0))
    + rtb_Polynomial2 * sin(rtb_Delay_idx_1) * cos(rtb_Delay_idx_0), rtb_Divide2
    * cos(rtb_Delay_idx_0) - rtb_Polynomial2 * sin(rtb_Delay_idx_0));

  /* '<S25>:1:12' if isempty(prevPsi) */
  if (!flightController_DW->prevPsi_not_empty) {
    /* '<S25>:1:13' prevPsi = psi; */
    flightController_DW->prevPsi = rtb_Polynomial2;
    flightController_DW->prevPsi_not_empty = true;

    /* '<S25>:1:14' rounds = 0; */
  }

  /* '<S25>:1:17' if prevPsi - psi > pi */
  if (flightController_DW->prevPsi - rtb_Polynomial2 > 3.1415926535897931) {
    /* '<S25>:1:18' rounds = rounds + 1; */
    flightController_DW->rounds++;
  } else {
    if (rtb_Polynomial2 - flightController_DW->prevPsi > 3.1415926535897931) {
      /* '<S25>:1:19' elseif psi - prevPsi > pi */
      /* '<S25>:1:20' rounds = rounds - 1; */
      flightController_DW->rounds--;
    }
  }

  /* '<S25>:1:23' prevPsi = psi; */
  flightController_DW->prevPsi = rtb_Polynomial2;

  /* Logic: '<S5>/Logical Operator' incorporates:
   *  Inport: '<Root>/D_orDataProcessing'
   */
  /* '<S25>:1:25' y = psi + rounds*2*pi; */
  rtb_Compare_cy = !(flightController_U->D_orDataProcessing != 0.0);

  /* Switch: '<S5>/Switch' incorporates:
   *  Gain: '<S21>/Gain'
   *  Inport: '<Root>/acc'
   *  Inport: '<Root>/z_Phi'
   *  MATLAB Function: '<S21>/CompassTiltCompensation'
   *  Trigonometry: '<S21>/Trigonometric Function'
   *  Trigonometry: '<S21>/Trigonometric Function1'
   */
  if (rtb_Compare_cy) {
    rtb_Switch_idx_0 = rt_atan2d_snf(flightController_U->acc[1],
      flightController_U->acc[2]);
    rtb_Switch_idx_1 = flightController_P->Gain_Gain_a * rt_atan2d_snf
      (flightController_U->acc[0], flightController_U->acc[2]);
    rtb_Switch_idx_2 = flightController_DW->rounds * 2.0 * 3.1415926535897931 +
      rtb_Polynomial2;
  } else {
    rtb_Switch_idx_0 = flightController_U->z_Phi[0];
    rtb_Switch_idx_1 = flightController_U->z_Phi[1];
    rtb_Switch_idx_2 = flightController_U->z_Phi[2];
  }

  /* End of Switch: '<S5>/Switch' */

  /* MATLAB Function: '<S21>/GyroTiltCompensation' incorporates:
   *  Inport: '<Root>/gyr'
   *  Switch: '<S5>/Switch1'
   */
  /* MATLAB Function 'orDataProcessing/DataProcessing/GyroTiltCompensation': '<S26>:1' */
  /* '<S26>:1:3' phi = Phi(1); */
  /* '<S26>:1:4' theta = Phi(2); */
  /* '<S26>:1:5' R_x = [1 0 0; 0 cos(phi) -sin(phi); 0 sin(phi) cos(phi)]; */
  /* '<S26>:1:6' R_y = [cos(theta) 0 sin(theta); 0 1 0; -sin(theta) 0 cos(theta)]; */
  /* DPhi = R_x'*R_y'*[0; 0; DPhi_gyr(3)] + R_x'*[0; DPhi_gyr(2); 0] + [DPhi_gyr(1); 0; 0]; */
  /* '<S26>:1:9' DPhi = [1 0 sin(theta); 0 cos(phi) -sin(phi)*cos(theta); 0 sin(phi) cos(phi)*cos(theta)]*DPhi_gyr; */
  c_0[0] = 1.0;
  c_0[3] = 0.0;
  c_0[6] = sin(rtb_Delay_idx_1);
  c_0[1] = 0.0;
  c_0[4] = cos(rtb_Delay_idx_0);
  c_0[7] = -sin(rtb_Delay_idx_0) * cos(rtb_Delay_idx_1);
  c_0[2] = 0.0;
  c_0[5] = sin(rtb_Delay_idx_0);
  c_0[8] = cos(rtb_Delay_idx_0) * cos(rtb_Delay_idx_1);
  for (i = 0; i < 3; i++) {
    tmp[i] = c_0[i + 6] * flightController_U->gyr[2] + (c_0[i + 3] *
      flightController_U->gyr[1] + c_0[i] * flightController_U->gyr[0]);
  }

  /* End of MATLAB Function: '<S21>/GyroTiltCompensation' */

  /* Switch: '<S5>/Switch1' incorporates:
   *  Inport: '<Root>/z_DPhi'
   */
  if (rtb_Compare_cy) {
    rtb_Switch1_idx_0 = tmp[0];
    rtb_Switch1_idx_1 = tmp[1];
    rtb_Switch1_idx_2 = tmp[2];
  } else {
    rtb_Switch1_idx_0 = flightController_U->z_DPhi[0];
    rtb_Switch1_idx_1 = flightController_U->z_DPhi[1];
    rtb_Switch1_idx_2 = flightController_U->z_DPhi[2];
  }

  /* MATLAB Function: '<S22>/filterPhi' incorporates:
   *  Constant: '<S22>/Constant1'
   *  Constant: '<S22>/Constant2'
   *  Constant: '<S22>/Constant3'
   */
  flightController_filterPhi(rtb_Switch_idx_0, rtb_Switch1_idx_0,
    flightController_P->R_phi, flightController_P->Q_phi, flightController_P->dt,
    &flightController_B->sf_filterPhi, &flightController_DW->sf_filterPhi);

  /* MATLAB Function: '<S22>/filterTheta' incorporates:
   *  Constant: '<S22>/Constant4'
   *  Constant: '<S22>/Constant5'
   *  Constant: '<S22>/Constant6'
   */
  flightController_filterPhi(rtb_Switch_idx_1, rtb_Switch1_idx_1,
    flightController_P->R_theta, flightController_P->Q_theta,
    flightController_P->dt, &flightController_B->sf_filterTheta,
    &flightController_DW->sf_filterTheta);

  /* MATLAB Function: '<S22>/filterPsi' incorporates:
   *  Constant: '<S22>/Constant7'
   *  Constant: '<S22>/Constant8'
   *  Constant: '<S22>/Constant9'
   *  SignalConversion: '<S28>/TmpSignal ConversionAt SFunction Inport1'
   */
  /* MATLAB Function 'orDataProcessing/Filter/filterPsi': '<S28>:1' */
  /* '<S28>:1:3' A = [1 dt; 0 1]; */
  A[0] = 1.0;
  A[2] = flightController_P->dt;
  A[1] = 0.0;
  A[3] = 1.0;

  /* '<S28>:1:4' H = eye(2,2); */
  /* '<S28>:1:6' if isempty(P_prior) */
  if (!flightController_DW->P_prior_not_empty) {
    /* '<S28>:1:7' x_prior_estimate = u; */
    flightController_DW->x_prior_estimate_n[0] = rtb_Switch_idx_2;
    flightController_DW->x_prior_estimate_n[1] = rtb_Switch1_idx_2;

    /* '<S28>:1:8' P_prior = [0 0; 0 0]; */
    flightController_DW->P_prior_not_empty = true;
  }

  /* '<S28>:1:11' K = P_prior*H'*(H*P_prior*H' + R)^-1; */
  for (i = 0; i < 2; i++) {
    b_1[i] = 0.0;
    b_1[i] += (real_T)b[i] * flightController_DW->P_prior_f[0];
    b_1[i] += (real_T)b[i + 2] * flightController_DW->P_prior_f[1];
    b_1[i + 2] = 0.0;
    b_1[i + 2] += (real_T)b[i] * flightController_DW->P_prior_f[2];
    b_1[i + 2] += (real_T)b[i + 2] * flightController_DW->P_prior_f[3];
  }

  for (i = 0; i < 2; i++) {
    a[i] = (b_1[i + 2] * 0.0 + b_1[i]) + flightController_P->Q_psi[i];
    a[i + 2] = (b_1[i] * 0.0 + b_1[i + 2]) + flightController_P->Q_psi[i + 2];
  }

  if (fabs(a[1]) > fabs(a[0])) {
    rtb_Divide2 = a[0] / a[1];
    rtb_Delay_idx_0 = 1.0 / (rtb_Divide2 * a[3] - a[2]);
    rtb_Delay_idx_1 = a[3] / a[1] * rtb_Delay_idx_0;
    c_idx_1 = -rtb_Delay_idx_0;
    c_idx_2 = -a[2] / a[1] * rtb_Delay_idx_0;
    rtb_Delay_idx_0 *= rtb_Divide2;
  } else {
    rtb_Divide2 = a[1] / a[0];
    rtb_Delay_idx_0 = 1.0 / (a[3] - rtb_Divide2 * a[2]);
    rtb_Delay_idx_1 = a[3] / a[0] * rtb_Delay_idx_0;
    c_idx_1 = -rtb_Divide2 * rtb_Delay_idx_0;
    c_idx_2 = -a[2] / a[0] * rtb_Delay_idx_0;
  }

  for (i = 0; i < 2; i++) {
    b_1[i] = 0.0;
    b_1[i] += flightController_DW->P_prior_f[i];
    b_1[i] += flightController_DW->P_prior_f[i + 2] * 0.0;
    b_1[i + 2] = 0.0;
    b_1[i + 2] += flightController_DW->P_prior_f[i] * 0.0;
    b_1[i + 2] += flightController_DW->P_prior_f[i + 2];
  }

  for (i = 0; i < 2; i++) {
    K[i] = 0.0;
    K[i] += b_1[i] * rtb_Delay_idx_1;
    K[i] += b_1[i + 2] * c_idx_1;
    K[i + 2] = 0.0;
    K[i + 2] += b_1[i] * c_idx_2;
    K[i + 2] += b_1[i + 2] * rtb_Delay_idx_0;
  }

  /* 1 */
  /* '<S28>:1:12' x_estimate = x_prior_estimate + K*(u - H*x_prior_estimate); */
  c_idx_1 = rtb_Switch_idx_2 - (0.0 * flightController_DW->x_prior_estimate_n[1]
    + flightController_DW->x_prior_estimate_n[0]);
  rtb_Delay_idx_0 = rtb_Switch1_idx_2 - (0.0 *
    flightController_DW->x_prior_estimate_n[0] +
    flightController_DW->x_prior_estimate_n[1]);
  rtb_y_n_idx_0 = (K[0] * c_idx_1 + K[2] * rtb_Delay_idx_0) +
    flightController_DW->x_prior_estimate_n[0];
  rtb_y_n_idx_1 = (K[1] * c_idx_1 + K[3] * rtb_Delay_idx_0) +
    flightController_DW->x_prior_estimate_n[1];

  /* 2 */
  /* '<S28>:1:13' P = (eye(2,2) - K*H)*P_prior; */
  a[1] = 0.0;
  a[2] = 0.0;
  a[0] = 1.0;
  a[3] = 1.0;

  /* 3 */
  /* '<S28>:1:14' x_prior_estimate = A*x_estimate; */
  flightController_DW->x_prior_estimate_n[0] = 0.0;
  flightController_DW->x_prior_estimate_n[0] += rtb_y_n_idx_0;
  flightController_DW->x_prior_estimate_n[0] += flightController_P->dt *
    rtb_y_n_idx_1;
  flightController_DW->x_prior_estimate_n[1] = 0.0;
  flightController_DW->x_prior_estimate_n[1] += 0.0 * rtb_y_n_idx_0;
  flightController_DW->x_prior_estimate_n[1] += rtb_y_n_idx_1;

  /* 4 */
  /* '<S28>:1:15' P_prior = A*P*A' + Q; */
  for (i = 0; i < 2; i++) {
    b_1[i] = a[i] - (K[i + 2] * 0.0 + K[i]);
    b_1[i + 2] = a[i + 2] - (K[i] * 0.0 + K[i + 2]);
  }

  for (i = 0; i < 2; i++) {
    a_0[i] = 0.0;
    a_0[i] += b_1[i] * flightController_DW->P_prior_f[0];
    a_0[i] += b_1[i + 2] * flightController_DW->P_prior_f[1];
    a_0[i + 2] = 0.0;
    a_0[i + 2] += b_1[i] * flightController_DW->P_prior_f[2];
    a_0[i + 2] += b_1[i + 2] * flightController_DW->P_prior_f[3];
  }

  for (i = 0; i < 2; i++) {
    b_1[i] = 0.0;
    b_1[i] += A[i] * a_0[0];
    b_1[i] += A[i + 2] * a_0[1];
    b_1[i + 2] = 0.0;
    b_1[i + 2] += A[i] * a_0[2];
    b_1[i + 2] += A[i + 2] * a_0[3];
  }

  for (i = 0; i < 2; i++) {
    flightController_DW->P_prior_f[i] = (b_1[i + 2] * flightController_P->dt +
      b_1[i]) + flightController_P->R_psi[i];
    flightController_DW->P_prior_f[i + 2] = (b_1[i] * 0.0 + b_1[i + 2]) +
      flightController_P->R_psi[i + 2];
  }

  /* End of MATLAB Function: '<S22>/filterPsi' */

  /* SignalConversion: '<S5>/TmpSignal ConversionAtDelayInport1' */
  /* 4 */
  /* '<S28>:1:17' y = x_estimate; */
  rtb_TmpSignalConversionAtDelayI[0] = flightController_B->sf_filterPhi.y[0];
  rtb_TmpSignalConversionAtDelayI[1] = flightController_B->sf_filterTheta.y[0];
  rtb_TmpSignalConversionAtDelayI[2] = rtb_y_n_idx_0;

  /* RateTransition: '<S1>/Rate Transition3' */
  if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
    flightController_B->RateTransition3[0] = rtb_TmpSignalConversionAtDelayI[0];
    flightController_B->RateTransition3[1] = rtb_TmpSignalConversionAtDelayI[1];
    flightController_B->RateTransition3[2] = rtb_TmpSignalConversionAtDelayI[2];
  }

  /* End of RateTransition: '<S1>/Rate Transition3' */

  /* Outputs for Enabled SubSystem: '<S1>/poDataProcessing' incorporates:
   *  EnablePort: '<S7>/Enable'
   */
  /* Inport: '<Root>/armed' */
  if (flightController_U->armed > 0.0) {
    if (!flightController_DW->poDataProcessing_MODE) {
      flightController_DW->poDataProcessing_MODE = true;
    }

    if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
      /* Inport: '<S7>/z_r' incorporates:
       *  Inport: '<Root>/z_r'
       */
      flightController_B->z_r[0] = flightController_U->z_r[0];
      flightController_B->z_r[1] = flightController_U->z_r[1];
      flightController_B->z_r[2] = flightController_U->z_r[2];

      /* Math: '<S54>/Math Function' incorporates:
       *  Constant: '<S54>/Constant'
       */
      rtb_Polynomial2 = flightController_P->Constant_Value_l *
        flightController_P->Constant_Value_l;

      /* DiscreteIntegrator: '<S54>/Discrete-Time Integrator1' */
      if (flightController_DW->DiscreteTimeIntegrator1_IC_LOAD != 0) {
        flightController_DW->DiscreteTimeIntegrator1_DSTATE =
          flightController_B->z_r[2];
      }

      flightController_B->y =
        flightController_DW->DiscreteTimeIntegrator1_DSTATE;

      /* End of DiscreteIntegrator: '<S54>/Discrete-Time Integrator1' */

      /* MATLAB Function: '<S7>/RotationMatrix' incorporates:
       *  Product: '<S7>/Matrix Multiply'
       */
      /* MATLAB Function 'poDataProcessing/RotationMatrix': '<S50>:1' */
      /* '<S50>:1:4' phi = Phi(1); */
      /* '<S50>:1:5' theta = Phi(2); */
      /* '<S50>:1:6' psi = Phi(3); */
      /* '<S50>:1:8' R_x = [1 0 0; 0 cos(phi) -sin(phi); 0 sin(phi) cos(phi)]; */
      /* '<S50>:1:9' R_y = [cos(theta) 0 sin(theta); 0 1 0; -sin(theta) 0 cos(theta)]; */
      /* '<S50>:1:10' R_z = [cos(psi) -sin(psi) 0; sin(psi) cos(psi) 0; 0 0 1]; */
      /* '<S50>:1:12' Ri_b = R_x'*R_y'*R_z'; */
      /* R_z'*R_x'*R_y'; */
      c[0] = 1.0;
      c[1] = 0.0;
      c[2] = 0.0;
      c[3] = 0.0;
      c[4] = cos(flightController_B->RateTransition3[0]);
      c[5] = -sin(flightController_B->RateTransition3[0]);
      c[6] = 0.0;
      c[7] = sin(flightController_B->RateTransition3[0]);
      c[8] = cos(flightController_B->RateTransition3[0]);
      tmp_0[0] = cos(flightController_B->RateTransition3[1]);
      tmp_0[1] = 0.0;
      tmp_0[2] = sin(flightController_B->RateTransition3[1]);
      tmp_0[3] = 0.0;
      tmp_0[4] = 1.0;
      tmp_0[5] = 0.0;
      tmp_0[6] = -sin(flightController_B->RateTransition3[1]);
      tmp_0[7] = 0.0;
      tmp_0[8] = cos(flightController_B->RateTransition3[1]);
      for (i = 0; i < 3; i++) {
        for (i_0 = 0; i_0 < 3; i_0++) {
          c_0[i + 3 * i_0] = 0.0;
          c_0[i + 3 * i_0] += tmp_0[3 * i_0] * c[i];
          c_0[i + 3 * i_0] += tmp_0[3 * i_0 + 1] * c[i + 3];
          c_0[i + 3 * i_0] += tmp_0[3 * i_0 + 2] * c[i + 6];
        }
      }

      tmp_1[0] = cos(flightController_B->RateTransition3[2]);
      tmp_1[1] = -sin(flightController_B->RateTransition3[2]);
      tmp_1[2] = 0.0;
      tmp_1[3] = sin(flightController_B->RateTransition3[2]);
      tmp_1[4] = cos(flightController_B->RateTransition3[2]);
      tmp_1[5] = 0.0;
      tmp_1[6] = 0.0;
      tmp_1[7] = 0.0;
      tmp_1[8] = 1.0;
      for (i = 0; i < 3; i++) {
        for (i_0 = 0; i_0 < 3; i_0++) {
          c[i + 3 * i_0] = 0.0;
          c[i + 3 * i_0] += tmp_1[3 * i_0] * c_0[i];
          c[i + 3 * i_0] += tmp_1[3 * i_0 + 1] * c_0[i + 3];
          c[i + 3 * i_0] += tmp_1[3 * i_0 + 2] * c_0[i + 6];
        }
      }

      /* End of MATLAB Function: '<S7>/RotationMatrix' */

      /* Gain: '<S7>/Gain1' incorporates:
       *  Constant: '<S7>/Constant11'
       *  Saturate: '<S7>/Saturation'
       *  Sum: '<S7>/Sum1'
       */
      c_idx_1 = flightController_B->RateTransition1 -
        flightController_P->Constant11_Value;
      if (c_idx_1 > flightController_P->Saturation_UpperSat) {
        c_idx_1 = flightController_P->Saturation_UpperSat;
      } else {
        if (c_idx_1 < flightController_P->Saturation_LowerSat) {
          c_idx_1 = flightController_P->Saturation_LowerSat;
        }
      }

      /* SignalConversion: '<S7>/TmpSignal ConversionAtMatrix MultiplyInport2' incorporates:
       *  Gain: '<S7>/Gain1'
       *  Saturate: '<S7>/Saturation'
       */
      rtb_Delay_idx_0 = flightController_P->F_c * 4.0 / flightController_P->m *
        c_idx_1;

      /* Sum: '<S7>/Sum' incorporates:
       *  Constant: '<S7>/Constant10'
       *  Constant: '<S7>/Constant8'
       *  Product: '<S7>/Matrix Multiply'
       */
      for (i = 0; i < 3; i++) {
        rtb_Switch_l[i] = ((c[i + 3] * flightController_P->Constant10_Value +
                            c[i] * flightController_P->Constant10_Value) + c[i +
                           6] * rtb_Delay_idx_0) +
          flightController_P->Constant8_Value_b[i];
      }

      /* End of Sum: '<S7>/Sum' */

      /* MATLAB Function: '<S7>/filterX' incorporates:
       *  Constant: '<S7>/Constant'
       *  Constant: '<S7>/Constant1'
       *  Constant: '<S7>/Constant9'
       *  Inport: '<Root>/z_v'
       */
      flightController_filterX(flightController_U->z_v[0], rtb_Switch_l[0],
        flightController_P->Q_x, flightController_P->R_x,
        flightController_P->dt_p, &flightController_B->sf_filterX,
        &flightController_DW->sf_filterX);

      /* MATLAB Function: '<S7>/filterY' incorporates:
       *  Constant: '<S7>/Constant2'
       *  Constant: '<S7>/Constant3'
       *  Constant: '<S7>/Constant6'
       *  Inport: '<Root>/z_v'
       *  SignalConversion: '<S52>/TmpSignal ConversionAt SFunction Inport1'
       */
      /* MATLAB Function 'poDataProcessing/filterY': '<S52>:1' */
      /* '<S52>:1:3' A = [1 dt; 0 1]; */
      A[0] = 1.0;
      A[2] = flightController_P->dt_p;
      A[1] = 0.0;
      A[3] = 1.0;

      /* '<S52>:1:4' H = [1 0; 0 1]; */
      /* '<S52>:1:6' if isempty(P_prior) */
      /* '<S52>:1:11' K = P_prior*H'*(H*P_prior*H' + R)^-1; */
      for (i = 0; i < 2; i++) {
        b_1[i] = 0.0;
        b_1[i] += (real_T)b_0[i] * flightController_DW->P_prior[0];
        b_1[i] += (real_T)b_0[i + 2] * flightController_DW->P_prior[1];
        b_1[i + 2] = 0.0;
        b_1[i + 2] += (real_T)b_0[i] * flightController_DW->P_prior[2];
        b_1[i + 2] += (real_T)b_0[i + 2] * flightController_DW->P_prior[3];
      }

      for (i = 0; i < 2; i++) {
        a[i] = (b_1[i + 2] * 0.0 + b_1[i]) + flightController_P->R_y[i];
        a[i + 2] = (b_1[i] * 0.0 + b_1[i + 2]) + flightController_P->R_y[i + 2];
      }

      if (fabs(a[1]) > fabs(a[0])) {
        rtb_Divide2 = a[0] / a[1];
        rtb_Delay_idx_0 = 1.0 / (rtb_Divide2 * a[3] - a[2]);
        rtb_Delay_idx_1 = a[3] / a[1] * rtb_Delay_idx_0;
        c_idx_1 = -rtb_Delay_idx_0;
        c_idx_2 = -a[2] / a[1] * rtb_Delay_idx_0;
        rtb_Delay_idx_0 *= rtb_Divide2;
      } else {
        rtb_Divide2 = a[1] / a[0];
        rtb_Delay_idx_0 = 1.0 / (a[3] - rtb_Divide2 * a[2]);
        rtb_Delay_idx_1 = a[3] / a[0] * rtb_Delay_idx_0;
        c_idx_1 = -rtb_Divide2 * rtb_Delay_idx_0;
        c_idx_2 = -a[2] / a[0] * rtb_Delay_idx_0;
      }

      for (i = 0; i < 2; i++) {
        b_1[i] = 0.0;
        b_1[i] += flightController_DW->P_prior[i];
        b_1[i] += flightController_DW->P_prior[i + 2] * 0.0;
        b_1[i + 2] = 0.0;
        b_1[i + 2] += flightController_DW->P_prior[i] * 0.0;
        b_1[i + 2] += flightController_DW->P_prior[i + 2];
      }

      for (i = 0; i < 2; i++) {
        K[i] = 0.0;
        K[i] += b_1[i] * rtb_Delay_idx_1;
        K[i] += b_1[i + 2] * c_idx_1;
        K[i + 2] = 0.0;
        K[i + 2] += b_1[i] * c_idx_2;
        K[i + 2] += b_1[i + 2] * rtb_Delay_idx_0;
      }

      /* 1 */
      /* '<S52>:1:12' x_estimate = x_prior_estimate + K*(z - H*x_prior_estimate); */
      rtb_Delay_idx_0 = flightController_U->z_v[1] - (0.0 *
        flightController_DW->x_prior_estimate[1] +
        flightController_DW->x_prior_estimate[0]);
      rtb_Delay_idx_1 = rtb_Switch_l[1] - (0.0 *
        flightController_DW->x_prior_estimate[0] +
        flightController_DW->x_prior_estimate[1]);
      c_idx_1 = (K[0] * rtb_Delay_idx_0 + K[2] * rtb_Delay_idx_1) +
        flightController_DW->x_prior_estimate[0];
      rtb_Delay_idx_0 = (K[1] * rtb_Delay_idx_0 + K[3] * rtb_Delay_idx_1) +
        flightController_DW->x_prior_estimate[1];

      /* 2 */
      /* '<S52>:1:13' P = (eye(2,2) - K*H)*P_prior; */
      a[1] = 0.0;
      a[2] = 0.0;
      a[0] = 1.0;
      a[3] = 1.0;

      /* 3 */
      /* '<S52>:1:14' x_prior_estimate = A*x_estimate; */
      flightController_DW->x_prior_estimate[0] = 0.0;
      flightController_DW->x_prior_estimate[0] += c_idx_1;
      flightController_DW->x_prior_estimate[0] += flightController_P->dt_p *
        rtb_Delay_idx_0;
      flightController_DW->x_prior_estimate[1] = 0.0;
      flightController_DW->x_prior_estimate[1] += 0.0 * c_idx_1;
      flightController_DW->x_prior_estimate[1] += rtb_Delay_idx_0;

      /* 4 */
      /* '<S52>:1:15' P_prior = A*P*A' + Q; */
      for (i = 0; i < 2; i++) {
        b_1[i] = a[i] - (K[i + 2] * 0.0 + K[i]);
        b_1[i + 2] = a[i + 2] - (K[i] * 0.0 + K[i + 2]);
      }

      for (i = 0; i < 2; i++) {
        a[i] = 0.0;
        a[i] += b_1[i] * flightController_DW->P_prior[0];
        a[i] += b_1[i + 2] * flightController_DW->P_prior[1];
        a[i + 2] = 0.0;
        a[i + 2] += b_1[i] * flightController_DW->P_prior[2];
        a[i + 2] += b_1[i + 2] * flightController_DW->P_prior[3];
      }

      for (i = 0; i < 2; i++) {
        b_1[i] = 0.0;
        b_1[i] += A[i] * a[0];
        b_1[i] += A[i + 2] * a[1];
        b_1[i + 2] = 0.0;
        b_1[i + 2] += A[i] * a[2];
        b_1[i + 2] += A[i + 2] * a[3];
      }

      for (i = 0; i < 2; i++) {
        flightController_DW->P_prior[i] = (b_1[i + 2] * flightController_P->dt_p
          + b_1[i]) + flightController_P->Q_y[i];
        flightController_DW->P_prior[i + 2] = (b_1[i] * 0.0 + b_1[i + 2]) +
          flightController_P->Q_y[i + 2];
      }

      /* End of MATLAB Function: '<S7>/filterY' */

      /* MATLAB Function: '<S7>/filterZ' incorporates:
       *  Constant: '<S7>/Constant4'
       *  Constant: '<S7>/Constant5'
       *  Constant: '<S7>/Constant7'
       *  Inport: '<Root>/z_v'
       */
      /* 4 */
      /* '<S52>:1:17' y = x_estimate; */
      flightController_filterX(flightController_U->z_v[2], rtb_Switch_l[2],
        flightController_P->Q_z, flightController_P->R_z,
        flightController_P->dt_p, &flightController_B->sf_filterZ,
        &flightController_DW->sf_filterZ);

      /* SignalConversion: '<S7>/OutportBufferForx_hat_a' */
      flightController_B->OutportBufferForx_hat_a[0] =
        flightController_B->sf_filterX.y[1];
      flightController_B->OutportBufferForx_hat_a[1] = rtb_Delay_idx_0;
      flightController_B->OutportBufferForx_hat_a[2] =
        flightController_B->sf_filterZ.y[1];

      /* SignalConversion: '<S7>/OutportBufferForx_hat_v' */
      flightController_B->OutportBufferForx_hat_v[0] =
        flightController_B->sf_filterX.y[0];
      flightController_B->OutportBufferForx_hat_v[1] = c_idx_1;
      flightController_B->OutportBufferForx_hat_v[2] =
        flightController_B->sf_filterZ.y[0];

      /* Update for DiscreteIntegrator: '<S54>/Discrete-Time Integrator1' incorporates:
       *  DiscreteIntegrator: '<S54>/Discrete-Time Integrator'
       */
      flightController_DW->DiscreteTimeIntegrator1_IC_LOAD = 0U;
      flightController_DW->DiscreteTimeIntegrator1_DSTATE +=
        flightController_P->DiscreteTimeIntegrator1_gainv_i *
        flightController_DW->DiscreteTimeIntegrator_DSTATE;

      /* Update for DiscreteIntegrator: '<S54>/Discrete-Time Integrator' incorporates:
       *  Constant: '<S54>/Constant'
       *  Constant: '<S54>/Constant1'
       *  DiscreteIntegrator: '<S54>/Discrete-Time Integrator'
       *  Product: '<S54>/Product1'
       *  Product: '<S54>/Product2'
       *  Product: '<S54>/Product3'
       *  Sum: '<S54>/Add'
       */
      flightController_DW->DiscreteTimeIntegrator_DSTATE +=
        ((flightController_B->z_r[2] * rtb_Polynomial2 - flightController_B->y *
          rtb_Polynomial2) - flightController_DW->DiscreteTimeIntegrator_DSTATE *
         flightController_P->Constant1_Value_i *
         flightController_P->Constant_Value_l) *
        flightController_P->DiscreteTimeIntegrator_gainva_d;
    }
  } else {
    if (flightController_DW->poDataProcessing_MODE) {
      flightController_DW->poDataProcessing_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S1>/poDataProcessing' */

  /* Switch: '<S3>/Switch2' incorporates:
   *  Constant: '<S3>/Constant6'
   *  Constant: '<S3>/Constant7'
   *  Constant: '<S3>/Constant9'
   *  Inport: '<Root>/RCctrl'
   *  Inport: '<Root>/RCok'
   *  Product: '<S3>/Divide3'
   *  Sum: '<S3>/Sum1'
   */
  if (flightController_U->RCok > flightController_P->Switch2_Threshold) {
    rtb_Delay_idx_0 = (flightController_U->RCctrl[3] -
                       flightController_P->Constant7_Value) /
      flightController_P->Constant6_Value;
  } else {
    rtb_Delay_idx_0 = flightController_P->Constant9_Value;
  }

  /* End of Switch: '<S3>/Switch2' */

  /* Switch: '<S3>/Switch' incorporates:
   *  Constant: '<S3>/Constant5'
   *  Constant: '<S8>/Constant'
   *  Inport: '<Root>/mode'
   *  RelationalOperator: '<S8>/Compare'
   */
  if (flightController_U->mode == flightController_P->CompareToConstant_const_j)
  {
    flightController_B->Switch[0] = rtb_Divide;
    flightController_B->Switch[1] = rtb_Divide1;
    flightController_B->Switch[2] = rtb_Delay_idx_0;
  } else {
    flightController_B->Switch[0] = flightController_P->Constant5_Value[0];
    flightController_B->Switch[1] = flightController_P->Constant5_Value[1];
    flightController_B->Switch[2] = flightController_P->Constant5_Value[2];
  }

  /* End of Switch: '<S3>/Switch' */

  /* Outputs for Enabled SubSystem: '<S1>/poController' incorporates:
   *  EnablePort: '<S6>/Enable'
   */
  /* Logic: '<S1>/Logical Operator1' incorporates:
   *  Constant: '<S2>/Constant'
   *  Inport: '<Root>/armed'
   *  Inport: '<Root>/mode'
   *  RelationalOperator: '<S2>/Compare'
   */
  if ((flightController_U->armed != 0.0) && (flightController_U->mode >=
       flightController_P->CompareToConstant1_const_g)) {
    if (!flightController_DW->poController_MODE) {
      /* InitializeConditions for DiscreteIntegrator: '<S38>/Discrete-Time Integrator2' */
      flightController_DW->DiscreteTimeIntegrator2_DSTATE =
        flightController_P->DiscreteTimeIntegrator2_IC_f;

      /* InitializeConditions for MATLAB Function: '<S34>/mode2sequence' */
      /* '<S39>:1:4' sequence = -1; */
      flightController_DW->sequence = -1.0;

      /* '<S39>:1:5' r_SP = [0; 0; 0]; */
      flightController_DW->r_SP[0] = 0.0;
      flightController_DW->r_SP[1] = 0.0;
      flightController_DW->r_SP[2] = 0.0;

      /* '<S39>:1:6' lastGPSsp = [0; 0; 0]; */
      flightController_DW->lastGPSsp[0] = 0.0;
      flightController_DW->lastGPSsp[1] = 0.0;
      flightController_DW->lastGPSsp[2] = 0.0;

      /* InitializeConditions for DiscreteIntegrator: '<S33>/Discrete-Time Integrator2' */
      flightController_DW->DiscreteTimeIntegrator2_IC_LOAD = 1U;

      /* InitializeConditions for DiscreteIntegrator: '<S35>/Discrete-Time Integrator1' */
      flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[0] =
        flightController_P->DiscreteTimeIntegrator1_IC;
      flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[1] =
        flightController_P->DiscreteTimeIntegrator1_IC;
      flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[2] =
        flightController_P->DiscreteTimeIntegrator1_IC;

      /* InitializeConditions for Delay: '<S35>/Delay1' */
      flightController_DW->icLoad_do = 1U;

      /* InitializeConditions for DiscreteIntegrator: '<S36>/Discrete-Time Integrator' */
      flightController_DW->DiscreteTimeIntegrator_DSTATE_l[0] =
        flightController_P->DiscreteTimeIntegrator_IC_nb[0];
      flightController_DW->DiscreteTimeIntegrator_DSTATE_l[1] =
        flightController_P->DiscreteTimeIntegrator_IC_nb[1];
      flightController_DW->DiscreteTimeIntegrator_DSTATE_l[2] =
        flightController_P->DiscreteTimeIntegrator_IC_nb[2];

      /* InitializeConditions for Delay: '<S36>/Delay' */
      flightController_DW->icLoad_k = 1U;

      /* InitializeConditions for Delay: '<S40>/Delay3' */
      flightController_DW->icLoad_o = 1U;

      /* InitializeConditions for Delay: '<S40>/Delay' */
      flightController_DW->icLoad_a = 1U;
      flightController_DW->poController_MODE = true;
    }

    /* RelationalOperator: '<S31>/Compare' incorporates:
     *  Constant: '<S31>/Constant'
     */
    rtb_Compare_gs = (flightController_U->mode ==
                      flightController_P->CompareToConstant_const_k);
    if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
      /* MATLAB Function: '<S6>/RotationMatrix' */
      /* MATLAB Function 'flightController/poController/RotationMatrix': '<S32>:1' */
      /* '<S32>:1:4' psi = Phi(3); */
      /*  - pi/2; */
      /* R_z = [cos(psi) -sin(psi) 0; sin(psi) cos(psi) 0; 0 0 1]'; */
      /* '<S32>:1:7' R_z = [sin(psi) -cos(psi) 0; cos(psi) sin(psi) 0; 0 0 1]; */
      flightController_B->R_z[0] = sin(flightController_B->RateTransition3[2]);
      flightController_B->R_z[3] = -cos(flightController_B->RateTransition3[2]);
      flightController_B->R_z[6] = 0.0;
      flightController_B->R_z[1] = cos(flightController_B->RateTransition3[2]);
      flightController_B->R_z[4] = sin(flightController_B->RateTransition3[2]);
      flightController_B->R_z[7] = 0.0;
      flightController_B->R_z[2] = 0.0;
      flightController_B->R_z[5] = 0.0;
      flightController_B->R_z[8] = 1.0;

      /* RelationalOperator: '<S37>/Compare' incorporates:
       *  Constant: '<S37>/Constant'
       */
      rtb_Compare_gt = (flightController_U->mode ==
                        flightController_P->CompareToConstant1_const_l);

      /* Outputs for Enabled SubSystem: '<S34>/velocitySPmanager' incorporates:
       *  EnablePort: '<S41>/Enable'
       */
      if (rtb_Compare_gt) {
        if (!flightController_DW->velocitySPmanager_MODE) {
          /* InitializeConditions for Delay: '<S41>/Delay' */
          flightController_DW->icLoad_e = 1U;
          flightController_DW->velocitySPmanager_MODE = true;
        }
      } else {
        if (flightController_DW->velocitySPmanager_MODE) {
          /* Disable for Outport: '<S41>/r_SP' */
          flightController_B->Switch_k[0] = flightController_P->r_SP_Y0[0];
          flightController_B->Switch_k[1] = flightController_P->r_SP_Y0[1];
          flightController_B->Switch_k[2] = flightController_P->r_SP_Y0[2];

          /* Disable for Outport: '<S41>/E_v_SP' */
          flightController_B->OutportBufferForE_v_SP[0] =
            flightController_P->E_v_SP_Y0[0];
          flightController_B->OutportBufferForE_v_SP[1] =
            flightController_P->E_v_SP_Y0[1];
          flightController_B->OutportBufferForE_v_SP[2] =
            flightController_P->E_v_SP_Y0[2];

          /* Disable for Outport: '<S41>/v_SP' */
          flightController_B->SP[0] = flightController_P->v_SP_Y0[0];
          flightController_B->SP[1] = flightController_P->v_SP_Y0[1];
          flightController_B->SP[2] = flightController_P->v_SP_Y0[2];
          flightController_DW->velocitySPmanager_MODE = false;
        }
      }

      /* End of Outputs for SubSystem: '<S34>/velocitySPmanager' */
    }

    /* Outputs for Enabled SubSystem: '<S34>/velocitySPmanager' incorporates:
     *  EnablePort: '<S41>/Enable'
     */
    if (flightController_DW->velocitySPmanager_MODE) {
      if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
        flightController_B->SP[0] = flightController_B->Switch[0];
        flightController_B->SP[1] = flightController_B->Switch[1];
        flightController_B->SP[2] = flightController_B->Switch[2];

        /* Abs: '<S41>/Abs' incorporates:
         *  Inport: '<S41>/SP'
         */
        flightController_B->Abs[0] = fabs(flightController_B->SP[0]);
        flightController_B->Abs[1] = fabs(flightController_B->SP[1]);
        flightController_B->Abs[2] = fabs(flightController_B->SP[2]);
      }

      /* RelationalOperator: '<S48>/Compare' incorporates:
       *  Constant: '<S48>/Constant'
       *  Inport: '<S41>/SP'
       */
      rtb_Compare_cy = (flightController_B->Abs[2] >=
                        flightController_P->CompareToConstant2_const);
      if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
        /* Delay: '<S41>/Delay' incorporates:
         *  SignalConversion: '<S41>/TmpSignal ConversionAtDelayInport2'
         */
        if (flightController_DW->icLoad_e != 0) {
          flightController_DW->Delay_DSTATE_a[0] = flightController_B->z_r[0];
          flightController_DW->Delay_DSTATE_a[1] = flightController_B->z_r[1];
          flightController_DW->Delay_DSTATE_a[2] = flightController_B->y;
        }

        flightController_B->Delay_d[0] = flightController_DW->Delay_DSTATE_a[0];
        flightController_B->Delay_d[1] = flightController_DW->Delay_DSTATE_a[1];
        flightController_B->Delay_d[2] = flightController_DW->Delay_DSTATE_a[2];

        /* End of Delay: '<S41>/Delay' */
      }

      /* Logic: '<S41>/Logical Operator' incorporates:
       *  Constant: '<S47>/Constant'
       *  Constant: '<S49>/Constant'
       *  RelationalOperator: '<S47>/Compare'
       *  RelationalOperator: '<S49>/Compare'
       */
      rtb_LogicalOperator_l = ((flightController_B->Abs[0] >=
        flightController_P->CompareToConstant5_const) ||
        (flightController_B->Abs[1] >=
         flightController_P->CompareToConstant1_const_k));

      /* SignalConversion: '<S41>/OutportBufferForE_v_SP' */
      flightController_B->OutportBufferForE_v_SP[0] = rtb_LogicalOperator_l;
      flightController_B->OutportBufferForE_v_SP[1] = rtb_LogicalOperator_l;
      flightController_B->OutportBufferForE_v_SP[2] = rtb_Compare_cy;

      /* Switch: '<S41>/Switch' */
      if (rtb_LogicalOperator_l) {
        flightController_B->Switch_k[0] = flightController_B->z_r[0];
        flightController_B->Switch_k[1] = flightController_B->z_r[1];
      } else {
        flightController_B->Switch_k[0] = flightController_B->Delay_d[0];
        flightController_B->Switch_k[1] = flightController_B->Delay_d[1];
      }

      if (rtb_Compare_cy) {
        flightController_B->Switch_k[2] = flightController_B->y;
      } else {
        flightController_B->Switch_k[2] = flightController_B->Delay_d[2];
      }

      /* End of Switch: '<S41>/Switch' */
      if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
        /* Update for Delay: '<S41>/Delay' */
        flightController_DW->icLoad_e = 0U;
        flightController_DW->Delay_DSTATE_a[0] = flightController_B->Switch_k[0];
        flightController_DW->Delay_DSTATE_a[1] = flightController_B->Switch_k[1];
        flightController_DW->Delay_DSTATE_a[2] = flightController_B->Switch_k[2];
      }
    }

    /* End of Outputs for SubSystem: '<S34>/velocitySPmanager' */
    if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
      /* DiscreteIntegrator: '<S38>/Discrete-Time Integrator2' */
      rtb_DiscreteTimeIntegrator2 =
        flightController_DW->DiscreteTimeIntegrator2_DSTATE;

      /* MATLAB Function: '<S34>/mode2sequence' incorporates:
       *  DiscreteIntegrator: '<S38>/Discrete-Time Integrator2'
       *  Inport: '<Root>/r_SP'
       *  SignalConversion: '<S39>/TmpSignal ConversionAt SFunction Inport3'
       */
      /* MATLAB Function 'flightController/poController/posSPmanager/mode2sequence': '<S39>:1' */
      /* '<S39>:1:3' if isempty(sequence) */
      /* '<S39>:1:8' SP = [0; 0; 0]; */
      rtb_SP_idx_0 = 0.0;
      rtb_SP_idx_1 = 0.0;
      rtb_SP_idx_2 = 0.0;

      /* '<S39>:1:9' SPreached = 0; */
      rtb_SPreached = 0;

      /* '<S39>:1:10' landed = 0; */
      rtb_landed = 0;

      /* '<S39>:1:12' if lastGPSsp(1) ~= r_gps_SP(1) ||... */
      /* '<S39>:1:13'    lastGPSsp(2) ~= r_gps_SP(2) ||... */
      /* '<S39>:1:14'    lastGPSsp(3) ~= r_gps_SP(3) */
      if ((flightController_DW->lastGPSsp[0] != flightController_U->r_SP[0]) ||
          (flightController_DW->lastGPSsp[1] != flightController_U->r_SP[1]) ||
          (flightController_DW->lastGPSsp[2] != flightController_U->r_SP[2])) {
        /* '<S39>:1:15' sequence = 0; */
        flightController_DW->sequence = 0.0;
      }

      /* '<S39>:1:17' lastGPSsp = r_gps_SP; */
      flightController_DW->lastGPSsp[0] = flightController_U->r_SP[0];
      flightController_DW->lastGPSsp[1] = flightController_U->r_SP[1];
      flightController_DW->lastGPSsp[2] = flightController_U->r_SP[2];

      /* '<S39>:1:19' if mode == 2 */
      if (flightController_U->mode == 2.0) {
        /* '<S39>:1:20' if sequence == 0 */
        if (flightController_DW->sequence == 0.0) {
          /* '<S39>:1:21' if (r_gps_SP(3) > 0 && abs(r_gps(3) - r_gps_SP(3)) < 1 ) */
          if ((flightController_U->r_SP[2] > 0.0) && (fabs(flightController_B->y
                - flightController_U->r_SP[2]) < 1.0)) {
            /* '<S39>:1:22' r_SP = [r_gps_SP(1);r_gps_SP(2);r_gps_SP(3)]; */
            flightController_DW->r_SP[0] = flightController_U->r_SP[0];
            flightController_DW->r_SP[1] = flightController_U->r_SP[1];
            flightController_DW->r_SP[2] = flightController_U->r_SP[2];

            /* airbourne */
            /* '<S39>:1:23' sequence = 11; */
            flightController_DW->sequence = 11.0;
          } else if (flightController_U->r_SP[2] > 0.0) {
            /* '<S39>:1:24' elseif (r_gps_SP(3) > 0) */
            /* '<S39>:1:25' r_SP = [r_gps(1);r_gps(2);r_gps_SP(3)]; */
            flightController_DW->r_SP[0] = flightController_B->z_r[0];
            flightController_DW->r_SP[1] = flightController_B->z_r[1];
            flightController_DW->r_SP[2] = flightController_U->r_SP[2];

            /* take-off/new altitude */
            /* '<S39>:1:26' sequence = 11; */
            flightController_DW->sequence = 11.0;
          } else {
            /* '<S39>:1:27' else */
            /* '<S39>:1:28' r_SP = [r_gps_SP(1);r_gps_SP(2);r_gps(3)]; */
            flightController_DW->r_SP[0] = flightController_U->r_SP[0];
            flightController_DW->r_SP[1] = flightController_U->r_SP[1];
            flightController_DW->r_SP[2] = flightController_B->y;

            /* land */
            /* '<S39>:1:29' sequence = 12; */
            flightController_DW->sequence = 12.0;
          }
        } else if ((fabs(flightController_B->y - flightController_U->r_SP[2]) <
                    1.0) && (flightController_DW->sequence == 11.0)) {
          /* '<S39>:1:31' elseif abs(r_gps(3) - r_gps_SP(3)) < 1 && sequence == 11 */
          /* airourne */
          /* '<S39>:1:32' sequence = 21; */
          flightController_DW->sequence = 21.0;

          /* '<S39>:1:33' r_SP = r_gps_SP; */
          flightController_DW->r_SP[0] = flightController_U->r_SP[0];
          flightController_DW->r_SP[1] = flightController_U->r_SP[1];
          flightController_DW->r_SP[2] = flightController_U->r_SP[2];
        } else if ((fabs(flightController_B->z_r[0] - flightController_U->r_SP[0])
                    < 1.0) && (fabs(flightController_B->z_r[1] -
                     flightController_U->r_SP[1]) < 1.0) &&
                   (flightController_DW->sequence == 12.0)) {
          /* '<S39>:1:34' elseif abs(r_gps(1) - r_gps_SP(1)) < 1 &&... */
          /* '<S39>:1:35'            abs(r_gps(2) - r_gps_SP(2)) < 1 && sequence == 12 */
          /* landing */
          /* '<S39>:1:36' sequence = 22; */
          flightController_DW->sequence = 22.0;

          /* '<S39>:1:37' r_SP = [r_gps_SP(1); r_gps_SP(2); -10]; */
          flightController_DW->r_SP[0] = flightController_U->r_SP[0];
          flightController_DW->r_SP[1] = flightController_U->r_SP[1];
          flightController_DW->r_SP[2] = -10.0;
        } else if ((flightController_DW->DiscreteTimeIntegrator2_DSTATE < 0.3) &&
                   (flightController_DW->sequence == 22.0)) {
          /* '<S39>:1:38' elseif z_IR < 0.3 && sequence == 22 */
          /* landed */
          /* '<S39>:1:39' landed = 1; */
          rtb_landed = 1;
        } else {
          rtb_Switch_l[0] = fabs(flightController_U->r_SP[0] -
            flightController_B->z_r[0]);
          rtb_Switch_l[1] = fabs(flightController_U->r_SP[1] -
            flightController_B->z_r[1]);
          rtb_Switch_l[2] = fabs(flightController_U->r_SP[2] -
            flightController_B->y);
          rtb_Compare_cy = true;
          i = 0;
          rtb_LogicalOperator_l = false;
          while ((!rtb_LogicalOperator_l) && (i < 3)) {
            if (!(rtb_Switch_l[i] < 1.0)) {
              rtb_Compare_cy = false;
              rtb_LogicalOperator_l = true;
            } else {
              i++;
            }
          }

          if (rtb_Compare_cy && (flightController_DW->sequence == 21.0)) {
            /* '<S39>:1:40' elseif all(abs(r_gps_SP - r_gps) < 1) && sequence == 21 */
            /* airbourne */
            /* '<S39>:1:41' SPreached = 1; */
            rtb_SPreached = 1;
          }
        }

        /* '<S39>:1:43' SP = r_SP; */
        rtb_SP_idx_0 = flightController_DW->r_SP[0];
        rtb_SP_idx_1 = flightController_DW->r_SP[1];
        rtb_SP_idx_2 = flightController_DW->r_SP[2];
      } else {
        /* '<S39>:1:44' else */
        /* '<S39>:1:45' sequence = 0; */
        flightController_DW->sequence = 0.0;

        /* '<S39>:1:46' r_SP = r_gps; */
        flightController_DW->r_SP[0] = flightController_B->z_r[0];
        flightController_DW->r_SP[1] = flightController_B->z_r[1];
        flightController_DW->r_SP[2] = flightController_B->y;
      }

      /* End of MATLAB Function: '<S34>/mode2sequence' */

      /* MultiPortSwitch: '<S34>/Multiport Switch' */
      if (!rtb_Compare_gt) {
        flightController_B->MultiportSwitch[0] = rtb_SP_idx_0;
        flightController_B->MultiportSwitch[1] = rtb_SP_idx_1;
        flightController_B->MultiportSwitch[2] = rtb_SP_idx_2;
      } else {
        flightController_B->MultiportSwitch[0] = flightController_B->Switch_k[0];
        flightController_B->MultiportSwitch[1] = flightController_B->Switch_k[1];
        flightController_B->MultiportSwitch[2] = flightController_B->Switch_k[2];
      }

      /* End of MultiPortSwitch: '<S34>/Multiport Switch' */

      /* DiscreteIntegrator: '<S33>/Discrete-Time Integrator2' */
      if (flightController_DW->DiscreteTimeIntegrator2_IC_LOAD != 0) {
        flightController_DW->DiscreteTimeIntegrator2_DSTAT_c[0] =
          flightController_B->MultiportSwitch[0];
        flightController_DW->DiscreteTimeIntegrator2_DSTAT_c[1] =
          flightController_B->MultiportSwitch[1];
        flightController_DW->DiscreteTimeIntegrator2_DSTAT_c[2] =
          flightController_B->MultiportSwitch[2];
      }

      flightController_B->DiscreteTimeIntegrator2[0] =
        flightController_DW->DiscreteTimeIntegrator2_DSTAT_c[0];
      flightController_B->DiscreteTimeIntegrator2[1] =
        flightController_DW->DiscreteTimeIntegrator2_DSTAT_c[1];
      flightController_B->DiscreteTimeIntegrator2[2] =
        flightController_DW->DiscreteTimeIntegrator2_DSTAT_c[2];

      /* End of DiscreteIntegrator: '<S33>/Discrete-Time Integrator2' */
    }

    /* Switch: '<S6>/Switch2' */
    if (rtb_Compare_gs) {
      rtb_Delay_idx_0 = flightController_B->MultiportSwitch[0];
    } else {
      rtb_Delay_idx_0 = flightController_B->DiscreteTimeIntegrator2[0];
    }

    /* Sum: '<S35>/Sum6' */
    flightController_B->Sum6[0] = rtb_Delay_idx_0 - flightController_B->z_r[0];

    /* Switch: '<S6>/Switch2' */
    if (rtb_Compare_gs) {
      rtb_Delay_idx_0 = flightController_B->MultiportSwitch[1];
    } else {
      rtb_Delay_idx_0 = flightController_B->DiscreteTimeIntegrator2[1];
    }

    /* Sum: '<S35>/Sum6' */
    flightController_B->Sum6[1] = rtb_Delay_idx_0 - flightController_B->z_r[1];

    /* Switch: '<S6>/Switch2' */
    if (rtb_Compare_gs) {
      rtb_Delay_idx_0 = flightController_B->MultiportSwitch[2];
    } else {
      rtb_Delay_idx_0 = flightController_B->DiscreteTimeIntegrator2[2];
    }

    /* Sum: '<S35>/Sum6' */
    flightController_B->Sum6[2] = rtb_Delay_idx_0 - flightController_B->y;
    if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
      /* Product: '<S35>/Product2' incorporates:
       *  DiscreteIntegrator: '<S35>/Discrete-Time Integrator1'
       *  Inport: '<Root>/PID_r'
       */
      flightController_B->Product2[0] =
        flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[0] *
        flightController_U->PID_r[1];
      flightController_B->Product2[1] =
        flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[1] *
        flightController_U->PID_r[4];
      flightController_B->Product2[2] =
        flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[2] *
        flightController_U->PID_r[7];

      /* Delay: '<S35>/Delay1' */
      if (flightController_DW->icLoad_do != 0) {
        flightController_DW->Delay1_DSTATE_g[0] = flightController_B->Sum6[0];
        flightController_DW->Delay1_DSTATE_g[1] = flightController_B->Sum6[1];
        flightController_DW->Delay1_DSTATE_g[2] = flightController_B->Sum6[2];
      }

      flightController_B->Delay1[0] = flightController_DW->Delay1_DSTATE_g[0];
      flightController_B->Delay1[1] = flightController_DW->Delay1_DSTATE_g[1];
      flightController_B->Delay1[2] = flightController_DW->Delay1_DSTATE_g[2];

      /* End of Delay: '<S35>/Delay1' */
    }

    /* Switch: '<S6>/Switch' incorporates:
     *  Constant: '<S35>/Constant2'
     *  Inport: '<Root>/PID_r'
     *  Product: '<S35>/Divide1'
     *  Product: '<S35>/Product1'
     *  Sum: '<S35>/Sum2'
     *  Sum: '<S35>/Sum3'
     */
    if (flightController_B->OutportBufferForE_v_SP[0]) {
      c_idx_1 = flightController_B->OutportBufferForx_hat_v[0];
    } else {
      c_idx_1 = 1.0 / flightController_P->dt_p * (flightController_B->Sum6[0] -
        flightController_B->Delay1[0]) * flightController_U->PID_r[2] +
        (flightController_U->PID_r[0] * flightController_B->Sum6[0] +
         flightController_B->Product2[0]);
    }

    /* Saturate: '<S6>/Saturation3' */
    if (c_idx_1 > flightController_P->Saturation3_UpperSat[0]) {
      /* Outport: '<Root>/v_SP_log' */
      flightController_Y->v_SP_log[0] = flightController_P->
        Saturation3_UpperSat[0];
    } else if (c_idx_1 < flightController_P->Saturation3_LowerSat[0]) {
      /* Outport: '<Root>/v_SP_log' */
      flightController_Y->v_SP_log[0] = flightController_P->
        Saturation3_LowerSat[0];
    } else {
      /* Outport: '<Root>/v_SP_log' */
      flightController_Y->v_SP_log[0] = c_idx_1;
    }

    /* Switch: '<S6>/Switch' incorporates:
     *  Constant: '<S35>/Constant2'
     *  Inport: '<Root>/PID_r'
     *  Product: '<S35>/Divide1'
     *  Product: '<S35>/Product1'
     *  Sum: '<S35>/Sum2'
     *  Sum: '<S35>/Sum3'
     */
    if (flightController_B->OutportBufferForE_v_SP[1]) {
      c_idx_1 = flightController_B->OutportBufferForx_hat_v[1];
    } else {
      c_idx_1 = 1.0 / flightController_P->dt_p * (flightController_B->Sum6[1] -
        flightController_B->Delay1[1]) * flightController_U->PID_r[5] +
        (flightController_U->PID_r[3] * flightController_B->Sum6[1] +
         flightController_B->Product2[1]);
    }

    /* Saturate: '<S6>/Saturation3' */
    if (c_idx_1 > flightController_P->Saturation3_UpperSat[1]) {
      /* Outport: '<Root>/v_SP_log' */
      flightController_Y->v_SP_log[1] = flightController_P->
        Saturation3_UpperSat[1];
    } else if (c_idx_1 < flightController_P->Saturation3_LowerSat[1]) {
      /* Outport: '<Root>/v_SP_log' */
      flightController_Y->v_SP_log[1] = flightController_P->
        Saturation3_LowerSat[1];
    } else {
      /* Outport: '<Root>/v_SP_log' */
      flightController_Y->v_SP_log[1] = c_idx_1;
    }

    /* Switch: '<S6>/Switch' incorporates:
     *  Constant: '<S35>/Constant2'
     *  Inport: '<Root>/PID_r'
     *  Product: '<S35>/Divide1'
     *  Product: '<S35>/Product1'
     *  Sum: '<S35>/Sum2'
     *  Sum: '<S35>/Sum3'
     */
    if (flightController_B->OutportBufferForE_v_SP[2]) {
      c_idx_1 = flightController_B->SP[2];
    } else {
      c_idx_1 = 1.0 / flightController_P->dt_p * (flightController_B->Sum6[2] -
        flightController_B->Delay1[2]) * flightController_U->PID_r[8] +
        (flightController_U->PID_r[6] * flightController_B->Sum6[2] +
         flightController_B->Product2[2]);
    }

    /* Saturate: '<S6>/Saturation3' */
    if (c_idx_1 > flightController_P->Saturation3_UpperSat[2]) {
      /* Outport: '<Root>/v_SP_log' */
      flightController_Y->v_SP_log[2] = flightController_P->
        Saturation3_UpperSat[2];
    } else if (c_idx_1 < flightController_P->Saturation3_LowerSat[2]) {
      /* Outport: '<Root>/v_SP_log' */
      flightController_Y->v_SP_log[2] = flightController_P->
        Saturation3_LowerSat[2];
    } else {
      /* Outport: '<Root>/v_SP_log' */
      flightController_Y->v_SP_log[2] = c_idx_1;
    }

    /* Sum: '<S36>/Sum4' */
    flightController_B->Sum4[0] = flightController_Y->v_SP_log[0] -
      flightController_B->OutportBufferForx_hat_v[0];
    flightController_B->Sum4[1] = flightController_Y->v_SP_log[1] -
      flightController_B->OutportBufferForx_hat_v[1];
    flightController_B->Sum4[2] = flightController_Y->v_SP_log[2] -
      flightController_B->OutportBufferForx_hat_v[2];
    if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
      /* Product: '<S36>/Product5' incorporates:
       *  DiscreteIntegrator: '<S36>/Discrete-Time Integrator'
       *  Inport: '<Root>/PID_v'
       */
      flightController_B->Product5[0] =
        flightController_DW->DiscreteTimeIntegrator_DSTATE_l[0] *
        flightController_U->PID_v[1];
      flightController_B->Product5[1] =
        flightController_DW->DiscreteTimeIntegrator_DSTATE_l[1] *
        flightController_U->PID_v[4];
      flightController_B->Product5[2] =
        flightController_DW->DiscreteTimeIntegrator_DSTATE_l[2] *
        flightController_U->PID_v[7];

      /* Delay: '<S36>/Delay' */
      if (flightController_DW->icLoad_k != 0) {
        flightController_DW->Delay_DSTATE_f[0] = flightController_B->Sum4[0];
        flightController_DW->Delay_DSTATE_f[1] = flightController_B->Sum4[1];
        flightController_DW->Delay_DSTATE_f[2] = flightController_B->Sum4[2];
      }

      flightController_B->Delay[0] = flightController_DW->Delay_DSTATE_f[0];
      flightController_B->Delay[1] = flightController_DW->Delay_DSTATE_f[1];
      flightController_B->Delay[2] = flightController_DW->Delay_DSTATE_f[2];

      /* End of Delay: '<S36>/Delay' */
    }

    /* Product: '<S36>/Divide' incorporates:
     *  Constant: '<S36>/Constant1'
     */
    rtb_Delay_idx_0 = 1.0 / flightController_P->dt_p;

    /* Saturate: '<S6>/Saturation1' incorporates:
     *  Constant: '<S36>/Constant'
     *  Inport: '<Root>/PID_v'
     *  Product: '<S36>/Divide'
     *  Product: '<S36>/Product4'
     *  Product: '<S6>/Matrix Multiply1'
     *  Sum: '<S36>/Sum'
     *  Sum: '<S36>/Sum1'
     *  Sum: '<S36>/Sum5'
     */
    c_idx_1 = ((flightController_B->Sum4[0] - flightController_B->Delay[0]) *
               rtb_Delay_idx_0 * flightController_U->PID_v[2] +
               (flightController_U->PID_v[0] * flightController_B->Sum4[0] +
                flightController_B->Product5[0])) +
      flightController_P->Constant_Value_d5[0];
    if (c_idx_1 > flightController_P->Saturation1_UpperSat[0]) {
      c_idx_1 = flightController_P->Saturation1_UpperSat[0];
    } else {
      if (c_idx_1 < flightController_P->Saturation1_LowerSat[0]) {
        c_idx_1 = flightController_P->Saturation1_LowerSat[0];
      }
    }

    rtb_Delay_idx_1 = ((flightController_B->Sum4[1] - flightController_B->Delay
                        [1]) * rtb_Delay_idx_0 * flightController_U->PID_v[5] +
                       (flightController_U->PID_v[3] * flightController_B->Sum4
                        [1] + flightController_B->Product5[1])) +
      flightController_P->Constant_Value_d5[1];
    if (rtb_Delay_idx_1 > flightController_P->Saturation1_UpperSat[1]) {
      rtb_Delay_idx_1 = flightController_P->Saturation1_UpperSat[1];
    } else {
      if (rtb_Delay_idx_1 < flightController_P->Saturation1_LowerSat[1]) {
        rtb_Delay_idx_1 = flightController_P->Saturation1_LowerSat[1];
      }
    }

    rtb_Delay_idx_0 = ((flightController_B->Sum4[2] - flightController_B->Delay
                        [2]) * rtb_Delay_idx_0 * flightController_U->PID_v[8] +
                       (flightController_U->PID_v[6] * flightController_B->Sum4
                        [2] + flightController_B->Product5[2])) +
      flightController_P->Constant_Value_d5[2];
    if (rtb_Delay_idx_0 > flightController_P->Saturation1_UpperSat[2]) {
      rtb_Delay_idx_0 = flightController_P->Saturation1_UpperSat[2];
    } else {
      if (rtb_Delay_idx_0 < flightController_P->Saturation1_LowerSat[2]) {
        rtb_Delay_idx_0 = flightController_P->Saturation1_LowerSat[2];
      }
    }

    /* End of Saturate: '<S6>/Saturation1' */

    /* Product: '<S6>/Matrix Multiply1' */
    for (i = 0; i < 3; i++) {
      flightController_B->MatrixMultiply1[i] = 0.0;
      flightController_B->MatrixMultiply1[i] += flightController_B->R_z[i] *
        c_idx_1;
      flightController_B->MatrixMultiply1[i] += flightController_B->R_z[i + 3] *
        rtb_Delay_idx_1;
      flightController_B->MatrixMultiply1[i] += flightController_B->R_z[i + 6] *
        rtb_Delay_idx_0;
    }

    if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
      /* SignalConversion: '<S6>/OutportBufferForlanded' */
      flightController_B->OutportBufferForlanded = rtb_landed;

      /* SignalConversion: '<S6>/OutportBufferForspReached' */
      flightController_B->OutportBufferForspReached = rtb_SPreached;

      /* Delay: '<S40>/Delay3' */
      if (flightController_DW->icLoad_o != 0) {
        flightController_DW->Delay3_DSTATE[0] = rtb_SP_idx_0;
        flightController_DW->Delay3_DSTATE[1] = rtb_SP_idx_1;
        flightController_DW->Delay3_DSTATE[2] = rtb_SP_idx_2;
      }

      /* Sum: '<S40>/Sum5' incorporates:
       *  Delay: '<S40>/Delay3'
       */
      flightController_B->Sum5[0] = rtb_SP_idx_0 -
        flightController_DW->Delay3_DSTATE[0];
      flightController_B->Sum5[1] = rtb_SP_idx_1 -
        flightController_DW->Delay3_DSTATE[1];
      flightController_B->Sum5[2] = rtb_SP_idx_2 -
        flightController_DW->Delay3_DSTATE[2];
    }

    /* Logic: '<S40>/Logical Operator' incorporates:
     *  Constant: '<S43>/Constant'
     *  Constant: '<S44>/Constant'
     *  RelationalOperator: '<S43>/Compare'
     *  RelationalOperator: '<S44>/Compare'
     */
    rtb_Compare_cy = ((flightController_B->Sum5[0] !=
                       flightController_P->CompareToConstant5_const_j) ||
                      (flightController_B->Sum5[1] !=
                       flightController_P->CompareToConstant1_const_p));
    if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
      /* RelationalOperator: '<S42>/Compare' incorporates:
       *  Constant: '<S42>/Constant'
       */
      rtb_LogicalOperator_l = (flightController_U->mode >=
        flightController_P->CompareToConstant_const_f);

      /* Delay: '<S40>/Delay' */
      if (rtb_LogicalOperator_l && (flightController_PrevZCX->Delay_Reset_ZCE !=
           POS_ZCSIG)) {
        flightController_DW->icLoad_a = 1U;
      }

      flightController_PrevZCX->Delay_Reset_ZCE = rtb_LogicalOperator_l;
      if (flightController_DW->icLoad_a != 0) {
        flightController_DW->Delay_DSTATE_e =
          flightController_B->RateTransition3[2];
      }

      flightController_B->Delay_c = flightController_DW->Delay_DSTATE_e;

      /* End of Delay: '<S40>/Delay' */
    }

    /* Outputs for Enabled SubSystem: '<S40>/psiCalc' incorporates:
     *  EnablePort: '<S45>/Enable'
     */
    if (rtb_Compare_cy) {
      if (!flightController_DW->psiCalc_MODE) {
        flightController_DW->psiCalc_MODE = true;
      }

      /* Gain: '<S45>/Gain1' incorporates:
       *  Constant: '<S45>/Constant'
       *  Product: '<S45>/Divide'
       *  Rounding: '<S45>/Rounding Function'
       */
      rtb_Divide2 = floor(flightController_B->RateTransition3[2] /
                          flightController_P->Constant_Value_d) *
        flightController_P->Gain1_Gain_j;
      if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
        /* Gain: '<S45>/Gain' incorporates:
         *  Sum: '<S45>/Sum'
         *  Trigonometry: '<S45>/Trigonometric Function'
         */
        flightController_B->Gain = rt_atan2d_snf(rtb_SP_idx_0 -
          flightController_B->z_r[0], rtb_SP_idx_1 - flightController_B->z_r[1])
          * flightController_P->Gain_Gain_i;
      }

      /* Switch: '<S45>/Switch' incorporates:
       *  Abs: '<S45>/Abs'
       *  Constant: '<S45>/Constant1'
       *  Constant: '<S45>/Constant2'
       *  Constant: '<S46>/Constant'
       *  RelationalOperator: '<S46>/Compare'
       *  Sum: '<S45>/Add'
       */
      if (fabs((flightController_B->RateTransition3[2] - rtb_Divide2) -
               flightController_B->Gain) >
          flightController_P->CompareToConstant_const_e) {
        rtb_Delay_idx_0 = flightController_P->Constant1_Value_j;
      } else {
        rtb_Delay_idx_0 = flightController_P->Constant2_Value;
      }

      /* End of Switch: '<S45>/Switch' */

      /* Sum: '<S45>/Sum1' incorporates:
       *  Sum: '<S45>/Sum3'
       */
      flightController_B->Sum1 = (rtb_Delay_idx_0 + rtb_Divide2) +
        flightController_B->Gain;
    } else {
      if (flightController_DW->psiCalc_MODE) {
        flightController_DW->psiCalc_MODE = false;
      }
    }

    /* End of Outputs for SubSystem: '<S40>/psiCalc' */

    /* MultiPortSwitch: '<S40>/Multiport Switch2' */
    if (!rtb_Compare_cy) {
      flightController_B->MultiportSwitch2 = flightController_B->Delay_c;
    } else {
      flightController_B->MultiportSwitch2 = flightController_B->Sum1;
    }

    /* End of MultiPortSwitch: '<S40>/Multiport Switch2' */

    /* Switch: '<S6>/Switch1' */
    if (flightController_B->OutportBufferForE_v_SP[0]) {
      flightController_B->Switch1[0] = flightController_B->SP[0];
    } else {
      flightController_B->Switch1[0] = flightController_B->MatrixMultiply1[0];
    }

    if (flightController_B->OutportBufferForE_v_SP[1]) {
      flightController_B->Switch1[1] = flightController_B->SP[1];
    } else {
      flightController_B->Switch1[1] = flightController_B->MatrixMultiply1[1];
    }

    flightController_B->Switch1[2] = flightController_B->MultiportSwitch2;

    /* End of Switch: '<S6>/Switch1' */
    if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
      /* Update for DiscreteIntegrator: '<S38>/Discrete-Time Integrator2' incorporates:
       *  Gain: '<S38>/Gain1'
       *  Inport: '<Root>/z_IR'
       *  Sum: '<S38>/Sum1'
       */
      flightController_DW->DiscreteTimeIntegrator2_DSTATE +=
        (flightController_U->z_IR - rtb_DiscreteTimeIntegrator2) *
        flightController_P->Gain1_Gain_ka *
        flightController_P->DiscreteTimeIntegrator2_gainv_d;

      /* Update for DiscreteIntegrator: '<S33>/Discrete-Time Integrator2' incorporates:
       *  Gain: '<S33>/Gain1'
       *  Sum: '<S33>/Sum1'
       */
      flightController_DW->DiscreteTimeIntegrator2_IC_LOAD = 0U;
      flightController_DW->DiscreteTimeIntegrator2_DSTAT_c[0] +=
        (flightController_B->MultiportSwitch[0] -
         flightController_B->DiscreteTimeIntegrator2[0]) *
        flightController_P->Gain1_Gain_k[0] *
        flightController_P->DiscreteTimeIntegrator2_gainv_f;
      flightController_DW->DiscreteTimeIntegrator2_DSTAT_c[1] +=
        (flightController_B->MultiportSwitch[1] -
         flightController_B->DiscreteTimeIntegrator2[1]) *
        flightController_P->Gain1_Gain_k[1] *
        flightController_P->DiscreteTimeIntegrator2_gainv_f;
      flightController_DW->DiscreteTimeIntegrator2_DSTAT_c[2] +=
        (flightController_B->MultiportSwitch[2] -
         flightController_B->DiscreteTimeIntegrator2[2]) *
        flightController_P->Gain1_Gain_k[2] *
        flightController_P->DiscreteTimeIntegrator2_gainv_f;

      /* Update for DiscreteIntegrator: '<S35>/Discrete-Time Integrator1' */
      flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[0] +=
        flightController_P->DiscreteTimeIntegrator1_gainv_n *
        flightController_B->Sum6[0];
      flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[1] +=
        flightController_P->DiscreteTimeIntegrator1_gainv_n *
        flightController_B->Sum6[1];
      flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[2] +=
        flightController_P->DiscreteTimeIntegrator1_gainv_n *
        flightController_B->Sum6[2];
      if (flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[0] >=
          flightController_P->DiscreteTimeIntegrator1_UpperSa[0]) {
        flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[0] =
          flightController_P->DiscreteTimeIntegrator1_UpperSa[0];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[0] <=
            flightController_P->DiscreteTimeIntegrator1_LowerSa[0]) {
          flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[0] =
            flightController_P->DiscreteTimeIntegrator1_LowerSa[0];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[1] >=
          flightController_P->DiscreteTimeIntegrator1_UpperSa[1]) {
        flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[1] =
          flightController_P->DiscreteTimeIntegrator1_UpperSa[1];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[1] <=
            flightController_P->DiscreteTimeIntegrator1_LowerSa[1]) {
          flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[1] =
            flightController_P->DiscreteTimeIntegrator1_LowerSa[1];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[2] >=
          flightController_P->DiscreteTimeIntegrator1_UpperSa[2]) {
        flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[2] =
          flightController_P->DiscreteTimeIntegrator1_UpperSa[2];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[2] <=
            flightController_P->DiscreteTimeIntegrator1_LowerSa[2]) {
          flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[2] =
            flightController_P->DiscreteTimeIntegrator1_LowerSa[2];
        }
      }

      /* End of Update for DiscreteIntegrator: '<S35>/Discrete-Time Integrator1' */

      /* Update for Delay: '<S35>/Delay1' */
      flightController_DW->icLoad_do = 0U;
      flightController_DW->Delay1_DSTATE_g[0] = flightController_B->Sum6[0];
      flightController_DW->Delay1_DSTATE_g[1] = flightController_B->Sum6[1];
      flightController_DW->Delay1_DSTATE_g[2] = flightController_B->Sum6[2];

      /* Update for DiscreteIntegrator: '<S36>/Discrete-Time Integrator' */
      flightController_DW->DiscreteTimeIntegrator_DSTATE_l[0] +=
        flightController_P->DiscreteTimeIntegrator_gainva_h *
        flightController_B->Sum4[0];
      flightController_DW->DiscreteTimeIntegrator_DSTATE_l[1] +=
        flightController_P->DiscreteTimeIntegrator_gainva_h *
        flightController_B->Sum4[1];
      flightController_DW->DiscreteTimeIntegrator_DSTATE_l[2] +=
        flightController_P->DiscreteTimeIntegrator_gainva_h *
        flightController_B->Sum4[2];
      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_l[0] >=
          flightController_P->DiscreteTimeIntegrator_UpperS_g[0]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_l[0] =
          flightController_P->DiscreteTimeIntegrator_UpperS_g[0];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_l[0] <=
            flightController_P->DiscreteTimeIntegrator_Lower_lv[0]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_l[0] =
            flightController_P->DiscreteTimeIntegrator_Lower_lv[0];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_l[1] >=
          flightController_P->DiscreteTimeIntegrator_UpperS_g[1]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_l[1] =
          flightController_P->DiscreteTimeIntegrator_UpperS_g[1];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_l[1] <=
            flightController_P->DiscreteTimeIntegrator_Lower_lv[1]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_l[1] =
            flightController_P->DiscreteTimeIntegrator_Lower_lv[1];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_l[2] >=
          flightController_P->DiscreteTimeIntegrator_UpperS_g[2]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_l[2] =
          flightController_P->DiscreteTimeIntegrator_UpperS_g[2];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_l[2] <=
            flightController_P->DiscreteTimeIntegrator_Lower_lv[2]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_l[2] =
            flightController_P->DiscreteTimeIntegrator_Lower_lv[2];
        }
      }

      /* End of Update for DiscreteIntegrator: '<S36>/Discrete-Time Integrator' */

      /* Update for Delay: '<S36>/Delay' */
      flightController_DW->icLoad_k = 0U;
      flightController_DW->Delay_DSTATE_f[0] = flightController_B->Sum4[0];
      flightController_DW->Delay_DSTATE_f[1] = flightController_B->Sum4[1];
      flightController_DW->Delay_DSTATE_f[2] = flightController_B->Sum4[2];

      /* Update for Delay: '<S40>/Delay3' */
      flightController_DW->icLoad_o = 0U;
      flightController_DW->Delay3_DSTATE[0] = rtb_SP_idx_0;
      flightController_DW->Delay3_DSTATE[1] = rtb_SP_idx_1;
      flightController_DW->Delay3_DSTATE[2] = rtb_SP_idx_2;

      /* Update for Delay: '<S40>/Delay' */
      flightController_DW->icLoad_a = 0U;
      flightController_DW->Delay_DSTATE_e = flightController_B->MultiportSwitch2;
    }
  } else {
    if (flightController_DW->poController_MODE) {
      /* Disable for Enabled SubSystem: '<S34>/velocitySPmanager' */
      if (flightController_DW->velocitySPmanager_MODE) {
        /* Disable for Outport: '<S41>/r_SP' */
        flightController_B->Switch_k[0] = flightController_P->r_SP_Y0[0];
        flightController_B->Switch_k[1] = flightController_P->r_SP_Y0[1];
        flightController_B->Switch_k[2] = flightController_P->r_SP_Y0[2];

        /* Disable for Outport: '<S41>/E_v_SP' */
        flightController_B->OutportBufferForE_v_SP[0] =
          flightController_P->E_v_SP_Y0[0];
        flightController_B->OutportBufferForE_v_SP[1] =
          flightController_P->E_v_SP_Y0[1];
        flightController_B->OutportBufferForE_v_SP[2] =
          flightController_P->E_v_SP_Y0[2];

        /* Disable for Outport: '<S41>/v_SP' */
        flightController_B->SP[0] = flightController_P->v_SP_Y0[0];
        flightController_B->SP[1] = flightController_P->v_SP_Y0[1];
        flightController_B->SP[2] = flightController_P->v_SP_Y0[2];
        flightController_DW->velocitySPmanager_MODE = false;
      }

      /* End of Disable for SubSystem: '<S34>/velocitySPmanager' */

      /* Disable for Enabled SubSystem: '<S40>/psiCalc' */
      if (flightController_DW->psiCalc_MODE) {
        flightController_DW->psiCalc_MODE = false;
      }

      /* End of Disable for SubSystem: '<S40>/psiCalc' */

      /* Disable for Outport: '<S6>/Phi_SP' */
      flightController_B->Switch1[0] = flightController_P->Phi_SP_Y0[0];
      flightController_B->Switch1[1] = flightController_P->Phi_SP_Y0[1];
      flightController_B->Switch1[2] = flightController_P->Phi_SP_Y0[2];

      /* Disable for Outport: '<Root>/v_SP_log' incorporates:
       *  Disable for Outport: '<S6>/v_log_SP'
       */
      flightController_Y->v_SP_log[0] = flightController_P->v_log_SP_Y0[0];
      flightController_Y->v_SP_log[1] = flightController_P->v_log_SP_Y0[1];
      flightController_Y->v_SP_log[2] = flightController_P->v_log_SP_Y0[2];

      /* Disable for Outport: '<S6>/spReached' */
      flightController_B->OutportBufferForspReached =
        flightController_P->spReached_Y0;

      /* Disable for Outport: '<S6>/landed' */
      flightController_B->OutportBufferForlanded = flightController_P->landed_Y0;
      flightController_DW->poController_MODE = false;
    }
  }

  /* End of Logic: '<S1>/Logical Operator1' */
  /* End of Outputs for SubSystem: '<S1>/poController' */

  /* Outputs for Enabled SubSystem: '<S1>/orController' incorporates:
   *  EnablePort: '<S4>/Enable'
   */
  /* Inport: '<Root>/armed' */
  if (flightController_U->armed > 0.0) {
    if (!flightController_DW->orController_MODE) {
      flightController_DW->orController_MODE = true;
    }

    /* Switch: '<S4>/Switch' incorporates:
     *  Constant: '<S10>/Constant'
     *  Inport: '<Root>/mode'
     *  RelationalOperator: '<S10>/Compare'
     */
    if (flightController_U->mode ==
        flightController_P->CompareToConstant1_const_n) {
      /* Switch: '<S3>/Switch1' incorporates:
       *  Inport: '<Root>/RCok'
       */
      if (flightController_U->RCok > flightController_P->Switch1_Threshold) {
        /* Outport: '<Root>/throttle_log' incorporates:
         *  Constant: '<S3>/Constant'
         *  Constant: '<S3>/Constant1'
         *  Inport: '<Root>/RCctrl'
         *  Product: '<S3>/Product'
         *  Sum: '<S3>/Sum'
         */
        flightController_Y->throttle_log = flightController_U->RCctrl[3] *
          flightController_P->Constant_Value +
          flightController_P->Constant1_Value;
      } else {
        /* Outport: '<Root>/throttle_log' incorporates:
         *  Constant: '<S3>/Constant8'
         */
        flightController_Y->throttle_log = flightController_P->Constant8_Value;
      }

      /* End of Switch: '<S3>/Switch1' */
    } else {
      /* Outport: '<Root>/throttle_log' */
      flightController_Y->throttle_log = flightController_B->MatrixMultiply1[2];
    }

    /* End of Switch: '<S4>/Switch' */

    /* RelationalOperator: '<S9>/Compare' incorporates:
     *  Constant: '<S9>/Constant'
     */
    rtb_LogicalOperator_l = (flightController_Y->throttle_log >=
      flightController_P->CompareToConstant_const);

    /* Sum: '<S4>/Sum4' incorporates:
     *  Constant: '<S4>/Constant1'
     */
    rtb_Divide2 = flightController_Y->throttle_log -
      flightController_P->Constant1_Value_h;

    /* Gain: '<S4>/Gain' */
    rtb_Polynomial2 = flightController_P->Gain_Gain * rtb_Divide2;

    /* RelationalOperator: '<S16>/Compare' incorporates:
     *  Constant: '<S16>/Constant'
     *  Inport: '<Root>/mode'
     */
    rtb_Compare_cy = (flightController_U->mode <=
                      flightController_P->CompareToConstant_const_b);

    /* Outputs for Enabled SubSystem: '<S12>/psi_manual' incorporates:
     *  EnablePort: '<S19>/Enable'
     */
    if (rtb_Compare_cy) {
      if (!flightController_DW->psi_manual_MODE) {
        /* InitializeConditions for Delay: '<S19>/Delay1' */
        flightController_DW->icLoad_f = 1U;
        flightController_DW->psi_manual_MODE = true;
      }

      /* RelationalOperator: '<S20>/Compare' incorporates:
       *  Abs: '<S19>/Abs1'
       *  Constant: '<S20>/Constant'
       */
      flightController_B->Compare = (fabs(rtb_MultiportSwitch_idx_2) >
        flightController_P->CompareToConstant1_const);

      /* Delay: '<S19>/Delay1' */
      if (flightController_PrevZCX->Delay1_Reset_ZCE != POS_ZCSIG) {
        flightController_DW->icLoad_f = 1U;
      }

      flightController_PrevZCX->Delay1_Reset_ZCE = 1U;
      if (flightController_DW->icLoad_f != 0) {
        flightController_DW->Delay1_DSTATE_g4 = rtb_y_n_idx_0;
      }

      /* MultiPortSwitch: '<S19>/Multiport Switch1' incorporates:
       *  Delay: '<S19>/Delay1'
       */
      if (!flightController_B->Compare) {
        flightController_B->MultiportSwitch1 =
          flightController_DW->Delay1_DSTATE_g4;
      } else {
        flightController_B->MultiportSwitch1 = rtb_y_n_idx_0;
      }

      /* End of MultiPortSwitch: '<S19>/Multiport Switch1' */

      /* Update for Delay: '<S19>/Delay1' */
      flightController_DW->icLoad_f = 0U;
      flightController_DW->Delay1_DSTATE_g4 =
        flightController_B->MultiportSwitch1;
    } else {
      if (flightController_DW->psi_manual_MODE) {
        /* Disable for Outport: '<S19>/E_Dpsi_SP' */
        flightController_B->Compare = flightController_P->E_Dpsi_SP_Y0;

        /* Disable for Outport: '<S19>/psi_SP' */
        flightController_B->MultiportSwitch1 = flightController_P->psi_SP_Y0;
        flightController_DW->psi_manual_MODE = false;
      }
    }

    /* End of Outputs for SubSystem: '<S12>/psi_manual' */

    /* Outputs for Enabled SubSystem: '<S12>/Autonomous' incorporates:
     *  EnablePort: '<S15>/Enable'
     */
    /* RelationalOperator: '<S17>/Compare' incorporates:
     *  Constant: '<S17>/Constant'
     *  Inport: '<Root>/mode'
     */
    if (flightController_U->mode >=
        flightController_P->CompareToConstant1_const_ni) {
      if (!flightController_DW->Autonomous_MODE) {
        /* InitializeConditions for DiscreteIntegrator: '<S15>/Discrete-Time Integrator2' */
        flightController_DW->DiscreteTimeIntegrator2_DSTAT_p[0] =
          flightController_P->DiscreteTimeIntegrator2_IC[0];
        flightController_DW->DiscreteTimeIntegrator2_DSTAT_p[1] =
          flightController_P->DiscreteTimeIntegrator2_IC[1];
        flightController_DW->Autonomous_MODE = true;
      }

      /* DiscreteIntegrator: '<S15>/Discrete-Time Integrator2' */
      flightController_B->DiscreteTimeIntegrator2_m[0] =
        flightController_DW->DiscreteTimeIntegrator2_DSTAT_p[0];
      flightController_B->DiscreteTimeIntegrator2_m[1] =
        flightController_DW->DiscreteTimeIntegrator2_DSTAT_p[1];

      /* Update for DiscreteIntegrator: '<S15>/Discrete-Time Integrator2' incorporates:
       *  Gain: '<S15>/Gain1'
       *  Sum: '<S15>/Sum1'
       */
      flightController_DW->DiscreteTimeIntegrator2_DSTAT_p[0] +=
        (flightController_B->Switch1[0] -
         flightController_B->DiscreteTimeIntegrator2_m[0]) *
        flightController_P->Gain1_Gain_i *
        flightController_P->DiscreteTimeIntegrator2_gainval;
      flightController_DW->DiscreteTimeIntegrator2_DSTAT_p[1] +=
        (flightController_B->Switch1[1] -
         flightController_B->DiscreteTimeIntegrator2_m[1]) *
        flightController_P->Gain1_Gain_i *
        flightController_P->DiscreteTimeIntegrator2_gainval;
    } else {
      if (flightController_DW->Autonomous_MODE) {
        /* Disable for Outport: '<S15>/SP' */
        flightController_B->DiscreteTimeIntegrator2_m[0] =
          flightController_P->SP_Y0;
        flightController_B->DiscreteTimeIntegrator2_m[1] =
          flightController_P->SP_Y0;
        flightController_DW->Autonomous_MODE = false;
      }
    }

    /* End of RelationalOperator: '<S17>/Compare' */
    /* End of Outputs for SubSystem: '<S12>/Autonomous' */

    /* Logic: '<S12>/Logical Operator1' */
    rtb_Compare_cy = !rtb_Compare_cy;

    /* Outputs for Enabled SubSystem: '<S12>/psi_autonomous' incorporates:
     *  EnablePort: '<S18>/Enable'
     */
    if (rtb_Compare_cy) {
      if (!flightController_DW->psi_autonomous_MODE) {
        /* InitializeConditions for DiscreteIntegrator: '<S18>/Discrete-Time Integrator1' */
        flightController_DW->DiscreteTimeIntegrator1_IC_LO_k = 1U;
        flightController_DW->DiscreteTimeIntegrator1_PrevRes = 2;
        flightController_DW->psi_autonomous_MODE = true;
      }

      /* DiscreteIntegrator: '<S18>/Discrete-Time Integrator1' */
      if (flightController_DW->DiscreteTimeIntegrator1_IC_LO_k != 0) {
        flightController_DW->DiscreteTimeIntegrator1_DSTAT_b = rtb_y_n_idx_0;
      }

      if (flightController_DW->DiscreteTimeIntegrator1_PrevRes <= 0) {
        flightController_DW->DiscreteTimeIntegrator1_DSTAT_b = rtb_y_n_idx_0;
      }

      flightController_B->DiscreteTimeIntegrator1 =
        flightController_DW->DiscreteTimeIntegrator1_DSTAT_b;

      /* End of DiscreteIntegrator: '<S18>/Discrete-Time Integrator1' */

      /* Update for DiscreteIntegrator: '<S18>/Discrete-Time Integrator1' incorporates:
       *  Gain: '<S18>/Gain3'
       *  Sum: '<S18>/Sum6'
       */
      flightController_DW->DiscreteTimeIntegrator1_IC_LO_k = 0U;
      flightController_DW->DiscreteTimeIntegrator1_DSTAT_b +=
        (flightController_B->Switch1[2] -
         flightController_B->DiscreteTimeIntegrator1) *
        flightController_P->Gain3_Gain *
        flightController_P->DiscreteTimeIntegrator1_gainval;
      flightController_DW->DiscreteTimeIntegrator1_PrevRes = 1;
    } else {
      if (flightController_DW->psi_autonomous_MODE) {
        /* Disable for Outport: '<S18>/SP' */
        flightController_B->DiscreteTimeIntegrator1 =
          flightController_P->SP_Y0_m;
        flightController_DW->psi_autonomous_MODE = false;
      }
    }

    /* End of Outputs for SubSystem: '<S12>/psi_autonomous' */

    /* MultiPortSwitch: '<S12>/Multiport Switch' incorporates:
     *  Inport: '<Root>/mode'
     */
    switch ((int32_T)flightController_U->mode) {
     case 0:
      /* Outport: '<Root>/Phi_SP_log' */
      flightController_Y->Phi_SP_log[0] = rtb_MultiportSwitch_idx_0;
      flightController_Y->Phi_SP_log[1] = rtb_MultiportSwitch_idx_1;
      flightController_Y->Phi_SP_log[2] = flightController_B->MultiportSwitch1;
      break;

     case 1:
      /* Outport: '<Root>/Phi_SP_log' */
      flightController_Y->Phi_SP_log[0] =
        flightController_B->DiscreteTimeIntegrator2_m[0];
      flightController_Y->Phi_SP_log[1] =
        flightController_B->DiscreteTimeIntegrator2_m[1];
      flightController_Y->Phi_SP_log[2] = flightController_B->MultiportSwitch1;
      break;

     case 2:
      /* Outport: '<Root>/Phi_SP_log' */
      flightController_Y->Phi_SP_log[0] =
        flightController_B->DiscreteTimeIntegrator2_m[0];
      flightController_Y->Phi_SP_log[1] =
        flightController_B->DiscreteTimeIntegrator2_m[1];
      flightController_Y->Phi_SP_log[2] =
        flightController_B->DiscreteTimeIntegrator1;
      break;

     default:
      /* Outport: '<Root>/Phi_SP_log' */
      flightController_Y->Phi_SP_log[0] =
        flightController_B->DiscreteTimeIntegrator2_m[0];
      flightController_Y->Phi_SP_log[1] =
        flightController_B->DiscreteTimeIntegrator2_m[1];
      flightController_Y->Phi_SP_log[2] =
        flightController_B->DiscreteTimeIntegrator1;
      break;
    }

    /* End of MultiPortSwitch: '<S12>/Multiport Switch' */

    /* Outputs for Enabled SubSystem: '<S4>/PositionController' incorporates:
     *  EnablePort: '<S11>/Enable'
     */
    if (rtb_LogicalOperator_l) {
      if (!flightController_DW->PositionController_MODE) {
        /* InitializeConditions for Delay: '<S11>/Delay' */
        flightController_DW->icLoad_lo = 1U;

        /* InitializeConditions for DiscreteIntegrator: '<S11>/Discrete-Time Integrator' */
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] =
          flightController_P->DiscreteTimeIntegrator_IC;
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] =
          flightController_P->DiscreteTimeIntegrator_IC;
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] =
          flightController_P->DiscreteTimeIntegrator_IC;
        flightController_DW->DiscreteTimeIntegrator_PrevRe_c = 2;
        flightController_DW->PositionController_MODE = true;
      }

      /* Gain: '<S11>/Gain2' incorporates:
       *  Sum: '<S11>/Sum4'
       */
      for (i = 0; i < 3; i++) {
        tmp[i] = flightController_P->Gain2_Gain[i + 6] *
          flightController_Y->Phi_SP_log[2] + (flightController_P->Gain2_Gain[i
          + 3] * flightController_Y->Phi_SP_log[1] +
          flightController_P->Gain2_Gain[i] * flightController_Y->Phi_SP_log[0]);
      }

      /* End of Gain: '<S11>/Gain2' */

      /* Gain: '<S11>/Gain1' incorporates:
       *  SignalConversion: '<S11>/TmpSignal ConversionAtGain1Inport1'
       *  Sum: '<S11>/Sum4'
       */
      for (i = 0; i < 3; i++) {
        tmp_2[i] = flightController_P->Gain1_Gain[i + 6] * rtb_y_n_idx_0 +
          (flightController_P->Gain1_Gain[i + 3] *
           flightController_B->sf_filterTheta.y[0] +
           flightController_P->Gain1_Gain[i] *
           flightController_B->sf_filterPhi.y[0]);
      }

      /* End of Gain: '<S11>/Gain1' */

      /* Sum: '<S11>/Sum4' */
      rtb_Switch_l[0] = tmp[0] - tmp_2[0];
      rtb_Switch_l[1] = tmp[1] - tmp_2[1];
      rtb_Switch_l[2] = tmp[2] - tmp_2[2];

      /* Delay: '<S11>/Delay' */
      if (flightController_DW->icLoad_lo != 0) {
        flightController_DW->Delay_DSTATE_p[0] = rtb_Switch_l[0];
        flightController_DW->Delay_DSTATE_p[1] = rtb_Switch_l[1];
        flightController_DW->Delay_DSTATE_p[2] = rtb_Switch_l[2];
      }

      /* DiscreteIntegrator: '<S11>/Discrete-Time Integrator' */
      if (flightController_DW->DiscreteTimeIntegrator_PrevRe_c <= 0) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] =
          flightController_P->DiscreteTimeIntegrator_IC;
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] =
          flightController_P->DiscreteTimeIntegrator_IC;
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] =
          flightController_P->DiscreteTimeIntegrator_IC;
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] >=
          flightController_P->DiscreteTimeIntegrator_UpperSat[0]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] =
          flightController_P->DiscreteTimeIntegrator_UpperSat[0];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] <=
            flightController_P->DiscreteTimeIntegrator_LowerSat[0]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] =
            flightController_P->DiscreteTimeIntegrator_LowerSat[0];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] >=
          flightController_P->DiscreteTimeIntegrator_UpperSat[1]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] =
          flightController_P->DiscreteTimeIntegrator_UpperSat[1];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] <=
            flightController_P->DiscreteTimeIntegrator_LowerSat[1]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] =
            flightController_P->DiscreteTimeIntegrator_LowerSat[1];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] >=
          flightController_P->DiscreteTimeIntegrator_UpperSat[2]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] =
          flightController_P->DiscreteTimeIntegrator_UpperSat[2];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] <=
            flightController_P->DiscreteTimeIntegrator_LowerSat[2]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] =
            flightController_P->DiscreteTimeIntegrator_LowerSat[2];
        }
      }

      /* Sum: '<S11>/Sum7' incorporates:
       *  Constant: '<S11>/Constant1'
       *  Delay: '<S11>/Delay'
       *  DiscreteIntegrator: '<S11>/Discrete-Time Integrator'
       *  Inport: '<Root>/PID_Phi'
       *  Product: '<S11>/Divide'
       *  Product: '<S11>/Product6'
       *  Product: '<S11>/Product7'
       *  Sum: '<S11>/Sum6'
       */
      flightController_B->Sum7[0] = 1.0 / flightController_P->dt *
        (rtb_Switch_l[0] - flightController_DW->Delay_DSTATE_p[0]) *
        flightController_U->PID_Phi[2] + (flightController_U->PID_Phi[0] *
        rtb_Switch_l[0] + flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0]
        * flightController_U->PID_Phi[1]);
      flightController_B->Sum7[1] = 1.0 / flightController_P->dt *
        (rtb_Switch_l[1] - flightController_DW->Delay_DSTATE_p[1]) *
        flightController_U->PID_Phi[5] + (flightController_U->PID_Phi[3] *
        rtb_Switch_l[1] + flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1]
        * flightController_U->PID_Phi[4]);
      flightController_B->Sum7[2] = 1.0 / flightController_P->dt *
        (rtb_Switch_l[2] - flightController_DW->Delay_DSTATE_p[2]) *
        flightController_U->PID_Phi[8] + (flightController_U->PID_Phi[6] *
        rtb_Switch_l[2] + flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2]
        * flightController_U->PID_Phi[7]);

      /* Update for Delay: '<S11>/Delay' */
      flightController_DW->icLoad_lo = 0U;
      flightController_DW->Delay_DSTATE_p[0] = rtb_Switch_l[0];
      flightController_DW->Delay_DSTATE_p[1] = rtb_Switch_l[1];
      flightController_DW->Delay_DSTATE_p[2] = rtb_Switch_l[2];

      /* Update for DiscreteIntegrator: '<S11>/Discrete-Time Integrator' */
      flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] +=
        flightController_P->DiscreteTimeIntegrator_gainval * rtb_Switch_l[0];
      flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] +=
        flightController_P->DiscreteTimeIntegrator_gainval * rtb_Switch_l[1];
      flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] +=
        flightController_P->DiscreteTimeIntegrator_gainval * rtb_Switch_l[2];
      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] >=
          flightController_P->DiscreteTimeIntegrator_UpperSat[0]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] =
          flightController_P->DiscreteTimeIntegrator_UpperSat[0];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] <=
            flightController_P->DiscreteTimeIntegrator_LowerSat[0]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] =
            flightController_P->DiscreteTimeIntegrator_LowerSat[0];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] >=
          flightController_P->DiscreteTimeIntegrator_UpperSat[1]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] =
          flightController_P->DiscreteTimeIntegrator_UpperSat[1];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] <=
            flightController_P->DiscreteTimeIntegrator_LowerSat[1]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] =
            flightController_P->DiscreteTimeIntegrator_LowerSat[1];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] >=
          flightController_P->DiscreteTimeIntegrator_UpperSat[2]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] =
          flightController_P->DiscreteTimeIntegrator_UpperSat[2];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] <=
            flightController_P->DiscreteTimeIntegrator_LowerSat[2]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] =
            flightController_P->DiscreteTimeIntegrator_LowerSat[2];
        }
      }

      flightController_DW->DiscreteTimeIntegrator_PrevRe_c = 1;

      /* End of Update for DiscreteIntegrator: '<S11>/Discrete-Time Integrator' */
    } else {
      if (flightController_DW->PositionController_MODE) {
        /* Disable for Outport: '<S11>/u' */
        flightController_B->Sum7[0] = flightController_P->u_Y0[0];
        flightController_B->Sum7[1] = flightController_P->u_Y0[1];
        flightController_B->Sum7[2] = flightController_P->u_Y0[2];
        flightController_DW->PositionController_MODE = false;
      }
    }

    /* End of Outputs for SubSystem: '<S4>/PositionController' */

    /* MultiPortSwitch: '<S4>/Multiport Switch' */
    if (!flightController_B->Compare) {
      rtb_Delay_idx_0 = flightController_B->Sum7[2];
    } else {
      rtb_Delay_idx_0 = rtb_MultiportSwitch_idx_2;
    }

    /* End of MultiPortSwitch: '<S4>/Multiport Switch' */

    /* Outputs for Enabled SubSystem: '<S4>/VelocityController' incorporates:
     *  EnablePort: '<S14>/Enable'
     */
    if (rtb_LogicalOperator_l) {
      if (!flightController_DW->VelocityController_MODE) {
        /* InitializeConditions for Delay: '<S14>/Delay' */
        flightController_DW->icLoad_l = 1U;

        /* InitializeConditions for DiscreteIntegrator: '<S14>/Discrete-Time Integrator' */
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] =
          flightController_P->DiscreteTimeIntegrator_IC_n;
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] =
          flightController_P->DiscreteTimeIntegrator_IC_n;
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] =
          flightController_P->DiscreteTimeIntegrator_IC_n;
        flightController_DW->DiscreteTimeIntegrator_PrevRese = 2;
        flightController_DW->VelocityController_MODE = true;
      }

      /* Gain: '<S14>/Gain2' incorporates:
       *  SignalConversion: '<S14>/TmpSignal ConversionAtGain2Inport1'
       *  Sum: '<S14>/Sum4'
       */
      for (i = 0; i < 3; i++) {
        tmp[i] = flightController_P->Gain2_Gain_o[i + 6] * rtb_y_n_idx_1 +
          (flightController_P->Gain2_Gain_o[i + 3] *
           flightController_B->sf_filterTheta.y[1] +
           flightController_P->Gain2_Gain_o[i] *
           flightController_B->sf_filterPhi.y[1]);
      }

      /* End of Gain: '<S14>/Gain2' */

      /* Sum: '<S14>/Sum4' */
      rtb_Switch_l[0] = flightController_B->Sum7[0] - tmp[0];
      rtb_Switch_l[1] = flightController_B->Sum7[1] - tmp[1];
      rtb_Switch_l[2] = rtb_Delay_idx_0 - tmp[2];

      /* Delay: '<S14>/Delay' */
      if (flightController_DW->icLoad_l != 0) {
        flightController_DW->Delay_DSTATE_c[0] = rtb_Switch_l[0];
        flightController_DW->Delay_DSTATE_c[1] = rtb_Switch_l[1];
        flightController_DW->Delay_DSTATE_c[2] = rtb_Switch_l[2];
      }

      /* DiscreteIntegrator: '<S14>/Discrete-Time Integrator' */
      if (flightController_DW->DiscreteTimeIntegrator_PrevRese <= 0) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] =
          flightController_P->DiscreteTimeIntegrator_IC_n;
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] =
          flightController_P->DiscreteTimeIntegrator_IC_n;
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] =
          flightController_P->DiscreteTimeIntegrator_IC_n;
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] >=
          flightController_P->DiscreteTimeIntegrator_UpperS_e[0]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] =
          flightController_P->DiscreteTimeIntegrator_UpperS_e[0];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] <=
            flightController_P->DiscreteTimeIntegrator_LowerS_l[0]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] =
            flightController_P->DiscreteTimeIntegrator_LowerS_l[0];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] >=
          flightController_P->DiscreteTimeIntegrator_UpperS_e[1]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] =
          flightController_P->DiscreteTimeIntegrator_UpperS_e[1];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] <=
            flightController_P->DiscreteTimeIntegrator_LowerS_l[1]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] =
            flightController_P->DiscreteTimeIntegrator_LowerS_l[1];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] >=
          flightController_P->DiscreteTimeIntegrator_UpperS_e[2]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] =
          flightController_P->DiscreteTimeIntegrator_UpperS_e[2];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] <=
            flightController_P->DiscreteTimeIntegrator_LowerS_l[2]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] =
            flightController_P->DiscreteTimeIntegrator_LowerS_l[2];
        }
      }

      /* Product: '<S14>/Divide' incorporates:
       *  Constant: '<S14>/Constant1'
       *  Delay: '<S14>/Delay'
       *  Inport: '<Root>/PID_DPhi'
       *  Sum: '<S14>/Sum1'
       */
      flightController_B->Divide[0] = 1.0 / flightController_P->dt *
        (rtb_Switch_l[0] - flightController_DW->Delay_DSTATE_c[0]) *
        flightController_U->PID_DPhi[2];
      flightController_B->Divide[1] = 1.0 / flightController_P->dt *
        (rtb_Switch_l[1] - flightController_DW->Delay_DSTATE_c[1]) *
        flightController_U->PID_DPhi[5];
      flightController_B->Divide[2] = 1.0 / flightController_P->dt *
        (rtb_Switch_l[2] - flightController_DW->Delay_DSTATE_c[2]) *
        flightController_U->PID_DPhi[8];

      /* Product: '<S14>/Product4' incorporates:
       *  Inport: '<Root>/PID_DPhi'
       */
      flightController_B->Product4[0] = flightController_U->PID_DPhi[0] *
        rtb_Switch_l[0];
      flightController_B->Product4[1] = flightController_U->PID_DPhi[3] *
        rtb_Switch_l[1];
      flightController_B->Product4[2] = flightController_U->PID_DPhi[6] *
        rtb_Switch_l[2];

      /* Product: '<S14>/Product5' incorporates:
       *  DiscreteIntegrator: '<S14>/Discrete-Time Integrator'
       *  Inport: '<Root>/PID_DPhi'
       */
      flightController_B->Product5_k[0] =
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] *
        flightController_U->PID_DPhi[1];
      flightController_B->Product5_k[1] =
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] *
        flightController_U->PID_DPhi[4];
      flightController_B->Product5_k[2] =
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] *
        flightController_U->PID_DPhi[7];

      /* Sum: '<S14>/Sum5' */
      flightController_B->Sum5_l[0] = (flightController_B->Product4[0] +
        flightController_B->Product5_k[0]) + flightController_B->Divide[0];
      flightController_B->Sum5_l[1] = (flightController_B->Product4[1] +
        flightController_B->Product5_k[1]) + flightController_B->Divide[1];
      flightController_B->Sum5_l[2] = (flightController_B->Product4[2] +
        flightController_B->Product5_k[2]) + flightController_B->Divide[2];

      /* Update for Delay: '<S14>/Delay' */
      flightController_DW->icLoad_l = 0U;
      flightController_DW->Delay_DSTATE_c[0] = rtb_Switch_l[0];
      flightController_DW->Delay_DSTATE_c[1] = rtb_Switch_l[1];
      flightController_DW->Delay_DSTATE_c[2] = rtb_Switch_l[2];

      /* Update for DiscreteIntegrator: '<S14>/Discrete-Time Integrator' */
      flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] +=
        flightController_P->DiscreteTimeIntegrator_gainva_l * rtb_Switch_l[0];
      flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] +=
        flightController_P->DiscreteTimeIntegrator_gainva_l * rtb_Switch_l[1];
      flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] +=
        flightController_P->DiscreteTimeIntegrator_gainva_l * rtb_Switch_l[2];
      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] >=
          flightController_P->DiscreteTimeIntegrator_UpperS_e[0]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] =
          flightController_P->DiscreteTimeIntegrator_UpperS_e[0];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] <=
            flightController_P->DiscreteTimeIntegrator_LowerS_l[0]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] =
            flightController_P->DiscreteTimeIntegrator_LowerS_l[0];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] >=
          flightController_P->DiscreteTimeIntegrator_UpperS_e[1]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] =
          flightController_P->DiscreteTimeIntegrator_UpperS_e[1];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] <=
            flightController_P->DiscreteTimeIntegrator_LowerS_l[1]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] =
            flightController_P->DiscreteTimeIntegrator_LowerS_l[1];
        }
      }

      if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] >=
          flightController_P->DiscreteTimeIntegrator_UpperS_e[2]) {
        flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] =
          flightController_P->DiscreteTimeIntegrator_UpperS_e[2];
      } else {
        if (flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] <=
            flightController_P->DiscreteTimeIntegrator_LowerS_l[2]) {
          flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] =
            flightController_P->DiscreteTimeIntegrator_LowerS_l[2];
        }
      }

      flightController_DW->DiscreteTimeIntegrator_PrevRese = 1;

      /* End of Update for DiscreteIntegrator: '<S14>/Discrete-Time Integrator' */
    } else {
      if (flightController_DW->VelocityController_MODE) {
        /* Disable for Outport: '<S14>/u' */
        flightController_B->Sum5_l[0] = flightController_P->u_Y0_d[0];
        flightController_B->Sum5_l[1] = flightController_P->u_Y0_d[1];
        flightController_B->Sum5_l[2] = flightController_P->u_Y0_d[2];
        flightController_DW->VelocityController_MODE = false;
      }
    }

    /* End of Outputs for SubSystem: '<S4>/VelocityController' */
    for (i = 0; i < 3; i++) {
      /* Outport: '<Root>/u_p' incorporates:
       *  Gain: '<S4>/Gain1'
       */
      flightController_Y->u_p[i] = 0.0;
      flightController_Y->u_p[i] += flightController_P->Gain1_Gain_g[i] *
        flightController_B->Product4[0];
      flightController_Y->u_p[i] += flightController_P->Gain1_Gain_g[i + 3] *
        flightController_B->Product4[1];
      flightController_Y->u_p[i] += flightController_P->Gain1_Gain_g[i + 6] *
        flightController_B->Product4[2];
    }

    for (i = 0; i < 3; i++) {
      /* Outport: '<Root>/DPhi_SP_log' incorporates:
       *  Gain: '<S4>/Gain2'
       *  SignalConversion: '<S4>/TmpSignal ConversionAtGain2Inport1'
       */
      flightController_Y->DPhi_SP_log[i] = 0.0;
      flightController_Y->DPhi_SP_log[i] += flightController_P->Gain2_Gain_i[i] *
        flightController_B->Sum7[0];
      flightController_Y->DPhi_SP_log[i] += flightController_P->Gain2_Gain_i[i +
        3] * flightController_B->Sum7[1];
      flightController_Y->DPhi_SP_log[i] += flightController_P->Gain2_Gain_i[i +
        6] * rtb_Delay_idx_0;
    }

    for (i = 0; i < 3; i++) {
      /* Outport: '<Root>/u_i' incorporates:
       *  Gain: '<S4>/Gain3'
       */
      flightController_Y->u_i[i] = 0.0;
      flightController_Y->u_i[i] += flightController_P->Gain3_Gain_g[i] *
        flightController_B->Product5_k[0];
      flightController_Y->u_i[i] += flightController_P->Gain3_Gain_g[i + 3] *
        flightController_B->Product5_k[1];
      flightController_Y->u_i[i] += flightController_P->Gain3_Gain_g[i + 6] *
        flightController_B->Product5_k[2];
    }

    for (i = 0; i < 3; i++) {
      /* Outport: '<Root>/u_d' incorporates:
       *  Gain: '<S4>/Gain4'
       */
      flightController_Y->u_d[i] = 0.0;
      flightController_Y->u_d[i] += flightController_P->Gain4_Gain[i] *
        flightController_B->Divide[0];
      flightController_Y->u_d[i] += flightController_P->Gain4_Gain[i + 3] *
        flightController_B->Divide[1];
      flightController_Y->u_d[i] += flightController_P->Gain4_Gain[i + 6] *
        flightController_B->Divide[2];
    }

    /* Switch: '<S13>/Switch2' incorporates:
     *  RelationalOperator: '<S13>/LowerRelop1'
     *  RelationalOperator: '<S13>/UpperRelop'
     *  Switch: '<S13>/Switch'
     */
    if (flightController_B->Sum5_l[0] > rtb_Divide2) {
      rtb_Switch_l[0] = rtb_Divide2;
    } else if (flightController_B->Sum5_l[0] < rtb_Polynomial2) {
      /* Switch: '<S13>/Switch' */
      rtb_Switch_l[0] = rtb_Polynomial2;
    } else {
      rtb_Switch_l[0] = flightController_B->Sum5_l[0];
    }

    if (flightController_B->Sum5_l[1] > rtb_Divide2) {
      rtb_Switch_l[1] = rtb_Divide2;
    } else if (flightController_B->Sum5_l[1] < rtb_Polynomial2) {
      /* Switch: '<S13>/Switch' */
      rtb_Switch_l[1] = rtb_Polynomial2;
    } else {
      rtb_Switch_l[1] = flightController_B->Sum5_l[1];
    }

    if (flightController_B->Sum5_l[2] > rtb_Divide2) {
      rtb_Switch_l[2] = rtb_Divide2;
    } else if (flightController_B->Sum5_l[2] < rtb_Polynomial2) {
      /* Switch: '<S13>/Switch' */
      rtb_Switch_l[2] = rtb_Polynomial2;
    } else {
      rtb_Switch_l[2] = flightController_B->Sum5_l[2];
    }

    /* End of Switch: '<S13>/Switch2' */

    /* Sum: '<S4>/Sum8' incorporates:
     *  Sum: '<S4>/Sum'
     */
    c_idx_1 = (rtb_Switch_l[0] + flightController_Y->throttle_log) +
      rtb_Switch_l[2];

    /* Saturate: '<S4>/Saturation' */
    if (c_idx_1 > flightController_P->upper_limit) {
      /* Outport: '<Root>/FL_prop' */
      flightController_Y->FL_prop = flightController_P->upper_limit;
    } else if (c_idx_1 < flightController_P->lower_limit) {
      /* Outport: '<Root>/FL_prop' */
      flightController_Y->FL_prop = flightController_P->lower_limit;
    } else {
      /* Outport: '<Root>/FL_prop' */
      flightController_Y->FL_prop = c_idx_1;
    }

    /* End of Saturate: '<S4>/Saturation' */

    /* Sum: '<S4>/Sum10' incorporates:
     *  Sum: '<S4>/Sum6'
     */
    c_idx_1 = (flightController_Y->throttle_log - rtb_Switch_l[1]) -
      rtb_Switch_l[2];

    /* Saturate: '<S4>/Saturation1' */
    if (c_idx_1 > flightController_P->upper_limit) {
      /* Outport: '<Root>/FR_prop' */
      flightController_Y->FR_prop = flightController_P->upper_limit;
    } else if (c_idx_1 < flightController_P->lower_limit) {
      /* Outport: '<Root>/FR_prop' */
      flightController_Y->FR_prop = flightController_P->lower_limit;
    } else {
      /* Outport: '<Root>/FR_prop' */
      flightController_Y->FR_prop = c_idx_1;
    }

    /* End of Saturate: '<S4>/Saturation1' */

    /* Sum: '<S4>/Sum9' incorporates:
     *  Sum: '<S4>/Sum5'
     */
    c_idx_1 = (rtb_Switch_l[1] + flightController_Y->throttle_log) -
      rtb_Switch_l[2];

    /* Saturate: '<S4>/Saturation2' */
    if (c_idx_1 > flightController_P->upper_limit) {
      /* Outport: '<Root>/RL_prop' */
      flightController_Y->RL_prop = flightController_P->upper_limit;
    } else if (c_idx_1 < flightController_P->lower_limit) {
      /* Outport: '<Root>/RL_prop' */
      flightController_Y->RL_prop = flightController_P->lower_limit;
    } else {
      /* Outport: '<Root>/RL_prop' */
      flightController_Y->RL_prop = c_idx_1;
    }

    /* End of Saturate: '<S4>/Saturation2' */

    /* Sum: '<S4>/Sum11' incorporates:
     *  Sum: '<S4>/Sum3'
     */
    c_idx_1 = (flightController_Y->throttle_log - rtb_Switch_l[0]) +
      rtb_Switch_l[2];

    /* Saturate: '<S4>/Saturation3' */
    if (c_idx_1 > flightController_P->upper_limit) {
      /* Outport: '<Root>/RR_prop' */
      flightController_Y->RR_prop = flightController_P->upper_limit;
    } else if (c_idx_1 < flightController_P->lower_limit) {
      /* Outport: '<Root>/RR_prop' */
      flightController_Y->RR_prop = flightController_P->lower_limit;
    } else {
      /* Outport: '<Root>/RR_prop' */
      flightController_Y->RR_prop = c_idx_1;
    }

    /* End of Saturate: '<S4>/Saturation3' */
  } else {
    if (flightController_DW->orController_MODE) {
      /* Disable for Enabled SubSystem: '<S12>/psi_manual' */
      if (flightController_DW->psi_manual_MODE) {
        /* Disable for Outport: '<S19>/E_Dpsi_SP' */
        flightController_B->Compare = flightController_P->E_Dpsi_SP_Y0;

        /* Disable for Outport: '<S19>/psi_SP' */
        flightController_B->MultiportSwitch1 = flightController_P->psi_SP_Y0;
        flightController_DW->psi_manual_MODE = false;
      }

      /* End of Disable for SubSystem: '<S12>/psi_manual' */

      /* Disable for Enabled SubSystem: '<S12>/Autonomous' */
      if (flightController_DW->Autonomous_MODE) {
        /* Disable for Outport: '<S15>/SP' */
        flightController_B->DiscreteTimeIntegrator2_m[0] =
          flightController_P->SP_Y0;
        flightController_B->DiscreteTimeIntegrator2_m[1] =
          flightController_P->SP_Y0;
        flightController_DW->Autonomous_MODE = false;
      }

      /* End of Disable for SubSystem: '<S12>/Autonomous' */

      /* Disable for Enabled SubSystem: '<S12>/psi_autonomous' */
      if (flightController_DW->psi_autonomous_MODE) {
        /* Disable for Outport: '<S18>/SP' */
        flightController_B->DiscreteTimeIntegrator1 =
          flightController_P->SP_Y0_m;
        flightController_DW->psi_autonomous_MODE = false;
      }

      /* End of Disable for SubSystem: '<S12>/psi_autonomous' */

      /* Disable for Enabled SubSystem: '<S4>/PositionController' */
      if (flightController_DW->PositionController_MODE) {
        /* Disable for Outport: '<S11>/u' */
        flightController_B->Sum7[0] = flightController_P->u_Y0[0];
        flightController_B->Sum7[1] = flightController_P->u_Y0[1];
        flightController_B->Sum7[2] = flightController_P->u_Y0[2];
        flightController_DW->PositionController_MODE = false;
      }

      /* End of Disable for SubSystem: '<S4>/PositionController' */

      /* Disable for Enabled SubSystem: '<S4>/VelocityController' */
      if (flightController_DW->VelocityController_MODE) {
        /* Disable for Outport: '<S14>/u' */
        flightController_B->Sum5_l[0] = flightController_P->u_Y0_d[0];
        flightController_B->Sum5_l[1] = flightController_P->u_Y0_d[1];
        flightController_B->Sum5_l[2] = flightController_P->u_Y0_d[2];
        flightController_DW->VelocityController_MODE = false;
      }

      /* End of Disable for SubSystem: '<S4>/VelocityController' */

      /* Disable for Outport: '<Root>/FL_prop' incorporates:
       *  Disable for Outport: '<S4>/FL_Prop'
       */
      flightController_Y->FL_prop = flightController_P->FL_Prop_Y0;

      /* Disable for Outport: '<Root>/FR_prop' incorporates:
       *  Disable for Outport: '<S4>/FR_Prop'
       */
      flightController_Y->FR_prop = flightController_P->FR_Prop_Y0;

      /* Disable for Outport: '<Root>/RL_prop' incorporates:
       *  Disable for Outport: '<S4>/RL_Prop'
       */
      flightController_Y->RL_prop = flightController_P->RL_Prop_Y0;

      /* Disable for Outport: '<Root>/RR_prop' incorporates:
       *  Disable for Outport: '<S4>/RR_Prop'
       */
      flightController_Y->RR_prop = flightController_P->RR_Prop_Y0;

      /* Disable for Outport: '<Root>/Phi_SP_log' incorporates:
       *  Disable for Outport: '<S4>/Phi_SP_log'
       */
      flightController_Y->Phi_SP_log[0] = flightController_P->Phi_SP_log_Y0;
      flightController_Y->Phi_SP_log[1] = flightController_P->Phi_SP_log_Y0;
      flightController_Y->Phi_SP_log[2] = flightController_P->Phi_SP_log_Y0;

      /* Disable for Outport: '<Root>/DPhi_SP_log' incorporates:
       *  Disable for Outport: '<S4>/DPhi_SP_log'
       */
      flightController_Y->DPhi_SP_log[0] = flightController_P->DPhi_SP_log_Y0[0];
      flightController_Y->DPhi_SP_log[1] = flightController_P->DPhi_SP_log_Y0[1];
      flightController_Y->DPhi_SP_log[2] = flightController_P->DPhi_SP_log_Y0[2];

      /* Disable for Outport: '<Root>/throttle_log' incorporates:
       *  Disable for Outport: '<S4>/throttle'
       */
      flightController_Y->throttle_log = flightController_P->throttle_Y0;
      flightController_DW->orController_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S1>/orController' */
  if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
    /* Outport: '<Root>/spReached' */
    flightController_Y->spReached =
      flightController_B->OutportBufferForspReached;

    /* Outport: '<Root>/x_hat_r_log' */
    flightController_Y->x_hat_r_log[0] = flightController_B->z_r[0];
    flightController_Y->x_hat_r_log[1] = flightController_B->z_r[1];
    flightController_Y->x_hat_r_log[2] = flightController_B->y;

    /* Outport: '<Root>/x_hat_v_log' */
    flightController_Y->x_hat_v_log[0] =
      flightController_B->OutportBufferForx_hat_v[0];
    flightController_Y->x_hat_v_log[1] =
      flightController_B->OutportBufferForx_hat_v[1];
    flightController_Y->x_hat_v_log[2] =
      flightController_B->OutportBufferForx_hat_v[2];

    /* Outport: '<Root>/x_hat_a_log' */
    flightController_Y->x_hat_a_log[0] =
      flightController_B->OutportBufferForx_hat_a[0];
    flightController_Y->x_hat_a_log[1] =
      flightController_B->OutportBufferForx_hat_a[1];
    flightController_Y->x_hat_a_log[2] =
      flightController_B->OutportBufferForx_hat_a[2];
  }

  /* Outport: '<Root>/z_Phi_log' */
  flightController_Y->z_Phi_log[0] = rtb_Switch_idx_0;
  flightController_Y->z_Phi_log[1] = rtb_Switch_idx_1;
  flightController_Y->z_Phi_log[2] = rtb_Switch_idx_2;

  /* Outport: '<Root>/z_DPhi_log' */
  flightController_Y->z_DPhi_log[0] = rtb_Switch1_idx_0;
  flightController_Y->z_DPhi_log[1] = rtb_Switch1_idx_1;
  flightController_Y->z_DPhi_log[2] = rtb_Switch1_idx_2;

  /* Outport: '<Root>/x_hat_DPhi_log' */
  flightController_Y->x_hat_DPhi_log[0] = flightController_B->sf_filterPhi.y[1];
  flightController_Y->x_hat_DPhi_log[1] = flightController_B->sf_filterTheta.y[1];
  flightController_Y->x_hat_DPhi_log[2] = rtb_y_n_idx_1;
  if (flightController_M->Timing.TaskCounters.TID[1] == 0) {
    /* Outport: '<Root>/r_SP_log' */
    flightController_Y->r_SP_log[0] = flightController_B->MultiportSwitch[0];
    flightController_Y->r_SP_log[1] = flightController_B->MultiportSwitch[1];
    flightController_Y->r_SP_log[2] = flightController_B->MultiportSwitch[2];

    /* Outport: '<Root>/landed' */
    flightController_Y->landed = flightController_B->OutportBufferForlanded;
  }

  /* Outport: '<Root>/x_hat_Phi_log' */
  flightController_Y->x_hat_Phi_log[0] = rtb_TmpSignalConversionAtDelayI[0];
  flightController_Y->x_hat_Phi_log[1] = rtb_TmpSignalConversionAtDelayI[1];
  flightController_Y->x_hat_Phi_log[2] = rtb_TmpSignalConversionAtDelayI[2];

  /* Update for Delay: '<S1>/Delay1' */
  flightController_DW->Delay1_DSTATE = flightController_Y->throttle_log;

  /* Update for Delay: '<S23>/Delay1' */
  flightController_DW->icLoad = 0U;
  flightController_DW->Delay1_DSTATE_m[0] = rtb_Switch2_idx_0;
  flightController_DW->Delay1_DSTATE_m[1] = rtb_Switch2_idx_1;
  flightController_DW->Delay1_DSTATE_m[2] = rtb_Switch2_idx_2;

  /* Update for Delay: '<S23>/Delay2' incorporates:
   *  Update for Inport: '<Root>/com'
   */
  flightController_DW->icLoad_d = 0U;
  flightController_DW->Delay2_DSTATE[0] = flightController_U->com[0];
  flightController_DW->Delay2_DSTATE[1] = flightController_U->com[1];
  flightController_DW->Delay2_DSTATE[2] = flightController_U->com[2];

  /* Update for Delay: '<S5>/Delay' */
  flightController_DW->Delay_DSTATE[0] = rtb_TmpSignalConversionAtDelayI[0];
  flightController_DW->Delay_DSTATE[1] = rtb_TmpSignalConversionAtDelayI[1];
  flightController_DW->Delay_DSTATE[2] = rtb_TmpSignalConversionAtDelayI[2];
  rate_scheduler(flightController_M);
}

/* Model initialize function */
void flightController_initialize(RT_MODEL_flightController_T *const
  flightController_M)
{
  P_flightController_T *flightController_P = ((P_flightController_T *)
    flightController_M->ModelData.defaultParam);
  B_flightController_T *flightController_B = ((B_flightController_T *)
    flightController_M->ModelData.blockIO);
  DW_flightController_T *flightController_DW = ((DW_flightController_T *)
    flightController_M->ModelData.dwork);
  ExtY_flightController_T *flightController_Y = (ExtY_flightController_T *)
    flightController_M->ModelData.outputs;

  /* InitializeConditions for Enabled SubSystem: '<S1>/poDataProcessing' */
  /* InitializeConditions for DiscreteIntegrator: '<S54>/Discrete-Time Integrator1' */
  flightController_DW->DiscreteTimeIntegrator1_IC_LOAD = 1U;

  /* InitializeConditions for DiscreteIntegrator: '<S54>/Discrete-Time Integrator' */
  flightController_DW->DiscreteTimeIntegrator_DSTATE =
    flightController_P->DiscreteTimeIntegrator_IC_d;

  /* InitializeConditions for MATLAB Function: '<S7>/filterX' */
  flightController_filterX_Init(&flightController_DW->sf_filterX);

  /* InitializeConditions for MATLAB Function: '<S7>/filterY' */
  /* '<S52>:1:7' x_prior_estimate = [0; 0]; */
  flightController_DW->x_prior_estimate[0] = 0.0;
  flightController_DW->x_prior_estimate[1] = 0.0;

  /* '<S52>:1:8' P_prior = [0 0; 0 0]; */
  flightController_DW->P_prior[0] = 0.0;
  flightController_DW->P_prior[1] = 0.0;
  flightController_DW->P_prior[2] = 0.0;
  flightController_DW->P_prior[3] = 0.0;

  /* InitializeConditions for MATLAB Function: '<S7>/filterZ' */
  flightController_filterX_Init(&flightController_DW->sf_filterZ);

  /* End of InitializeConditions for SubSystem: '<S1>/poDataProcessing' */

  /* Start for Enabled SubSystem: '<S1>/poDataProcessing' */
  /* VirtualOutportStart for Outport: '<S7>/x_hat_r' */
  flightController_B->z_r[0] = flightController_P->x_hat_r_Y0;
  flightController_B->z_r[1] = flightController_P->x_hat_r_Y0;
  flightController_B->y = flightController_P->x_hat_r_Y0;

  /* VirtualOutportStart for Outport: '<S7>/x_hat_v' */
  flightController_B->OutportBufferForx_hat_v[0] =
    flightController_P->x_hat_v_Y0;
  flightController_B->OutportBufferForx_hat_v[1] =
    flightController_P->x_hat_v_Y0;
  flightController_B->OutportBufferForx_hat_v[2] =
    flightController_P->x_hat_v_Y0;

  /* VirtualOutportStart for Outport: '<S7>/x_hat_a' */
  flightController_B->OutportBufferForx_hat_a[0] =
    flightController_P->x_hat_a_Y0;
  flightController_B->OutportBufferForx_hat_a[1] =
    flightController_P->x_hat_a_Y0;
  flightController_B->OutportBufferForx_hat_a[2] =
    flightController_P->x_hat_a_Y0;

  /* End of Start for SubSystem: '<S1>/poDataProcessing' */

  /* Start for Enabled SubSystem: '<S1>/poController' */
  /* InitializeConditions for Enabled SubSystem: '<S34>/velocitySPmanager' */
  /* InitializeConditions for Delay: '<S41>/Delay' */
  flightController_DW->icLoad_e = 1U;

  /* End of InitializeConditions for SubSystem: '<S34>/velocitySPmanager' */

  /* Start for Enabled SubSystem: '<S34>/velocitySPmanager' */
  /* VirtualOutportStart for Outport: '<S41>/r_SP' */
  flightController_B->Switch_k[0] = flightController_P->r_SP_Y0[0];
  flightController_B->Switch_k[1] = flightController_P->r_SP_Y0[1];
  flightController_B->Switch_k[2] = flightController_P->r_SP_Y0[2];

  /* VirtualOutportStart for Outport: '<S41>/E_v_SP' */
  flightController_B->OutportBufferForE_v_SP[0] = flightController_P->E_v_SP_Y0
    [0];
  flightController_B->OutportBufferForE_v_SP[1] = flightController_P->E_v_SP_Y0
    [1];
  flightController_B->OutportBufferForE_v_SP[2] = flightController_P->E_v_SP_Y0
    [2];

  /* VirtualOutportStart for Outport: '<S41>/v_SP' */
  flightController_B->SP[0] = flightController_P->v_SP_Y0[0];
  flightController_B->SP[1] = flightController_P->v_SP_Y0[1];
  flightController_B->SP[2] = flightController_P->v_SP_Y0[2];

  /* End of Start for SubSystem: '<S34>/velocitySPmanager' */

  /* Start for Enabled SubSystem: '<S40>/psiCalc' */
  /* VirtualOutportStart for Outport: '<S45>/psi_SP' */
  flightController_B->Sum1 = flightController_P->psi_SP_Y0_c;

  /* End of Start for SubSystem: '<S40>/psiCalc' */
  /* End of Start for SubSystem: '<S1>/poController' */

  /* InitializeConditions for Enabled SubSystem: '<S1>/poController' */
  /* InitializeConditions for DiscreteIntegrator: '<S38>/Discrete-Time Integrator2' */
  flightController_DW->DiscreteTimeIntegrator2_DSTATE =
    flightController_P->DiscreteTimeIntegrator2_IC_f;

  /* InitializeConditions for MATLAB Function: '<S34>/mode2sequence' */
  /* '<S39>:1:4' sequence = -1; */
  flightController_DW->sequence = -1.0;

  /* '<S39>:1:5' r_SP = [0; 0; 0]; */
  flightController_DW->r_SP[0] = 0.0;
  flightController_DW->r_SP[1] = 0.0;
  flightController_DW->r_SP[2] = 0.0;

  /* '<S39>:1:6' lastGPSsp = [0; 0; 0]; */
  flightController_DW->lastGPSsp[0] = 0.0;
  flightController_DW->lastGPSsp[1] = 0.0;
  flightController_DW->lastGPSsp[2] = 0.0;

  /* InitializeConditions for DiscreteIntegrator: '<S33>/Discrete-Time Integrator2' */
  flightController_DW->DiscreteTimeIntegrator2_IC_LOAD = 1U;

  /* InitializeConditions for DiscreteIntegrator: '<S35>/Discrete-Time Integrator1' */
  flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[0] =
    flightController_P->DiscreteTimeIntegrator1_IC;
  flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[1] =
    flightController_P->DiscreteTimeIntegrator1_IC;
  flightController_DW->DiscreteTimeIntegrator1_DSTAT_l[2] =
    flightController_P->DiscreteTimeIntegrator1_IC;

  /* InitializeConditions for Delay: '<S35>/Delay1' */
  flightController_DW->icLoad_do = 1U;

  /* InitializeConditions for DiscreteIntegrator: '<S36>/Discrete-Time Integrator' */
  flightController_DW->DiscreteTimeIntegrator_DSTATE_l[0] =
    flightController_P->DiscreteTimeIntegrator_IC_nb[0];
  flightController_DW->DiscreteTimeIntegrator_DSTATE_l[1] =
    flightController_P->DiscreteTimeIntegrator_IC_nb[1];
  flightController_DW->DiscreteTimeIntegrator_DSTATE_l[2] =
    flightController_P->DiscreteTimeIntegrator_IC_nb[2];

  /* InitializeConditions for Delay: '<S36>/Delay' */
  flightController_DW->icLoad_k = 1U;

  /* InitializeConditions for Delay: '<S40>/Delay3' */
  flightController_DW->icLoad_o = 1U;

  /* InitializeConditions for Delay: '<S40>/Delay' */
  flightController_DW->icLoad_a = 1U;

  /* End of InitializeConditions for SubSystem: '<S1>/poController' */

  /* Start for Enabled SubSystem: '<S1>/poController' */
  /* VirtualOutportStart for Outport: '<S6>/Phi_SP' */
  flightController_B->Switch1[0] = flightController_P->Phi_SP_Y0[0];
  flightController_B->Switch1[1] = flightController_P->Phi_SP_Y0[1];
  flightController_B->Switch1[2] = flightController_P->Phi_SP_Y0[2];

  /* VirtualOutportStart for Outport: '<S6>/throttle' */
  flightController_B->MatrixMultiply1[2] = flightController_P->throttle_Y0_i;

  /* VirtualOutportStart for Outport: '<S6>/r_log_SP' */
  flightController_B->MultiportSwitch[0] = flightController_P->r_log_SP_Y0;
  flightController_B->MultiportSwitch[1] = flightController_P->r_log_SP_Y0;
  flightController_B->MultiportSwitch[2] = flightController_P->r_log_SP_Y0;

  /* VirtualOutportStart for Outport: '<Root>/v_SP_log' incorporates:
   *  VirtualOutportStart for Outport: '<S6>/v_log_SP'
   */
  flightController_Y->v_SP_log[0] = flightController_P->v_log_SP_Y0[0];
  flightController_Y->v_SP_log[1] = flightController_P->v_log_SP_Y0[1];
  flightController_Y->v_SP_log[2] = flightController_P->v_log_SP_Y0[2];

  /* VirtualOutportStart for Outport: '<S6>/spReached' */
  flightController_B->OutportBufferForspReached =
    flightController_P->spReached_Y0;

  /* VirtualOutportStart for Outport: '<S6>/landed' */
  flightController_B->OutportBufferForlanded = flightController_P->landed_Y0;

  /* End of Start for SubSystem: '<S1>/poController' */

  /* Start for Enabled SubSystem: '<S1>/orController' */
  /* InitializeConditions for Enabled SubSystem: '<S12>/psi_manual' */
  /* InitializeConditions for Delay: '<S19>/Delay1' */
  flightController_DW->icLoad_f = 1U;

  /* End of InitializeConditions for SubSystem: '<S12>/psi_manual' */

  /* Start for Enabled SubSystem: '<S12>/psi_manual' */
  /* VirtualOutportStart for Outport: '<S19>/E_Dpsi_SP' */
  flightController_B->Compare = flightController_P->E_Dpsi_SP_Y0;

  /* VirtualOutportStart for Outport: '<S19>/psi_SP' */
  flightController_B->MultiportSwitch1 = flightController_P->psi_SP_Y0;

  /* End of Start for SubSystem: '<S12>/psi_manual' */

  /* InitializeConditions for Enabled SubSystem: '<S12>/Autonomous' */
  /* InitializeConditions for DiscreteIntegrator: '<S15>/Discrete-Time Integrator2' */
  flightController_DW->DiscreteTimeIntegrator2_DSTAT_p[0] =
    flightController_P->DiscreteTimeIntegrator2_IC[0];
  flightController_DW->DiscreteTimeIntegrator2_DSTAT_p[1] =
    flightController_P->DiscreteTimeIntegrator2_IC[1];

  /* End of InitializeConditions for SubSystem: '<S12>/Autonomous' */

  /* Start for Enabled SubSystem: '<S12>/Autonomous' */
  /* VirtualOutportStart for Outport: '<S15>/SP' */
  flightController_B->DiscreteTimeIntegrator2_m[0] = flightController_P->SP_Y0;
  flightController_B->DiscreteTimeIntegrator2_m[1] = flightController_P->SP_Y0;

  /* End of Start for SubSystem: '<S12>/Autonomous' */

  /* InitializeConditions for Enabled SubSystem: '<S12>/psi_autonomous' */
  /* InitializeConditions for DiscreteIntegrator: '<S18>/Discrete-Time Integrator1' */
  flightController_DW->DiscreteTimeIntegrator1_IC_LO_k = 1U;
  flightController_DW->DiscreteTimeIntegrator1_PrevRes = 2;

  /* End of InitializeConditions for SubSystem: '<S12>/psi_autonomous' */

  /* Start for Enabled SubSystem: '<S12>/psi_autonomous' */
  /* VirtualOutportStart for Outport: '<S18>/SP' */
  flightController_B->DiscreteTimeIntegrator1 = flightController_P->SP_Y0_m;

  /* End of Start for SubSystem: '<S12>/psi_autonomous' */

  /* InitializeConditions for Enabled SubSystem: '<S4>/PositionController' */
  /* InitializeConditions for Delay: '<S11>/Delay' */
  flightController_DW->icLoad_lo = 1U;

  /* InitializeConditions for DiscreteIntegrator: '<S11>/Discrete-Time Integrator' */
  flightController_DW->DiscreteTimeIntegrator_DSTATE_h[0] =
    flightController_P->DiscreteTimeIntegrator_IC;
  flightController_DW->DiscreteTimeIntegrator_DSTATE_h[1] =
    flightController_P->DiscreteTimeIntegrator_IC;
  flightController_DW->DiscreteTimeIntegrator_DSTATE_h[2] =
    flightController_P->DiscreteTimeIntegrator_IC;
  flightController_DW->DiscreteTimeIntegrator_PrevRe_c = 2;

  /* End of InitializeConditions for SubSystem: '<S4>/PositionController' */

  /* Start for Enabled SubSystem: '<S4>/PositionController' */
  /* VirtualOutportStart for Outport: '<S11>/u' */
  flightController_B->Sum7[0] = flightController_P->u_Y0[0];
  flightController_B->Sum7[1] = flightController_P->u_Y0[1];
  flightController_B->Sum7[2] = flightController_P->u_Y0[2];

  /* End of Start for SubSystem: '<S4>/PositionController' */

  /* InitializeConditions for Enabled SubSystem: '<S4>/VelocityController' */
  /* InitializeConditions for Delay: '<S14>/Delay' */
  flightController_DW->icLoad_l = 1U;

  /* InitializeConditions for DiscreteIntegrator: '<S14>/Discrete-Time Integrator' */
  flightController_DW->DiscreteTimeIntegrator_DSTATE_i[0] =
    flightController_P->DiscreteTimeIntegrator_IC_n;
  flightController_DW->DiscreteTimeIntegrator_DSTATE_i[1] =
    flightController_P->DiscreteTimeIntegrator_IC_n;
  flightController_DW->DiscreteTimeIntegrator_DSTATE_i[2] =
    flightController_P->DiscreteTimeIntegrator_IC_n;
  flightController_DW->DiscreteTimeIntegrator_PrevRese = 2;

  /* End of InitializeConditions for SubSystem: '<S4>/VelocityController' */

  /* Start for Enabled SubSystem: '<S4>/VelocityController' */
  /* VirtualOutportStart for Outport: '<S14>/u_p' */
  flightController_B->Product4[0] = flightController_P->u_p_Y0;
  flightController_B->Product4[1] = flightController_P->u_p_Y0;
  flightController_B->Product4[2] = flightController_P->u_p_Y0;

  /* VirtualOutportStart for Outport: '<S14>/u_i' */
  flightController_B->Product5_k[0] = flightController_P->u_i_Y0;
  flightController_B->Product5_k[1] = flightController_P->u_i_Y0;
  flightController_B->Product5_k[2] = flightController_P->u_i_Y0;

  /* VirtualOutportStart for Outport: '<S14>/u_d' */
  flightController_B->Divide[0] = flightController_P->u_d_Y0;
  flightController_B->Divide[1] = flightController_P->u_d_Y0;
  flightController_B->Divide[2] = flightController_P->u_d_Y0;

  /* VirtualOutportStart for Outport: '<S14>/u' */
  flightController_B->Sum5_l[0] = flightController_P->u_Y0_d[0];
  flightController_B->Sum5_l[1] = flightController_P->u_Y0_d[1];
  flightController_B->Sum5_l[2] = flightController_P->u_Y0_d[2];

  /* End of Start for SubSystem: '<S4>/VelocityController' */

  /* VirtualOutportStart for Outport: '<Root>/FL_prop' incorporates:
   *  VirtualOutportStart for Outport: '<S4>/FL_Prop'
   */
  flightController_Y->FL_prop = flightController_P->FL_Prop_Y0;

  /* VirtualOutportStart for Outport: '<Root>/FR_prop' incorporates:
   *  VirtualOutportStart for Outport: '<S4>/FR_Prop'
   */
  flightController_Y->FR_prop = flightController_P->FR_Prop_Y0;

  /* VirtualOutportStart for Outport: '<Root>/RL_prop' incorporates:
   *  VirtualOutportStart for Outport: '<S4>/RL_Prop'
   */
  flightController_Y->RL_prop = flightController_P->RL_Prop_Y0;

  /* VirtualOutportStart for Outport: '<Root>/RR_prop' incorporates:
   *  VirtualOutportStart for Outport: '<S4>/RR_Prop'
   */
  flightController_Y->RR_prop = flightController_P->RR_Prop_Y0;

  /* VirtualOutportStart for Outport: '<Root>/Phi_SP_log' incorporates:
   *  VirtualOutportStart for Outport: '<S4>/Phi_SP_log'
   */
  flightController_Y->Phi_SP_log[0] = flightController_P->Phi_SP_log_Y0;
  flightController_Y->Phi_SP_log[1] = flightController_P->Phi_SP_log_Y0;
  flightController_Y->Phi_SP_log[2] = flightController_P->Phi_SP_log_Y0;

  /* VirtualOutportStart for Outport: '<Root>/DPhi_SP_log' incorporates:
   *  VirtualOutportStart for Outport: '<S4>/DPhi_SP_log'
   */
  flightController_Y->DPhi_SP_log[0] = flightController_P->DPhi_SP_log_Y0[0];
  flightController_Y->DPhi_SP_log[1] = flightController_P->DPhi_SP_log_Y0[1];
  flightController_Y->DPhi_SP_log[2] = flightController_P->DPhi_SP_log_Y0[2];

  /* VirtualOutportStart for Outport: '<Root>/throttle_log' incorporates:
   *  VirtualOutportStart for Outport: '<S4>/throttle'
   */
  flightController_Y->throttle_log = flightController_P->throttle_Y0;

  /* VirtualOutportStart for Outport: '<Root>/u_p' incorporates:
   *  VirtualOutportStart for Outport: '<S4>/u_p'
   */
  flightController_Y->u_p[0] = flightController_P->u_p_Y0_o;
  flightController_Y->u_p[1] = flightController_P->u_p_Y0_o;
  flightController_Y->u_p[2] = flightController_P->u_p_Y0_o;

  /* VirtualOutportStart for Outport: '<Root>/u_i' incorporates:
   *  VirtualOutportStart for Outport: '<S4>/u_i'
   */
  flightController_Y->u_i[0] = flightController_P->u_i_Y0_g;
  flightController_Y->u_i[1] = flightController_P->u_i_Y0_g;
  flightController_Y->u_i[2] = flightController_P->u_i_Y0_g;

  /* VirtualOutportStart for Outport: '<Root>/u_d' incorporates:
   *  VirtualOutportStart for Outport: '<S4>/u_d'
   */
  flightController_Y->u_d[0] = flightController_P->u_d_Y0_m;
  flightController_Y->u_d[1] = flightController_P->u_d_Y0_m;
  flightController_Y->u_d[2] = flightController_P->u_d_Y0_m;

  /* End of Start for SubSystem: '<S1>/orController' */

  /* Start for Outport: '<Root>/spReached' */
  flightController_Y->spReached = flightController_B->OutportBufferForspReached;

  /* Start for Outport: '<Root>/x_hat_r_log' */
  flightController_Y->x_hat_r_log[0] = flightController_B->z_r[0];
  flightController_Y->x_hat_r_log[1] = flightController_B->z_r[1];
  flightController_Y->x_hat_r_log[2] = flightController_B->y;

  /* Start for Outport: '<Root>/x_hat_v_log' */
  flightController_Y->x_hat_v_log[0] =
    flightController_B->OutportBufferForx_hat_v[0];
  flightController_Y->x_hat_v_log[1] =
    flightController_B->OutportBufferForx_hat_v[1];
  flightController_Y->x_hat_v_log[2] =
    flightController_B->OutportBufferForx_hat_v[2];

  /* Start for Outport: '<Root>/x_hat_a_log' */
  flightController_Y->x_hat_a_log[0] =
    flightController_B->OutportBufferForx_hat_a[0];
  flightController_Y->x_hat_a_log[1] =
    flightController_B->OutportBufferForx_hat_a[1];
  flightController_Y->x_hat_a_log[2] =
    flightController_B->OutportBufferForx_hat_a[2];

  /* Start for Outport: '<Root>/x_hat_DPhi_log' */
  flightController_Y->x_hat_DPhi_log[0] = flightController_B->sf_filterPhi.y[1];
  flightController_Y->x_hat_DPhi_log[1] = flightController_B->sf_filterTheta.y[1];

  /* Start for Outport: '<Root>/r_SP_log' */
  flightController_Y->r_SP_log[0] = flightController_B->MultiportSwitch[0];
  flightController_Y->r_SP_log[1] = flightController_B->MultiportSwitch[1];
  flightController_Y->r_SP_log[2] = flightController_B->MultiportSwitch[2];

  /* Start for Outport: '<Root>/landed' */
  flightController_Y->landed = flightController_B->OutportBufferForlanded;

  /* InitializeConditions for Delay: '<S1>/Delay1' */
  flightController_DW->Delay1_DSTATE =
    flightController_P->Delay1_InitialCondition;

  /* InitializeConditions for Delay: '<S23>/Delay1' */
  flightController_DW->icLoad = 1U;

  /* InitializeConditions for Delay: '<S23>/Delay2' */
  flightController_DW->icLoad_d = 1U;

  /* InitializeConditions for Delay: '<S5>/Delay' */
  flightController_DW->Delay_DSTATE[0] =
    flightController_P->Delay_InitialCondition[0];
  flightController_DW->Delay_DSTATE[1] =
    flightController_P->Delay_InitialCondition[1];
  flightController_DW->Delay_DSTATE[2] =
    flightController_P->Delay_InitialCondition[2];

  /* InitializeConditions for MATLAB Function: '<S21>/CompassTiltCompensation' */
  flightController_DW->prevPsi_not_empty = false;
  flightController_DW->rounds = 0.0;

  /* InitializeConditions for MATLAB Function: '<S22>/filterPhi' */
  flightController_filterPhi_Init(&flightController_DW->sf_filterPhi);

  /* InitializeConditions for MATLAB Function: '<S22>/filterTheta' */
  flightController_filterPhi_Init(&flightController_DW->sf_filterTheta);

  /* InitializeConditions for MATLAB Function: '<S22>/filterPsi' */
  flightController_DW->P_prior_not_empty = false;
  flightController_DW->P_prior_f[0] = 0.0;
  flightController_DW->P_prior_f[1] = 0.0;
  flightController_DW->P_prior_f[2] = 0.0;
  flightController_DW->P_prior_f[3] = 0.0;
}

/* Model terminate function */
void flightController_terminate(RT_MODEL_flightController_T * flightController_M)
{
  /* model code */
  rt_FREE(flightController_M->ModelData.blockIO);
  rt_FREE(flightController_M->ModelData.inputs);
  rt_FREE(flightController_M->ModelData.outputs);
  if (flightController_M->ModelData.paramIsMalloced) {
    rt_FREE(flightController_M->ModelData.defaultParam);
  }

  rt_FREE(flightController_M->ModelData.dwork);
  rt_FREE(flightController_M->ModelData.prevZCSigState);
  rt_FREE(flightController_M);
}

/* Model data allocation function */
RT_MODEL_flightController_T *flightController(void)
{
  RT_MODEL_flightController_T *flightController_M;
  flightController_M = (RT_MODEL_flightController_T *) malloc(sizeof
    (RT_MODEL_flightController_T));
  if (flightController_M == NULL) {
    return NULL;
  }

  (void) memset((char *)flightController_M, 0,
                sizeof(RT_MODEL_flightController_T));

  /* block I/O */
  {
    B_flightController_T *b = (B_flightController_T *) malloc(sizeof
      (B_flightController_T));
    rt_VALIDATE_MEMORY(flightController_M,b);
    flightController_M->ModelData.blockIO = (b);
  }

  /* parameters */
  {
    P_flightController_T *p;
    static int_T pSeen = 0;

    /* only malloc on multiple model instantiation */
    if (pSeen == 1 ) {
      p = (P_flightController_T *) malloc(sizeof(P_flightController_T));
      rt_VALIDATE_MEMORY(flightController_M,p);
      (void) memcpy(p, &flightController_P,
                    sizeof(P_flightController_T));
      flightController_M->ModelData.paramIsMalloced = (true);
    } else {
      p = &flightController_P;
      flightController_M->ModelData.paramIsMalloced = (false);
      pSeen = 1;
    }

    flightController_M->ModelData.defaultParam = (p);
  }

  /* states (dwork) */
  {
    DW_flightController_T *dwork = (DW_flightController_T *) malloc(sizeof
      (DW_flightController_T));
    rt_VALIDATE_MEMORY(flightController_M,dwork);
    flightController_M->ModelData.dwork = (dwork);
  }

  /* external inputs */
  {
    ExtU_flightController_T *flightController_U = (ExtU_flightController_T *)
      malloc(sizeof(ExtU_flightController_T));
    rt_VALIDATE_MEMORY(flightController_M,flightController_U);
    flightController_M->ModelData.inputs = (((ExtU_flightController_T *)
      flightController_U));
  }

  /* external outputs */
  {
    ExtY_flightController_T *flightController_Y = (ExtY_flightController_T *)
      malloc(sizeof(ExtY_flightController_T));
    rt_VALIDATE_MEMORY(flightController_M,flightController_Y);
    flightController_M->ModelData.outputs = (flightController_Y);
  }

  /* previous zero-crossing states */
  {
    PrevZCX_flightController_T *zc = (PrevZCX_flightController_T *) malloc
      (sizeof(PrevZCX_flightController_T));
    rt_VALIDATE_MEMORY(flightController_M,zc);
    flightController_M->ModelData.prevZCSigState = (zc);
  }

  {
    P_flightController_T *flightController_P = ((P_flightController_T *)
      flightController_M->ModelData.defaultParam);
    B_flightController_T *flightController_B = ((B_flightController_T *)
      flightController_M->ModelData.blockIO);
    DW_flightController_T *flightController_DW = ((DW_flightController_T *)
      flightController_M->ModelData.dwork);
    PrevZCX_flightController_T *flightController_PrevZCX =
      ((PrevZCX_flightController_T *)
       flightController_M->ModelData.prevZCSigState);
    ExtU_flightController_T *flightController_U = (ExtU_flightController_T *)
      flightController_M->ModelData.inputs;
    ExtY_flightController_T *flightController_Y = (ExtY_flightController_T *)
      flightController_M->ModelData.outputs;

    /* initialize non-finites */
    rt_InitInfAndNaN(sizeof(real_T));

    /* block I/O */
    (void) memset(((void *) flightController_B), 0,
                  sizeof(B_flightController_T));

    /* states (dwork) */
    (void) memset((void *)flightController_DW, 0,
                  sizeof(DW_flightController_T));

    /* external inputs */
    (void) memset((void *)flightController_U, 0,
                  sizeof(ExtU_flightController_T));

    /* external outputs */
    (void) memset((void *)flightController_Y, 0,
                  sizeof(ExtY_flightController_T));

    /* previous zero-crossing states */
    {
      flightController_PrevZCX->Delay1_Reset_ZCE = POS_ZCSIG;
      flightController_PrevZCX->Delay_Reset_ZCE = POS_ZCSIG;
    }
  }

  return flightController_M;
}
