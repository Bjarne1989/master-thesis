/*
 * flightController_data.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "flightController".
 *
 * Model version              : 1.1032
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Wed Jun 01 12:19:36 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#include "flightController.h"
#include "flightController_private.h"

/* Block parameters (auto storage) */
P_flightController_T flightController_P = {
  0.012556800000000002,                /* Variable: F_c
                                        * Referenced by: '<S7>/Gain1'
                                        */

  /*  Variable: Q_phi
   * Referenced by: '<S22>/Constant2'
   */
  { 0.2260184189215742, 0.0, 0.0, 0.00022077523713473219 },

  /*  Variable: Q_psi
   * Referenced by: '<S22>/Constant8'
   */
  { 1.4039184753565741, 0.0, 0.0, 6.1123211288757816E-5 },

  /*  Variable: Q_theta
   * Referenced by: '<S22>/Constant5'
   */
  { 0.13686720020944529, 0.0, 0.0, 0.00021953935841473063 },

  /*  Variable: Q_x
   * Referenced by: '<S7>/Constant'
   */
  { 0.0010422061824665655, 0.0, 0.0, 0.00031134743040950395 },

  /*  Variable: Q_y
   * Referenced by: '<S7>/Constant2'
   */
  { 0.0013819951777679265, 0.0, 0.0, 0.00034883929015230289 },

  /*  Variable: Q_z
   * Referenced by: '<S7>/Constant4'
   */
  { 0.11697829366000406, 0.0, 0.0, 6.8444936420537138E-7 },

  /*  Variable: R_phi
   * Referenced by: '<S22>/Constant1'
   */
  { 1.2232051284841309E-5, 0.0, 0.0, 4.3340831483009115E-5 },

  /*  Variable: R_psi
   * Referenced by: '<S22>/Constant7'
   */
  { 6.3601846840934619E-5, 0.0, 0.0, 1.202440158260461E-5 },

  /*  Variable: R_theta
   * Referenced by: '<S22>/Constant4'
   */
  { 7.6038734874161492E-6, 0.0, 0.0, 1.6449061578290503E-5 },

  /*  Variable: R_x
   * Referenced by: '<S7>/Constant1'
   */
  { 0.0027142424772026719, 0.0, 0.0, 0.00015637937707297018 },

  /*  Variable: R_y
   * Referenced by: '<S7>/Constant3'
   */
  { 0.0051129236318402374, 0.0, 0.0, 0.00015087694705133623 },

  /*  Variable: R_z
   * Referenced by: '<S7>/Constant5'
   */
  { 0.0068738123433304708, 0.0, 0.0, 9.4381734118139356E-5 },
  0.015,                               /* Variable: dt
                                        * Referenced by:
                                        *   '<S11>/Constant1'
                                        *   '<S14>/Constant1'
                                        *   '<S22>/Constant3'
                                        *   '<S22>/Constant6'
                                        *   '<S22>/Constant9'
                                        */
  0.09,                                /* Variable: dt_p
                                        * Referenced by:
                                        *   '<S7>/Constant6'
                                        *   '<S7>/Constant7'
                                        *   '<S7>/Constant9'
                                        *   '<S35>/Constant2'
                                        *   '<S36>/Constant1'
                                        */
  120.0,                               /* Variable: lower_limit
                                        * Referenced by:
                                        *   '<S4>/Saturation'
                                        *   '<S4>/Saturation1'
                                        *   '<S4>/Saturation2'
                                        *   '<S4>/Saturation3'
                                        */
  1.066,                               /* Variable: m
                                        * Referenced by: '<S7>/Gain1'
                                        */
  600.0,                               /* Variable: upper_limit
                                        * Referenced by:
                                        *   '<S4>/Saturation'
                                        *   '<S4>/Saturation1'
                                        *   '<S4>/Saturation2'
                                        *   '<S4>/Saturation3'
                                        */

  /*  Variable: xPoly
   * Referenced by: '<S21>/Polynomial1'
   */
  { 0.0027952472206191443, -0.50465455631799216, 51.128896895682409 },

  /*  Variable: yPoly
   * Referenced by: '<S21>/Polynomial'
   */
  { 0.0001076119339284555, -0.055668129644440914, 9.0005996067436023 },

  /*  Variable: zPoly
   * Referenced by: '<S21>/Polynomial2'
   */
  { -0.0032326461344709249, 0.546278959642923, -47.182591996780744 },
  0.15,                                /* Mask Parameter: CompareToConstant1_const
                                        * Referenced by: '<S20>/Constant'
                                        */
  0.0,                                 /* Mask Parameter: CompareToConstant1_const_n
                                        * Referenced by: '<S10>/Constant'
                                        */
  130.0,                               /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S9>/Constant'
                                        */
  1.0,                                 /* Mask Parameter: CompareToConstant_const_b
                                        * Referenced by: '<S16>/Constant'
                                        */
  1.0,                                 /* Mask Parameter: CompareToConstant1_const_ni
                                        * Referenced by: '<S17>/Constant'
                                        */
  3.1415926535897931,                  /* Mask Parameter: CompareToConstant_const_e
                                        * Referenced by: '<S46>/Constant'
                                        */
  0.03,                                /* Mask Parameter: CompareToConstant1_const_k
                                        * Referenced by: '<S47>/Constant'
                                        */
  0.3,                                 /* Mask Parameter: CompareToConstant2_const
                                        * Referenced by: '<S48>/Constant'
                                        */
  0.03,                                /* Mask Parameter: CompareToConstant5_const
                                        * Referenced by: '<S49>/Constant'
                                        */
  1.0,                                 /* Mask Parameter: CompareToConstant_const_k
                                        * Referenced by: '<S31>/Constant'
                                        */
  1.0,                                 /* Mask Parameter: CompareToConstant1_const_l
                                        * Referenced by: '<S37>/Constant'
                                        */
  0.0,                                 /* Mask Parameter: CompareToConstant5_const_j
                                        * Referenced by: '<S44>/Constant'
                                        */
  0.0,                                 /* Mask Parameter: CompareToConstant1_const_p
                                        * Referenced by: '<S43>/Constant'
                                        */
  2.0,                                 /* Mask Parameter: CompareToConstant_const_f
                                        * Referenced by: '<S42>/Constant'
                                        */
  30.0,                                /* Mask Parameter: CompareToConstant_const_m
                                        * Referenced by: '<S30>/Constant'
                                        */
  131.0,                               /* Mask Parameter: CompareToConstant1_const_b
                                        * Referenced by: '<S24>/Constant'
                                        */
  1.0,                                 /* Mask Parameter: CompareToConstant_const_j
                                        * Referenced by: '<S8>/Constant'
                                        */
  1.0,                                 /* Mask Parameter: CompareToConstant1_const_g
                                        * Referenced by: '<S2>/Constant'
                                        */
  -0.5,                                /* Expression: -0.5
                                        * Referenced by: '<S3>/Constant9'
                                        */
  250.0,                               /* Expression: 250
                                        * Referenced by: '<S3>/Constant6'
                                        */
  500.0,                               /* Expression: 500
                                        * Referenced by: '<S3>/Constant7'
                                        */
  300.0,                               /* Expression: 300
                                        * Referenced by: '<S3>/Constant8'
                                        */
  115.0,                               /* Expression: 115
                                        * Referenced by: '<S3>/Constant1'
                                        */
  0.354,                               /* Expression: 0.885/2.5
                                        * Referenced by: '<S3>/Constant'
                                        */

  /*  Expression: [0 0 0]
   * Referenced by: '<S11>/u'
   */
  { 0.0, 0.0, 0.0 },

  /*  Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]
   * Referenced by: '<S11>/Gain2'
   */
  { 0.70710678118654757, -0.70710678118654757, 0.0, 0.70710678118654757,
    0.70710678118654757, 0.0, 0.0, 0.0, 1.0 },

  /*  Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]
   * Referenced by: '<S11>/Gain1'
   */
  { 0.70710678118654757, -0.70710678118654757, 0.0, 0.70710678118654757,
    0.70710678118654757, 0.0, 0.0, 0.0, 1.0 },
  0.015,                               /* Computed Parameter: DiscreteTimeIntegrator_gainval
                                        * Referenced by: '<S11>/Discrete-Time Integrator'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S11>/Discrete-Time Integrator'
                                        */

  /*  Expression: [satPhi satTheta satPsi]
   * Referenced by: '<S11>/Discrete-Time Integrator'
   */
  { 30.0, 30.0, 40.0 },

  /*  Expression: -[satPhi satTheta satPsi]
   * Referenced by: '<S11>/Discrete-Time Integrator'
   */
  { -30.0, -30.0, -40.0 },
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S15>/SP'
                                        */
  0.015,                               /* Computed Parameter: DiscreteTimeIntegrator2_gainval
                                        * Referenced by: '<S15>/Discrete-Time Integrator2'
                                        */

  /*  Expression: [0;0]
   * Referenced by: '<S15>/Discrete-Time Integrator2'
   */
  { 0.0, 0.0 },
  11.111111111111111,                  /* Expression: 1/0.09
                                        * Referenced by: '<S15>/Gain1'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S18>/SP'
                                        */
  0.015,                               /* Computed Parameter: DiscreteTimeIntegrator1_gainval
                                        * Referenced by: '<S18>/Discrete-Time Integrator1'
                                        */
  0.66666666666666663,                 /* Expression: 1/1.5
                                        * Referenced by: '<S18>/Gain3'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S19>/psi_SP'
                                        */
  0.0,                                 /* Computed Parameter: u_p_Y0
                                        * Referenced by: '<S14>/u_p'
                                        */
  0.0,                                 /* Computed Parameter: u_i_Y0
                                        * Referenced by: '<S14>/u_i'
                                        */
  0.0,                                 /* Computed Parameter: u_d_Y0
                                        * Referenced by: '<S14>/u_d'
                                        */

  /*  Expression: [0; 0; 0]
   * Referenced by: '<S14>/u'
   */
  { 0.0, 0.0, 0.0 },

  /*  Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]
   * Referenced by: '<S14>/Gain2'
   */
  { 0.70710678118654757, -0.70710678118654757, 0.0, 0.70710678118654757,
    0.70710678118654757, 0.0, 0.0, 0.0, 1.0 },
  0.015,                               /* Computed Parameter: DiscreteTimeIntegrator_gainva_l
                                        * Referenced by: '<S14>/Discrete-Time Integrator'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S14>/Discrete-Time Integrator'
                                        */

  /*  Expression: [satDphi; satDtheta; satDpsi]
   * Referenced by: '<S14>/Discrete-Time Integrator'
   */
  { 50.0, 50.0, 50.0 },

  /*  Expression: -[satDphi; satDtheta; satDpsi]
   * Referenced by: '<S14>/Discrete-Time Integrator'
   */
  { -50.0, -50.0, -50.0 },
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S4>/FL_Prop'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S4>/FR_Prop'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S4>/RL_Prop'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S4>/RR_Prop'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S4>/Phi_SP_log'
                                        */

  /*  Expression: [0;0;0]
   * Referenced by: '<S4>/DPhi_SP_log'
   */
  { 0.0, 0.0, 0.0 },
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S4>/throttle'
                                        */
  0.0,                                 /* Computed Parameter: u_p_Y0_o
                                        * Referenced by: '<S4>/u_p'
                                        */
  0.0,                                 /* Computed Parameter: u_i_Y0_g
                                        * Referenced by: '<S4>/u_i'
                                        */
  0.0,                                 /* Computed Parameter: u_d_Y0_m
                                        * Referenced by: '<S4>/u_d'
                                        */
  115.0,                               /* Expression: 115
                                        * Referenced by: '<S4>/Constant1'
                                        */
  -1.0,                                /* Expression: -1
                                        * Referenced by: '<S4>/Gain'
                                        */

  /*  Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]'
   * Referenced by: '<S4>/Gain1'
   */
  { 0.70710678118654757, 0.70710678118654757, 0.0, -0.70710678118654757,
    0.70710678118654757, 0.0, 0.0, 0.0, 1.0 },

  /*  Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]'
   * Referenced by: '<S4>/Gain2'
   */
  { 0.70710678118654757, 0.70710678118654757, 0.0, -0.70710678118654757,
    0.70710678118654757, 0.0, 0.0, 0.0, 1.0 },

  /*  Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]'
   * Referenced by: '<S4>/Gain3'
   */
  { 0.70710678118654757, 0.70710678118654757, 0.0, -0.70710678118654757,
    0.70710678118654757, 0.0, 0.0, 0.0, 1.0 },

  /*  Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]'
   * Referenced by: '<S4>/Gain4'
   */
  { 0.70710678118654757, 0.70710678118654757, 0.0, -0.70710678118654757,
    0.70710678118654757, 0.0, 0.0, 0.0, 1.0 },
  -1.0,                                /* Expression: -1
                                        * Referenced by: '<S21>/Gain'
                                        */

  /*  Expression: [0; 0; 0]
   * Referenced by: '<S21>/Constant'
   */
  { 0.0, 0.0, 0.0 },
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S45>/Constant2'
                                        */
  6.2831853071795862,                  /* Expression: 2*pi
                                        * Referenced by: '<S45>/Constant1'
                                        */
  0.0,                                 /* Computed Parameter: psi_SP_Y0_c
                                        * Referenced by: '<S45>/psi_SP'
                                        */
  6.2831853071795862,                  /* Expression: 2*pi
                                        * Referenced by: '<S45>/Constant'
                                        */
  6.2831853071795862,                  /* Expression: 2*pi
                                        * Referenced by: '<S45>/Gain1'
                                        */
  -1.0,                                /* Expression: -1
                                        * Referenced by: '<S45>/Gain'
                                        */

  /*  Expression: [0;0;0]
   * Referenced by: '<S41>/r_SP'
   */
  { 0.0, 0.0, 0.0 },

  /*  Expression: [0;0;0]
   * Referenced by: '<S41>/v_SP'
   */
  { 0.0, 0.0, 0.0 },

  /*  Expression: [0 0 0]
   * Referenced by: '<S6>/Phi_SP'
   */
  { 0.0, 0.0, 0.0 },
  0.0,                                 /* Computed Parameter: throttle_Y0_i
                                        * Referenced by: '<S6>/throttle'
                                        */
  0.0,                                 /* Computed Parameter: r_log_SP_Y0
                                        * Referenced by: '<S6>/r_log_SP'
                                        */

  /*  Expression: [0; 0; 0]
   * Referenced by: '<S6>/v_log_SP'
   */
  { 0.0, 0.0, 0.0 },
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/spReached'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S6>/landed'
                                        */
  0.09,                                /* Computed Parameter: DiscreteTimeIntegrator2_gainv_d
                                        * Referenced by: '<S38>/Discrete-Time Integrator2'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S38>/Discrete-Time Integrator2'
                                        */
  0.09,                                /* Computed Parameter: DiscreteTimeIntegrator2_gainv_f
                                        * Referenced by: '<S33>/Discrete-Time Integrator2'
                                        */
  0.09,                                /* Computed Parameter: DiscreteTimeIntegrator1_gainv_n
                                        * Referenced by: '<S35>/Discrete-Time Integrator1'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S35>/Discrete-Time Integrator1'
                                        */

  /*  Expression: [1 1 1]
   * Referenced by: '<S35>/Discrete-Time Integrator1'
   */
  { 1.0, 1.0, 1.0 },

  /*  Expression: -[1 1 1]
   * Referenced by: '<S35>/Discrete-Time Integrator1'
   */
  { -1.0, -1.0, -1.0 },

  /*  Expression: [10 10 10]
   * Referenced by: '<S6>/Saturation3'
   */
  { 10.0, 10.0, 10.0 },

  /*  Expression: [-10 -10 -0.6]
   * Referenced by: '<S6>/Saturation3'
   */
  { -10.0, -10.0, -0.6 },
  0.09,                                /* Computed Parameter: DiscreteTimeIntegrator_gainva_h
                                        * Referenced by: '<S36>/Discrete-Time Integrator'
                                        */

  /*  Expression: [0 0 0]
   * Referenced by: '<S36>/Discrete-Time Integrator'
   */
  { 0.0, 0.0, 0.0 },

  /*  Expression: [0.2 0.2 40]
   * Referenced by: '<S36>/Discrete-Time Integrator'
   */
  { 0.2, 0.2, 40.0 },

  /*  Expression: [-0.2 -0.2 -20]
   * Referenced by: '<S36>/Discrete-Time Integrator'
   */
  { -0.2, -0.2, -20.0 },

  /*  Expression: [0; 0; 285]
   * Referenced by: '<S36>/Constant'
   */
  { 0.0, 0.0, 285.0 },

  /*  Expression: [0.35 0.35 450]
   * Referenced by: '<S6>/Saturation1'
   */
  { 0.35, 0.35, 450.0 },

  /*  Expression: [-0.35 -0.35 170]
   * Referenced by: '<S6>/Saturation1'
   */
  { -0.35, -0.35, 170.0 },

  /*  Expression: 1./[0.5 0.5 0.5]
   * Referenced by: '<S33>/Gain1'
   */
  { 2.0, 2.0, 2.0 },
  5.0,                                 /* Expression: 1/0.2
                                        * Referenced by: '<S38>/Gain1'
                                        */
  0.0,                                 /* Computed Parameter: x_hat_r_Y0
                                        * Referenced by: '<S7>/x_hat_r'
                                        */
  0.0,                                 /* Computed Parameter: x_hat_v_Y0
                                        * Referenced by: '<S7>/x_hat_v'
                                        */
  0.0,                                 /* Computed Parameter: x_hat_a_Y0
                                        * Referenced by: '<S7>/x_hat_a'
                                        */
  2.3,                                 /* Expression: 2.3
                                        * Referenced by: '<S54>/Constant'
                                        */
  0.09,                                /* Computed Parameter: DiscreteTimeIntegrator1_gainv_i
                                        * Referenced by: '<S54>/Discrete-Time Integrator1'
                                        */
  0.09,                                /* Computed Parameter: DiscreteTimeIntegrator_gainva_d
                                        * Referenced by: '<S54>/Discrete-Time Integrator'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S54>/Discrete-Time Integrator'
                                        */
  1.2,                                 /* Expression: 0.6*2
                                        * Referenced by: '<S54>/Constant1'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S7>/Constant10'
                                        */
  100.0,                               /* Expression: 100
                                        * Referenced by: '<S7>/Constant11'
                                        */

  /*  Expression: [0; 0; -g]
   * Referenced by: '<S7>/Constant8'
   */
  { 0.0, 0.0, -9.81 },
  600.0,                               /* Expression: 600
                                        * Referenced by: '<S7>/Saturation'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S7>/Saturation'
                                        */
  1250.0,                              /* Expression: 1250
                                        * Referenced by: '<S3>/Constant2'
                                        */
  150.0,                               /* Expression: 150
                                        * Referenced by: '<S3>/Constant3'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S3>/Constant4'
                                        */

  /*  Expression: [0;0;0]
   * Referenced by: '<S3>/Constant5'
   */
  { 0.0, 0.0, 0.0 },
  0.0,                                 /* Expression: 0.0
                                        * Referenced by: '<S1>/Delay1'
                                        */

  /*  Expression: [0 0 0]'
   * Referenced by: '<S5>/Delay'
   */
  { 0.0, 0.0, 0.0 },
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S3>/Switch2'
                                        */
  0.0,                                 /* Expression: 0
                                        * Referenced by: '<S3>/Switch1'
                                        */
  1U,                                  /* Computed Parameter: Delay_DelayLength
                                        * Referenced by: '<S11>/Delay'
                                        */
  1U,                                  /* Computed Parameter: Delay1_DelayLength
                                        * Referenced by: '<S19>/Delay1'
                                        */
  1U,                                  /* Computed Parameter: Delay_DelayLength_d
                                        * Referenced by: '<S14>/Delay'
                                        */
  1U,                                  /* Computed Parameter: Delay_DelayLength_dg
                                        * Referenced by: '<S41>/Delay'
                                        */
  1U,                                  /* Computed Parameter: Delay1_DelayLength_i
                                        * Referenced by: '<S35>/Delay1'
                                        */
  1U,                                  /* Computed Parameter: Delay_DelayLength_df
                                        * Referenced by: '<S36>/Delay'
                                        */
  1U,                                  /* Computed Parameter: Delay3_DelayLength
                                        * Referenced by: '<S40>/Delay3'
                                        */
  1U,                                  /* Computed Parameter: Delay_DelayLength_k
                                        * Referenced by: '<S40>/Delay'
                                        */
  1U,                                  /* Computed Parameter: Delay1_DelayLength_m
                                        * Referenced by: '<S1>/Delay1'
                                        */
  1U,                                  /* Computed Parameter: Delay1_DelayLength_g
                                        * Referenced by: '<S23>/Delay1'
                                        */
  1U,                                  /* Computed Parameter: Delay2_DelayLength
                                        * Referenced by: '<S23>/Delay2'
                                        */
  1U,                                  /* Computed Parameter: Delay_DelayLength_p
                                        * Referenced by: '<S5>/Delay'
                                        */
  0,                                   /* Computed Parameter: E_Dpsi_SP_Y0
                                        * Referenced by: '<S19>/E_Dpsi_SP'
                                        */

  /*  Computed Parameter: E_v_SP_Y0
   * Referenced by: '<S41>/E_v_SP'
   */
  { 0, 0, 0 }
};
