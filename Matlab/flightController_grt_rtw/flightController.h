/*
 * flightController.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "flightController".
 *
 * Model version              : 1.1032
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Wed Jun 01 12:19:36 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_flightController_h_
#define RTW_HEADER_flightController_h_
#include <math.h>
#include <stddef.h>
#include <string.h>
#ifndef flightController_COMMON_INCLUDES_
# define flightController_COMMON_INCLUDES_
#include <stdlib.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#endif                                 /* flightController_COMMON_INCLUDES_ */

#include "flightController_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetBlockIO
# define rtmGetBlockIO(rtm)            ((rtm)->ModelData.blockIO)
#endif

#ifndef rtmSetBlockIO
# define rtmSetBlockIO(rtm, val)       ((rtm)->ModelData.blockIO = (val))
#endif

#ifndef rtmGetDefaultParam
# define rtmGetDefaultParam(rtm)       ((rtm)->ModelData.defaultParam)
#endif

#ifndef rtmSetDefaultParam
# define rtmSetDefaultParam(rtm, val)  ((rtm)->ModelData.defaultParam = (val))
#endif

#ifndef rtmGetParamIsMalloced
# define rtmGetParamIsMalloced(rtm)    ((rtm)->ModelData.paramIsMalloced)
#endif

#ifndef rtmSetParamIsMalloced
# define rtmSetParamIsMalloced(rtm, val) ((rtm)->ModelData.paramIsMalloced = (val))
#endif

#ifndef rtmGetPrevZCSigState
# define rtmGetPrevZCSigState(rtm)     ((rtm)->ModelData.prevZCSigState)
#endif

#ifndef rtmSetPrevZCSigState
# define rtmSetPrevZCSigState(rtm, val) ((rtm)->ModelData.prevZCSigState = (val))
#endif

#ifndef rtmGetRootDWork
# define rtmGetRootDWork(rtm)          ((rtm)->ModelData.dwork)
#endif

#ifndef rtmSetRootDWork
# define rtmSetRootDWork(rtm, val)     ((rtm)->ModelData.dwork = (val))
#endif

#ifndef rtmGetU
# define rtmGetU(rtm)                  ((rtm)->ModelData.inputs)
#endif

#ifndef rtmSetU
# define rtmSetU(rtm, val)             ((rtm)->ModelData.inputs = (val))
#endif

#ifndef rtmGetY
# define rtmGetY(rtm)                  ((rtm)->ModelData.outputs)
#endif

#ifndef rtmSetY
# define rtmSetY(rtm, val)             ((rtm)->ModelData.outputs = (val))
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#define flightController_M_TYPE        RT_MODEL_flightController_T

/* Block signals for system '<S22>/filterPhi' */
typedef struct {
  real_T y[2];                         /* '<S22>/filterPhi' */
} B_filterPhi_flightController_T;

/* Block states (auto storage) for system '<S22>/filterPhi' */
typedef struct {
  real_T x_prior_estimate[2];          /* '<S22>/filterPhi' */
  real_T P_prior[4];                   /* '<S22>/filterPhi' */
} DW_filterPhi_flightController_T;

/* Block signals for system '<S7>/filterX' */
typedef struct {
  real_T y[2];                         /* '<S7>/filterX' */
} B_filterX_flightController_T;

/* Block states (auto storage) for system '<S7>/filterX' */
typedef struct {
  real_T x_prior_estimate[2];          /* '<S7>/filterX' */
  real_T P_prior[4];                   /* '<S7>/filterX' */
} DW_filterX_flightController_T;

/* Block signals (auto storage) */
typedef struct {
  real_T RateTransition1;              /* '<S1>/Rate Transition1' */
  real_T RateTransition3[3];           /* '<S1>/Rate Transition3' */
  real_T Switch[3];                    /* '<S3>/Switch' */
  real_T z_r[3];                       /* '<S7>/z_r' */
  real_T y;                            /* '<S54>/Discrete-Time Integrator1' */
  real_T OutportBufferForx_hat_a[3];
  real_T OutportBufferForx_hat_v[3];
  real_T MultiportSwitch[3];           /* '<S34>/Multiport Switch' */
  real_T DiscreteTimeIntegrator2[3];   /* '<S33>/Discrete-Time Integrator2' */
  real_T Sum6[3];                      /* '<S35>/Sum6' */
  real_T Product2[3];                  /* '<S35>/Product2' */
  real_T Delay1[3];                    /* '<S35>/Delay1' */
  real_T Sum4[3];                      /* '<S36>/Sum4' */
  real_T Product5[3];                  /* '<S36>/Product5' */
  real_T Delay[3];                     /* '<S36>/Delay' */
  real_T MatrixMultiply1[3];           /* '<S6>/Matrix Multiply1' */
  real_T OutportBufferForlanded;       /* '<S6>/posSPmanager' */
  real_T OutportBufferForspReached;    /* '<S6>/posSPmanager' */
  real_T Sum5[3];                      /* '<S40>/Sum5' */
  real_T Delay_c;                      /* '<S40>/Delay' */
  real_T MultiportSwitch2;             /* '<S40>/Multiport Switch2' */
  real_T Switch1[3];                   /* '<S6>/Switch1' */
  real_T SP[3];                        /* '<S41>/SP' */
  real_T Abs[3];                       /* '<S41>/Abs' */
  real_T Delay_d[3];                   /* '<S41>/Delay' */
  real_T Switch_k[3];                  /* '<S41>/Switch' */
  real_T Gain;                         /* '<S45>/Gain' */
  real_T Sum1;                         /* '<S45>/Sum1' */
  real_T R_z[9];                       /* '<S6>/RotationMatrix' */
  real_T Divide[3];                    /* '<S14>/Divide' */
  real_T Product4[3];                  /* '<S14>/Product4' */
  real_T Product5_k[3];                /* '<S14>/Product5' */
  real_T Sum5_l[3];                    /* '<S14>/Sum5' */
  real_T MultiportSwitch1;             /* '<S19>/Multiport Switch1' */
  real_T DiscreteTimeIntegrator1;      /* '<S18>/Discrete-Time Integrator1' */
  real_T DiscreteTimeIntegrator2_m[2]; /* '<S15>/Discrete-Time Integrator2' */
  real_T Sum7[3];                      /* '<S11>/Sum7' */
  boolean_T OutportBufferForE_v_SP[3];
  boolean_T Compare;                   /* '<S20>/Compare' */
  B_filterX_flightController_T sf_filterZ;/* '<S7>/filterZ' */
  B_filterX_flightController_T sf_filterX;/* '<S7>/filterX' */
  B_filterPhi_flightController_T sf_filterTheta;/* '<S22>/filterTheta' */
  B_filterPhi_flightController_T sf_filterPhi;/* '<S22>/filterPhi' */
} B_flightController_T;

/* Block states (auto storage) for system '<Root>' */
typedef struct {
  real_T Delay1_DSTATE;                /* '<S1>/Delay1' */
  real_T Delay1_DSTATE_m[3];           /* '<S23>/Delay1' */
  real_T Delay2_DSTATE[3];             /* '<S23>/Delay2' */
  real_T Delay_DSTATE[3];              /* '<S5>/Delay' */
  real_T DiscreteTimeIntegrator1_DSTATE;/* '<S54>/Discrete-Time Integrator1' */
  real_T DiscreteTimeIntegrator_DSTATE;/* '<S54>/Discrete-Time Integrator' */
  real_T DiscreteTimeIntegrator2_DSTATE;/* '<S38>/Discrete-Time Integrator2' */
  real_T DiscreteTimeIntegrator2_DSTAT_c[3];/* '<S33>/Discrete-Time Integrator2' */
  real_T DiscreteTimeIntegrator1_DSTAT_l[3];/* '<S35>/Discrete-Time Integrator1' */
  real_T Delay1_DSTATE_g[3];           /* '<S35>/Delay1' */
  real_T DiscreteTimeIntegrator_DSTATE_l[3];/* '<S36>/Discrete-Time Integrator' */
  real_T Delay_DSTATE_f[3];            /* '<S36>/Delay' */
  real_T Delay3_DSTATE[3];             /* '<S40>/Delay3' */
  real_T Delay_DSTATE_e;               /* '<S40>/Delay' */
  real_T Delay_DSTATE_a[3];            /* '<S41>/Delay' */
  real_T Delay_DSTATE_c[3];            /* '<S14>/Delay' */
  real_T DiscreteTimeIntegrator_DSTATE_i[3];/* '<S14>/Discrete-Time Integrator' */
  real_T Delay1_DSTATE_g4;             /* '<S19>/Delay1' */
  real_T DiscreteTimeIntegrator1_DSTAT_b;/* '<S18>/Discrete-Time Integrator1' */
  real_T DiscreteTimeIntegrator2_DSTAT_p[2];/* '<S15>/Discrete-Time Integrator2' */
  real_T Delay_DSTATE_p[3];            /* '<S11>/Delay' */
  real_T DiscreteTimeIntegrator_DSTATE_h[3];/* '<S11>/Discrete-Time Integrator' */
  real_T x_prior_estimate[2];          /* '<S7>/filterY' */
  real_T P_prior[4];                   /* '<S7>/filterY' */
  real_T sequence;                     /* '<S34>/mode2sequence' */
  real_T r_SP[3];                      /* '<S34>/mode2sequence' */
  real_T lastGPSsp[3];                 /* '<S34>/mode2sequence' */
  real_T x_prior_estimate_n[2];        /* '<S22>/filterPsi' */
  real_T P_prior_f[4];                 /* '<S22>/filterPsi' */
  real_T rounds;                       /* '<S21>/CompassTiltCompensation' */
  real_T prevPsi;                      /* '<S21>/CompassTiltCompensation' */
  int8_T DiscreteTimeIntegrator_PrevRese;/* '<S14>/Discrete-Time Integrator' */
  int8_T DiscreteTimeIntegrator1_PrevRes;/* '<S18>/Discrete-Time Integrator1' */
  int8_T DiscreteTimeIntegrator_PrevRe_c;/* '<S11>/Discrete-Time Integrator' */
  uint8_T icLoad;                      /* '<S23>/Delay1' */
  uint8_T icLoad_d;                    /* '<S23>/Delay2' */
  uint8_T DiscreteTimeIntegrator1_IC_LOAD;/* '<S54>/Discrete-Time Integrator1' */
  uint8_T DiscreteTimeIntegrator2_IC_LOAD;/* '<S33>/Discrete-Time Integrator2' */
  uint8_T icLoad_do;                   /* '<S35>/Delay1' */
  uint8_T icLoad_k;                    /* '<S36>/Delay' */
  uint8_T icLoad_o;                    /* '<S40>/Delay3' */
  uint8_T icLoad_a;                    /* '<S40>/Delay' */
  uint8_T icLoad_e;                    /* '<S41>/Delay' */
  uint8_T icLoad_l;                    /* '<S14>/Delay' */
  uint8_T icLoad_f;                    /* '<S19>/Delay1' */
  uint8_T DiscreteTimeIntegrator1_IC_LO_k;/* '<S18>/Discrete-Time Integrator1' */
  uint8_T icLoad_lo;                   /* '<S11>/Delay' */
  boolean_T P_prior_not_empty;         /* '<S22>/filterPsi' */
  boolean_T prevPsi_not_empty;         /* '<S21>/CompassTiltCompensation' */
  boolean_T poDataProcessing_MODE;     /* '<S1>/poDataProcessing' */
  boolean_T poController_MODE;         /* '<S1>/poController' */
  boolean_T orController_MODE;         /* '<S1>/orController' */
  boolean_T velocitySPmanager_MODE;    /* '<S34>/velocitySPmanager' */
  boolean_T psiCalc_MODE;              /* '<S40>/psiCalc' */
  boolean_T psi_manual_MODE;           /* '<S12>/psi_manual' */
  boolean_T Autonomous_MODE;           /* '<S12>/Autonomous' */
  boolean_T psi_autonomous_MODE;       /* '<S12>/psi_autonomous' */
  boolean_T PositionController_MODE;   /* '<S4>/PositionController' */
  boolean_T VelocityController_MODE;   /* '<S4>/VelocityController' */
  DW_filterX_flightController_T sf_filterZ;/* '<S7>/filterZ' */
  DW_filterX_flightController_T sf_filterX;/* '<S7>/filterX' */
  DW_filterPhi_flightController_T sf_filterTheta;/* '<S22>/filterTheta' */
  DW_filterPhi_flightController_T sf_filterPhi;/* '<S22>/filterPhi' */
} DW_flightController_T;

/* Zero-crossing (trigger) state */
typedef struct {
  ZCSigState Delay_Reset_ZCE;          /* '<S40>/Delay' */
  ZCSigState Delay1_Reset_ZCE;         /* '<S19>/Delay1' */
} PrevZCX_flightController_T;

/* External inputs (root inport signals with auto storage) */
typedef struct {
  real_T mode;                         /* '<Root>/mode' */
  real_T armed;                        /* '<Root>/armed' */
  real_T D_orDataProcessing;           /* '<Root>/D_orDataProcessing' */
  real_T r_SP[3];                      /* '<Root>/r_SP' */
  real_T RCctrl[4];                    /* '<Root>/RCctrl' */
  real_T RCok;                         /* '<Root>/RCok' */
  real_T PID_r[9];                     /* '<Root>/PID_r' */
  real_T PID_v[9];                     /* '<Root>/PID_v' */
  real_T PID_Phi[9];                   /* '<Root>/PID_Phi' */
  real_T PID_DPhi[9];                  /* '<Root>/PID_DPhi' */
  real_T acc[3];                       /* '<Root>/acc' */
  real_T com[3];                       /* '<Root>/com' */
  real_T gyr[3];                       /* '<Root>/gyr' */
  real_T z_r[3];                       /* '<Root>/z_r' */
  real_T z_v[3];                       /* '<Root>/z_v' */
  real_T z_Phi[3];                     /* '<Root>/z_Phi' */
  real_T z_DPhi[3];                    /* '<Root>/z_DPhi' */
  real_T z_IR;                         /* '<Root>/z_IR' */
} ExtU_flightController_T;

/* External outputs (root outports fed by signals with auto storage) */
typedef struct {
  real_T FL_prop;                      /* '<Root>/FL_prop' */
  real_T FR_prop;                      /* '<Root>/FR_prop' */
  real_T RL_prop;                      /* '<Root>/RL_prop' */
  real_T RR_prop;                      /* '<Root>/RR_prop' */
  real_T spReached;                    /* '<Root>/spReached' */
  real_T x_hat_r_log[3];               /* '<Root>/x_hat_r_log' */
  real_T x_hat_v_log[3];               /* '<Root>/x_hat_v_log' */
  real_T x_hat_a_log[3];               /* '<Root>/x_hat_a_log' */
  real_T z_Phi_log[3];                 /* '<Root>/z_Phi_log' */
  real_T z_DPhi_log[3];                /* '<Root>/z_DPhi_log' */
  real_T x_hat_Phi_log[3];             /* '<Root>/x_hat_Phi_log' */
  real_T x_hat_DPhi_log[3];            /* '<Root>/x_hat_DPhi_log' */
  real_T r_SP_log[3];                  /* '<Root>/r_SP_log' */
  real_T v_SP_log[3];                  /* '<Root>/v_SP_log' */
  real_T Phi_SP_log[3];                /* '<Root>/Phi_SP_log' */
  real_T DPhi_SP_log[3];               /* '<Root>/DPhi_SP_log' */
  real_T throttle_log;                 /* '<Root>/throttle_log' */
  real_T landed;                       /* '<Root>/landed' */
  real_T u_p[3];                       /* '<Root>/u_p' */
  real_T u_i[3];                       /* '<Root>/u_i' */
  real_T u_d[3];                       /* '<Root>/u_d' */
} ExtY_flightController_T;

/* Parameters (auto storage) */
struct P_flightController_T_ {
  real_T F_c;                          /* Variable: F_c
                                        * Referenced by: '<S7>/Gain1'
                                        */
  real_T Q_phi[4];                     /* Variable: Q_phi
                                        * Referenced by: '<S22>/Constant2'
                                        */
  real_T Q_psi[4];                     /* Variable: Q_psi
                                        * Referenced by: '<S22>/Constant8'
                                        */
  real_T Q_theta[4];                   /* Variable: Q_theta
                                        * Referenced by: '<S22>/Constant5'
                                        */
  real_T Q_x[4];                       /* Variable: Q_x
                                        * Referenced by: '<S7>/Constant'
                                        */
  real_T Q_y[4];                       /* Variable: Q_y
                                        * Referenced by: '<S7>/Constant2'
                                        */
  real_T Q_z[4];                       /* Variable: Q_z
                                        * Referenced by: '<S7>/Constant4'
                                        */
  real_T R_phi[4];                     /* Variable: R_phi
                                        * Referenced by: '<S22>/Constant1'
                                        */
  real_T R_psi[4];                     /* Variable: R_psi
                                        * Referenced by: '<S22>/Constant7'
                                        */
  real_T R_theta[4];                   /* Variable: R_theta
                                        * Referenced by: '<S22>/Constant4'
                                        */
  real_T R_x[4];                       /* Variable: R_x
                                        * Referenced by: '<S7>/Constant1'
                                        */
  real_T R_y[4];                       /* Variable: R_y
                                        * Referenced by: '<S7>/Constant3'
                                        */
  real_T R_z[4];                       /* Variable: R_z
                                        * Referenced by: '<S7>/Constant5'
                                        */
  real_T dt;                           /* Variable: dt
                                        * Referenced by:
                                        *   '<S11>/Constant1'
                                        *   '<S14>/Constant1'
                                        *   '<S22>/Constant3'
                                        *   '<S22>/Constant6'
                                        *   '<S22>/Constant9'
                                        */
  real_T dt_p;                         /* Variable: dt_p
                                        * Referenced by:
                                        *   '<S7>/Constant6'
                                        *   '<S7>/Constant7'
                                        *   '<S7>/Constant9'
                                        *   '<S35>/Constant2'
                                        *   '<S36>/Constant1'
                                        */
  real_T lower_limit;                  /* Variable: lower_limit
                                        * Referenced by:
                                        *   '<S4>/Saturation'
                                        *   '<S4>/Saturation1'
                                        *   '<S4>/Saturation2'
                                        *   '<S4>/Saturation3'
                                        */
  real_T m;                            /* Variable: m
                                        * Referenced by: '<S7>/Gain1'
                                        */
  real_T upper_limit;                  /* Variable: upper_limit
                                        * Referenced by:
                                        *   '<S4>/Saturation'
                                        *   '<S4>/Saturation1'
                                        *   '<S4>/Saturation2'
                                        *   '<S4>/Saturation3'
                                        */
  real_T xPoly[3];                     /* Variable: xPoly
                                        * Referenced by: '<S21>/Polynomial1'
                                        */
  real_T yPoly[3];                     /* Variable: yPoly
                                        * Referenced by: '<S21>/Polynomial'
                                        */
  real_T zPoly[3];                     /* Variable: zPoly
                                        * Referenced by: '<S21>/Polynomial2'
                                        */
  real_T CompareToConstant1_const;     /* Mask Parameter: CompareToConstant1_const
                                        * Referenced by: '<S20>/Constant'
                                        */
  real_T CompareToConstant1_const_n;   /* Mask Parameter: CompareToConstant1_const_n
                                        * Referenced by: '<S10>/Constant'
                                        */
  real_T CompareToConstant_const;      /* Mask Parameter: CompareToConstant_const
                                        * Referenced by: '<S9>/Constant'
                                        */
  real_T CompareToConstant_const_b;    /* Mask Parameter: CompareToConstant_const_b
                                        * Referenced by: '<S16>/Constant'
                                        */
  real_T CompareToConstant1_const_ni;  /* Mask Parameter: CompareToConstant1_const_ni
                                        * Referenced by: '<S17>/Constant'
                                        */
  real_T CompareToConstant_const_e;    /* Mask Parameter: CompareToConstant_const_e
                                        * Referenced by: '<S46>/Constant'
                                        */
  real_T CompareToConstant1_const_k;   /* Mask Parameter: CompareToConstant1_const_k
                                        * Referenced by: '<S47>/Constant'
                                        */
  real_T CompareToConstant2_const;     /* Mask Parameter: CompareToConstant2_const
                                        * Referenced by: '<S48>/Constant'
                                        */
  real_T CompareToConstant5_const;     /* Mask Parameter: CompareToConstant5_const
                                        * Referenced by: '<S49>/Constant'
                                        */
  real_T CompareToConstant_const_k;    /* Mask Parameter: CompareToConstant_const_k
                                        * Referenced by: '<S31>/Constant'
                                        */
  real_T CompareToConstant1_const_l;   /* Mask Parameter: CompareToConstant1_const_l
                                        * Referenced by: '<S37>/Constant'
                                        */
  real_T CompareToConstant5_const_j;   /* Mask Parameter: CompareToConstant5_const_j
                                        * Referenced by: '<S44>/Constant'
                                        */
  real_T CompareToConstant1_const_p;   /* Mask Parameter: CompareToConstant1_const_p
                                        * Referenced by: '<S43>/Constant'
                                        */
  real_T CompareToConstant_const_f;    /* Mask Parameter: CompareToConstant_const_f
                                        * Referenced by: '<S42>/Constant'
                                        */
  real_T CompareToConstant_const_m;    /* Mask Parameter: CompareToConstant_const_m
                                        * Referenced by: '<S30>/Constant'
                                        */
  real_T CompareToConstant1_const_b;   /* Mask Parameter: CompareToConstant1_const_b
                                        * Referenced by: '<S24>/Constant'
                                        */
  real_T CompareToConstant_const_j;    /* Mask Parameter: CompareToConstant_const_j
                                        * Referenced by: '<S8>/Constant'
                                        */
  real_T CompareToConstant1_const_g;   /* Mask Parameter: CompareToConstant1_const_g
                                        * Referenced by: '<S2>/Constant'
                                        */
  real_T Constant9_Value;              /* Expression: -0.5
                                        * Referenced by: '<S3>/Constant9'
                                        */
  real_T Constant6_Value;              /* Expression: 250
                                        * Referenced by: '<S3>/Constant6'
                                        */
  real_T Constant7_Value;              /* Expression: 500
                                        * Referenced by: '<S3>/Constant7'
                                        */
  real_T Constant8_Value;              /* Expression: 300
                                        * Referenced by: '<S3>/Constant8'
                                        */
  real_T Constant1_Value;              /* Expression: 115
                                        * Referenced by: '<S3>/Constant1'
                                        */
  real_T Constant_Value;               /* Expression: 0.885/2.5
                                        * Referenced by: '<S3>/Constant'
                                        */
  real_T u_Y0[3];                      /* Expression: [0 0 0]
                                        * Referenced by: '<S11>/u'
                                        */
  real_T Gain2_Gain[9];                /* Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]
                                        * Referenced by: '<S11>/Gain2'
                                        */
  real_T Gain1_Gain[9];                /* Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]
                                        * Referenced by: '<S11>/Gain1'
                                        */
  real_T DiscreteTimeIntegrator_gainval;/* Computed Parameter: DiscreteTimeIntegrator_gainval
                                         * Referenced by: '<S11>/Discrete-Time Integrator'
                                         */
  real_T DiscreteTimeIntegrator_IC;    /* Expression: 0
                                        * Referenced by: '<S11>/Discrete-Time Integrator'
                                        */
  real_T DiscreteTimeIntegrator_UpperSat[3];/* Expression: [satPhi satTheta satPsi]
                                             * Referenced by: '<S11>/Discrete-Time Integrator'
                                             */
  real_T DiscreteTimeIntegrator_LowerSat[3];/* Expression: -[satPhi satTheta satPsi]
                                             * Referenced by: '<S11>/Discrete-Time Integrator'
                                             */
  real_T SP_Y0;                        /* Expression: 0
                                        * Referenced by: '<S15>/SP'
                                        */
  real_T DiscreteTimeIntegrator2_gainval;/* Computed Parameter: DiscreteTimeIntegrator2_gainval
                                          * Referenced by: '<S15>/Discrete-Time Integrator2'
                                          */
  real_T DiscreteTimeIntegrator2_IC[2];/* Expression: [0;0]
                                        * Referenced by: '<S15>/Discrete-Time Integrator2'
                                        */
  real_T Gain1_Gain_i;                 /* Expression: 1/0.09
                                        * Referenced by: '<S15>/Gain1'
                                        */
  real_T SP_Y0_m;                      /* Expression: 0
                                        * Referenced by: '<S18>/SP'
                                        */
  real_T DiscreteTimeIntegrator1_gainval;/* Computed Parameter: DiscreteTimeIntegrator1_gainval
                                          * Referenced by: '<S18>/Discrete-Time Integrator1'
                                          */
  real_T Gain3_Gain;                   /* Expression: 1/1.5
                                        * Referenced by: '<S18>/Gain3'
                                        */
  real_T psi_SP_Y0;                    /* Expression: 0
                                        * Referenced by: '<S19>/psi_SP'
                                        */
  real_T u_p_Y0;                       /* Computed Parameter: u_p_Y0
                                        * Referenced by: '<S14>/u_p'
                                        */
  real_T u_i_Y0;                       /* Computed Parameter: u_i_Y0
                                        * Referenced by: '<S14>/u_i'
                                        */
  real_T u_d_Y0;                       /* Computed Parameter: u_d_Y0
                                        * Referenced by: '<S14>/u_d'
                                        */
  real_T u_Y0_d[3];                    /* Expression: [0; 0; 0]
                                        * Referenced by: '<S14>/u'
                                        */
  real_T Gain2_Gain_o[9];              /* Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]
                                        * Referenced by: '<S14>/Gain2'
                                        */
  real_T DiscreteTimeIntegrator_gainva_l;/* Computed Parameter: DiscreteTimeIntegrator_gainva_l
                                          * Referenced by: '<S14>/Discrete-Time Integrator'
                                          */
  real_T DiscreteTimeIntegrator_IC_n;  /* Expression: 0
                                        * Referenced by: '<S14>/Discrete-Time Integrator'
                                        */
  real_T DiscreteTimeIntegrator_UpperS_e[3];/* Expression: [satDphi; satDtheta; satDpsi]
                                             * Referenced by: '<S14>/Discrete-Time Integrator'
                                             */
  real_T DiscreteTimeIntegrator_LowerS_l[3];/* Expression: -[satDphi; satDtheta; satDpsi]
                                             * Referenced by: '<S14>/Discrete-Time Integrator'
                                             */
  real_T FL_Prop_Y0;                   /* Expression: 0
                                        * Referenced by: '<S4>/FL_Prop'
                                        */
  real_T FR_Prop_Y0;                   /* Expression: 0
                                        * Referenced by: '<S4>/FR_Prop'
                                        */
  real_T RL_Prop_Y0;                   /* Expression: 0
                                        * Referenced by: '<S4>/RL_Prop'
                                        */
  real_T RR_Prop_Y0;                   /* Expression: 0
                                        * Referenced by: '<S4>/RR_Prop'
                                        */
  real_T Phi_SP_log_Y0;                /* Expression: 0
                                        * Referenced by: '<S4>/Phi_SP_log'
                                        */
  real_T DPhi_SP_log_Y0[3];            /* Expression: [0;0;0]
                                        * Referenced by: '<S4>/DPhi_SP_log'
                                        */
  real_T throttle_Y0;                  /* Expression: 0
                                        * Referenced by: '<S4>/throttle'
                                        */
  real_T u_p_Y0_o;                     /* Computed Parameter: u_p_Y0_o
                                        * Referenced by: '<S4>/u_p'
                                        */
  real_T u_i_Y0_g;                     /* Computed Parameter: u_i_Y0_g
                                        * Referenced by: '<S4>/u_i'
                                        */
  real_T u_d_Y0_m;                     /* Computed Parameter: u_d_Y0_m
                                        * Referenced by: '<S4>/u_d'
                                        */
  real_T Constant1_Value_h;            /* Expression: 115
                                        * Referenced by: '<S4>/Constant1'
                                        */
  real_T Gain_Gain;                    /* Expression: -1
                                        * Referenced by: '<S4>/Gain'
                                        */
  real_T Gain1_Gain_g[9];              /* Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]'
                                        * Referenced by: '<S4>/Gain1'
                                        */
  real_T Gain2_Gain_i[9];              /* Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]'
                                        * Referenced by: '<S4>/Gain2'
                                        */
  real_T Gain3_Gain_g[9];              /* Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]'
                                        * Referenced by: '<S4>/Gain3'
                                        */
  real_T Gain4_Gain[9];                /* Expression: [sqrt(2)/2 sqrt(2)/2 0; -sqrt(2)/2 sqrt(2)/2 0; 0 0 1]'
                                        * Referenced by: '<S4>/Gain4'
                                        */
  real_T Gain_Gain_a;                  /* Expression: -1
                                        * Referenced by: '<S21>/Gain'
                                        */
  real_T Constant_Value_g[3];          /* Expression: [0; 0; 0]
                                        * Referenced by: '<S21>/Constant'
                                        */
  real_T Constant2_Value;              /* Expression: 0
                                        * Referenced by: '<S45>/Constant2'
                                        */
  real_T Constant1_Value_j;            /* Expression: 2*pi
                                        * Referenced by: '<S45>/Constant1'
                                        */
  real_T psi_SP_Y0_c;                  /* Computed Parameter: psi_SP_Y0_c
                                        * Referenced by: '<S45>/psi_SP'
                                        */
  real_T Constant_Value_d;             /* Expression: 2*pi
                                        * Referenced by: '<S45>/Constant'
                                        */
  real_T Gain1_Gain_j;                 /* Expression: 2*pi
                                        * Referenced by: '<S45>/Gain1'
                                        */
  real_T Gain_Gain_i;                  /* Expression: -1
                                        * Referenced by: '<S45>/Gain'
                                        */
  real_T r_SP_Y0[3];                   /* Expression: [0;0;0]
                                        * Referenced by: '<S41>/r_SP'
                                        */
  real_T v_SP_Y0[3];                   /* Expression: [0;0;0]
                                        * Referenced by: '<S41>/v_SP'
                                        */
  real_T Phi_SP_Y0[3];                 /* Expression: [0 0 0]
                                        * Referenced by: '<S6>/Phi_SP'
                                        */
  real_T throttle_Y0_i;                /* Computed Parameter: throttle_Y0_i
                                        * Referenced by: '<S6>/throttle'
                                        */
  real_T r_log_SP_Y0;                  /* Computed Parameter: r_log_SP_Y0
                                        * Referenced by: '<S6>/r_log_SP'
                                        */
  real_T v_log_SP_Y0[3];               /* Expression: [0; 0; 0]
                                        * Referenced by: '<S6>/v_log_SP'
                                        */
  real_T spReached_Y0;                 /* Expression: 0
                                        * Referenced by: '<S6>/spReached'
                                        */
  real_T landed_Y0;                    /* Expression: 0
                                        * Referenced by: '<S6>/landed'
                                        */
  real_T DiscreteTimeIntegrator2_gainv_d;/* Computed Parameter: DiscreteTimeIntegrator2_gainv_d
                                          * Referenced by: '<S38>/Discrete-Time Integrator2'
                                          */
  real_T DiscreteTimeIntegrator2_IC_f; /* Expression: 0
                                        * Referenced by: '<S38>/Discrete-Time Integrator2'
                                        */
  real_T DiscreteTimeIntegrator2_gainv_f;/* Computed Parameter: DiscreteTimeIntegrator2_gainv_f
                                          * Referenced by: '<S33>/Discrete-Time Integrator2'
                                          */
  real_T DiscreteTimeIntegrator1_gainv_n;/* Computed Parameter: DiscreteTimeIntegrator1_gainv_n
                                          * Referenced by: '<S35>/Discrete-Time Integrator1'
                                          */
  real_T DiscreteTimeIntegrator1_IC;   /* Expression: 0
                                        * Referenced by: '<S35>/Discrete-Time Integrator1'
                                        */
  real_T DiscreteTimeIntegrator1_UpperSa[3];/* Expression: [1 1 1]
                                             * Referenced by: '<S35>/Discrete-Time Integrator1'
                                             */
  real_T DiscreteTimeIntegrator1_LowerSa[3];/* Expression: -[1 1 1]
                                             * Referenced by: '<S35>/Discrete-Time Integrator1'
                                             */
  real_T Saturation3_UpperSat[3];      /* Expression: [10 10 10]
                                        * Referenced by: '<S6>/Saturation3'
                                        */
  real_T Saturation3_LowerSat[3];      /* Expression: [-10 -10 -0.6]
                                        * Referenced by: '<S6>/Saturation3'
                                        */
  real_T DiscreteTimeIntegrator_gainva_h;/* Computed Parameter: DiscreteTimeIntegrator_gainva_h
                                          * Referenced by: '<S36>/Discrete-Time Integrator'
                                          */
  real_T DiscreteTimeIntegrator_IC_nb[3];/* Expression: [0 0 0]
                                          * Referenced by: '<S36>/Discrete-Time Integrator'
                                          */
  real_T DiscreteTimeIntegrator_UpperS_g[3];/* Expression: [0.2 0.2 40]
                                             * Referenced by: '<S36>/Discrete-Time Integrator'
                                             */
  real_T DiscreteTimeIntegrator_Lower_lv[3];/* Expression: [-0.2 -0.2 -20]
                                             * Referenced by: '<S36>/Discrete-Time Integrator'
                                             */
  real_T Constant_Value_d5[3];         /* Expression: [0; 0; 285]
                                        * Referenced by: '<S36>/Constant'
                                        */
  real_T Saturation1_UpperSat[3];      /* Expression: [0.35 0.35 450]
                                        * Referenced by: '<S6>/Saturation1'
                                        */
  real_T Saturation1_LowerSat[3];      /* Expression: [-0.35 -0.35 170]
                                        * Referenced by: '<S6>/Saturation1'
                                        */
  real_T Gain1_Gain_k[3];              /* Expression: 1./[0.5 0.5 0.5]
                                        * Referenced by: '<S33>/Gain1'
                                        */
  real_T Gain1_Gain_ka;                /* Expression: 1/0.2
                                        * Referenced by: '<S38>/Gain1'
                                        */
  real_T x_hat_r_Y0;                   /* Computed Parameter: x_hat_r_Y0
                                        * Referenced by: '<S7>/x_hat_r'
                                        */
  real_T x_hat_v_Y0;                   /* Computed Parameter: x_hat_v_Y0
                                        * Referenced by: '<S7>/x_hat_v'
                                        */
  real_T x_hat_a_Y0;                   /* Computed Parameter: x_hat_a_Y0
                                        * Referenced by: '<S7>/x_hat_a'
                                        */
  real_T Constant_Value_l;             /* Expression: 2.3
                                        * Referenced by: '<S54>/Constant'
                                        */
  real_T DiscreteTimeIntegrator1_gainv_i;/* Computed Parameter: DiscreteTimeIntegrator1_gainv_i
                                          * Referenced by: '<S54>/Discrete-Time Integrator1'
                                          */
  real_T DiscreteTimeIntegrator_gainva_d;/* Computed Parameter: DiscreteTimeIntegrator_gainva_d
                                          * Referenced by: '<S54>/Discrete-Time Integrator'
                                          */
  real_T DiscreteTimeIntegrator_IC_d;  /* Expression: 0
                                        * Referenced by: '<S54>/Discrete-Time Integrator'
                                        */
  real_T Constant1_Value_i;            /* Expression: 0.6*2
                                        * Referenced by: '<S54>/Constant1'
                                        */
  real_T Constant10_Value;             /* Expression: 0
                                        * Referenced by: '<S7>/Constant10'
                                        */
  real_T Constant11_Value;             /* Expression: 100
                                        * Referenced by: '<S7>/Constant11'
                                        */
  real_T Constant8_Value_b[3];         /* Expression: [0; 0; -g]
                                        * Referenced by: '<S7>/Constant8'
                                        */
  real_T Saturation_UpperSat;          /* Expression: 600
                                        * Referenced by: '<S7>/Saturation'
                                        */
  real_T Saturation_LowerSat;          /* Expression: 0
                                        * Referenced by: '<S7>/Saturation'
                                        */
  real_T Constant2_Value_i;            /* Expression: 1250
                                        * Referenced by: '<S3>/Constant2'
                                        */
  real_T Constant3_Value;              /* Expression: 150
                                        * Referenced by: '<S3>/Constant3'
                                        */
  real_T Constant4_Value;              /* Expression: 0
                                        * Referenced by: '<S3>/Constant4'
                                        */
  real_T Constant5_Value[3];           /* Expression: [0;0;0]
                                        * Referenced by: '<S3>/Constant5'
                                        */
  real_T Delay1_InitialCondition;      /* Expression: 0.0
                                        * Referenced by: '<S1>/Delay1'
                                        */
  real_T Delay_InitialCondition[3];    /* Expression: [0 0 0]'
                                        * Referenced by: '<S5>/Delay'
                                        */
  real_T Switch2_Threshold;            /* Expression: 0
                                        * Referenced by: '<S3>/Switch2'
                                        */
  real_T Switch1_Threshold;            /* Expression: 0
                                        * Referenced by: '<S3>/Switch1'
                                        */
  uint32_T Delay_DelayLength;          /* Computed Parameter: Delay_DelayLength
                                        * Referenced by: '<S11>/Delay'
                                        */
  uint32_T Delay1_DelayLength;         /* Computed Parameter: Delay1_DelayLength
                                        * Referenced by: '<S19>/Delay1'
                                        */
  uint32_T Delay_DelayLength_d;        /* Computed Parameter: Delay_DelayLength_d
                                        * Referenced by: '<S14>/Delay'
                                        */
  uint32_T Delay_DelayLength_dg;       /* Computed Parameter: Delay_DelayLength_dg
                                        * Referenced by: '<S41>/Delay'
                                        */
  uint32_T Delay1_DelayLength_i;       /* Computed Parameter: Delay1_DelayLength_i
                                        * Referenced by: '<S35>/Delay1'
                                        */
  uint32_T Delay_DelayLength_df;       /* Computed Parameter: Delay_DelayLength_df
                                        * Referenced by: '<S36>/Delay'
                                        */
  uint32_T Delay3_DelayLength;         /* Computed Parameter: Delay3_DelayLength
                                        * Referenced by: '<S40>/Delay3'
                                        */
  uint32_T Delay_DelayLength_k;        /* Computed Parameter: Delay_DelayLength_k
                                        * Referenced by: '<S40>/Delay'
                                        */
  uint32_T Delay1_DelayLength_m;       /* Computed Parameter: Delay1_DelayLength_m
                                        * Referenced by: '<S1>/Delay1'
                                        */
  uint32_T Delay1_DelayLength_g;       /* Computed Parameter: Delay1_DelayLength_g
                                        * Referenced by: '<S23>/Delay1'
                                        */
  uint32_T Delay2_DelayLength;         /* Computed Parameter: Delay2_DelayLength
                                        * Referenced by: '<S23>/Delay2'
                                        */
  uint32_T Delay_DelayLength_p;        /* Computed Parameter: Delay_DelayLength_p
                                        * Referenced by: '<S5>/Delay'
                                        */
  boolean_T E_Dpsi_SP_Y0;              /* Computed Parameter: E_Dpsi_SP_Y0
                                        * Referenced by: '<S19>/E_Dpsi_SP'
                                        */
  boolean_T E_v_SP_Y0[3];              /* Computed Parameter: E_v_SP_Y0
                                        * Referenced by: '<S41>/E_v_SP'
                                        */
};

/* Real-time Model Data Structure */
struct tag_RTM_flightController_T {
  const char_T *errorStatus;

  /*
   * ModelData:
   * The following substructure contains information regarding
   * the data used in the model.
   */
  struct {
    B_flightController_T *blockIO;
    P_flightController_T *defaultParam;
    PrevZCX_flightController_T *prevZCSigState;
    ExtU_flightController_T *inputs;
    ExtY_flightController_T *outputs;
    boolean_T paramIsMalloced;
    DW_flightController_T *dwork;
  } ModelData;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    struct {
      uint8_T TID[2];
    } TaskCounters;
  } Timing;
};

extern P_flightController_T flightController_P;/* parameters */

/* External data declarations for dependent source files */
extern const char *RT_MEMORY_ALLOCATION_ERROR;

/* Model entry point functions */
extern RT_MODEL_flightController_T *flightController(void);
extern void flightController_initialize(RT_MODEL_flightController_T *const
  flightController_M);
extern void flightController_step(RT_MODEL_flightController_T *const
  flightController_M);
extern void flightController_terminate(RT_MODEL_flightController_T
  * flightController_M);

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Note that this particular code originates from a subsystem build,
 * and has its own system numbers different from the parent model.
 * Refer to the system hierarchy for this subsystem below, and use the
 * MATLAB hilite_system command to trace the generated code back
 * to the parent model.  For example,
 *
 * hilite_system('flightController_Harness/flightController')    - opens subsystem flightController_Harness/flightController
 * hilite_system('flightController_Harness/flightController/Kp') - opens and selects block Kp
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'flightController_Harness'
 * '<S1>'   : 'flightController_Harness/flightController'
 * '<S2>'   : 'flightController_Harness/flightController/Compare To Constant1'
 * '<S3>'   : 'flightController_Harness/flightController/RcCtrlManagement'
 * '<S4>'   : 'flightController_Harness/flightController/orController'
 * '<S5>'   : 'flightController_Harness/flightController/orDataProcessing'
 * '<S6>'   : 'flightController_Harness/flightController/poController'
 * '<S7>'   : 'flightController_Harness/flightController/poDataProcessing'
 * '<S8>'   : 'flightController_Harness/flightController/RcCtrlManagement/Compare To Constant'
 * '<S9>'   : 'flightController_Harness/flightController/orController/Compare To Constant'
 * '<S10>'  : 'flightController_Harness/flightController/orController/Compare To Constant1'
 * '<S11>'  : 'flightController_Harness/flightController/orController/PositionController'
 * '<S12>'  : 'flightController_Harness/flightController/orController/SPmanager'
 * '<S13>'  : 'flightController_Harness/flightController/orController/Saturation Dynamic'
 * '<S14>'  : 'flightController_Harness/flightController/orController/VelocityController'
 * '<S15>'  : 'flightController_Harness/flightController/orController/SPmanager/Autonomous'
 * '<S16>'  : 'flightController_Harness/flightController/orController/SPmanager/Compare To Constant'
 * '<S17>'  : 'flightController_Harness/flightController/orController/SPmanager/Compare To Constant1'
 * '<S18>'  : 'flightController_Harness/flightController/orController/SPmanager/psi_autonomous'
 * '<S19>'  : 'flightController_Harness/flightController/orController/SPmanager/psi_manual'
 * '<S20>'  : 'flightController_Harness/flightController/orController/SPmanager/psi_manual/Compare To Constant1'
 * '<S21>'  : 'flightController_Harness/flightController/orDataProcessing/DataProcessing'
 * '<S22>'  : 'flightController_Harness/flightController/orDataProcessing/Filter'
 * '<S23>'  : 'flightController_Harness/flightController/orDataProcessing/acceptanceTest'
 * '<S24>'  : 'flightController_Harness/flightController/orDataProcessing/DataProcessing/Compare To Constant1'
 * '<S25>'  : 'flightController_Harness/flightController/orDataProcessing/DataProcessing/CompassTiltCompensation'
 * '<S26>'  : 'flightController_Harness/flightController/orDataProcessing/DataProcessing/GyroTiltCompensation'
 * '<S27>'  : 'flightController_Harness/flightController/orDataProcessing/Filter/filterPhi'
 * '<S28>'  : 'flightController_Harness/flightController/orDataProcessing/Filter/filterPsi'
 * '<S29>'  : 'flightController_Harness/flightController/orDataProcessing/Filter/filterTheta'
 * '<S30>'  : 'flightController_Harness/flightController/orDataProcessing/acceptanceTest/Compare To Constant'
 * '<S31>'  : 'flightController_Harness/flightController/poController/Compare To Constant'
 * '<S32>'  : 'flightController_Harness/flightController/poController/RotationMatrix'
 * '<S33>'  : 'flightController_Harness/flightController/poController/firstOrderFilter1'
 * '<S34>'  : 'flightController_Harness/flightController/poController/posSPmanager'
 * '<S35>'  : 'flightController_Harness/flightController/poController/positionController'
 * '<S36>'  : 'flightController_Harness/flightController/poController/velocityController'
 * '<S37>'  : 'flightController_Harness/flightController/poController/posSPmanager/Compare To Constant1'
 * '<S38>'  : 'flightController_Harness/flightController/poController/posSPmanager/firstOrderFilter'
 * '<S39>'  : 'flightController_Harness/flightController/poController/posSPmanager/mode2sequence'
 * '<S40>'  : 'flightController_Harness/flightController/poController/posSPmanager/psi_SPmanager'
 * '<S41>'  : 'flightController_Harness/flightController/poController/posSPmanager/velocitySPmanager'
 * '<S42>'  : 'flightController_Harness/flightController/poController/posSPmanager/psi_SPmanager/Compare To Constant'
 * '<S43>'  : 'flightController_Harness/flightController/poController/posSPmanager/psi_SPmanager/Compare To Constant1'
 * '<S44>'  : 'flightController_Harness/flightController/poController/posSPmanager/psi_SPmanager/Compare To Constant5'
 * '<S45>'  : 'flightController_Harness/flightController/poController/posSPmanager/psi_SPmanager/psiCalc'
 * '<S46>'  : 'flightController_Harness/flightController/poController/posSPmanager/psi_SPmanager/psiCalc/Compare To Constant'
 * '<S47>'  : 'flightController_Harness/flightController/poController/posSPmanager/velocitySPmanager/Compare To Constant1'
 * '<S48>'  : 'flightController_Harness/flightController/poController/posSPmanager/velocitySPmanager/Compare To Constant2'
 * '<S49>'  : 'flightController_Harness/flightController/poController/posSPmanager/velocitySPmanager/Compare To Constant5'
 * '<S50>'  : 'flightController_Harness/flightController/poDataProcessing/RotationMatrix'
 * '<S51>'  : 'flightController_Harness/flightController/poDataProcessing/filterX'
 * '<S52>'  : 'flightController_Harness/flightController/poDataProcessing/filterY'
 * '<S53>'  : 'flightController_Harness/flightController/poDataProcessing/filterZ'
 * '<S54>'  : 'flightController_Harness/flightController/poDataProcessing/secondOrderFilter'
 */
#endif                                 /* RTW_HEADER_flightController_h_ */
