/*
 * flightController_private.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "flightController".
 *
 * Model version              : 1.1032
 * Simulink Coder version : 8.8 (R2015a) 09-Feb-2015
 * C source code generated on : Wed Jun 01 12:19:36 2016
 *
 * Target selection: grt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objective: Execution efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_flightController_private_h_
#define RTW_HEADER_flightController_private_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#if !defined(rt_VALIDATE_MEMORY)
#define rt_VALIDATE_MEMORY(S, ptr)     if(!(ptr)) {\
 rtmSetErrorStatus(flightController_M, RT_MEMORY_ALLOCATION_ERROR);\
 }
#endif

#if !defined(rt_FREE)
#if !defined(_WIN32)
#define rt_FREE(ptr)                   if((ptr) != (NULL)) {\
 free((ptr));\
 (ptr) = (NULL);\
 }
#else

/* Visual and other windows compilers declare free without const */
#define rt_FREE(ptr)                   if((ptr) != (NULL)) {\
 free((void *)(ptr));\
 (ptr) = (NULL);\
 }
#endif
#endif

extern real_T rt_atan2d_snf(real_T u0, real_T u1);
void flightController_filterPhi_Init(DW_filterPhi_flightController_T *localDW);
void flightController_filterPhi(real_T rtu_u, real_T rtu_u_i, const real_T
  rtu_Q[4], const real_T rtu_R[4], real_T rtu_dt, B_filterPhi_flightController_T
  *localB, DW_filterPhi_flightController_T *localDW);
void flightController_filterX_Init(DW_filterX_flightController_T *localDW);
void flightController_filterX(real_T rtu_z, real_T rtu_z_c, const real_T rtu_Q[4],
  const real_T rtu_R[4], real_T rtu_dt, B_filterX_flightController_T *localB,
  DW_filterX_flightController_T *localDW);

#endif                                 /* RTW_HEADER_flightController_private_h_ */
