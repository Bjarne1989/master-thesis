clear
load('parameters.mat');
m = params.m;
L = params.L;
g = params.g;
I_x = params.I_x;
I_y = params.I_y;
I_z = params.I_z;
M_c = params.M_c;
T_c = params.T_c;
F_c = params.F_c;
t_delay = params.t_delay;
t_tau = params.t_tau;
u_offset = params.u_offset;
u_max = params.u_max;

%% variance
Q_phi = params.Q_phi;
Q_theta = params.Q_theta;
Q_psi = params.Q_psi;

R_phi = params.R_phi;
R_theta = params.R_theta;
R_psi = params.R_psi;

%% Controller values
Phi_param(1) = 4.0;
Phi_param(2) = 1.5;
Phi_param(3) = 14;
Theta_param(1) = Phi_param(1);
Theta_param(2) = Phi_param(2);
Theta_param(3) = Phi_param(3);
Psi_param(1) = 10;
Psi_param(2) = 0.1;
Psi_param(3) = 12;

satPhi = 50;
satTheta = 50;
satPsi = 50;

upper_limit = 1000;
lower_limit = 110;
%%
dt = 0.015;
t_sim = 30;
%% Test data
% load('RP12I0_05D15YP30I3D0.mat');
% real = RP12I0_05D15YP30I3D0;

u_temp = m*g/(4*F_c);
phi_sp = timeseries([0 0 1 1 0 0]'*0.3, [0 2 3 15 16 t_sim]);%timeseries(real.Data(8,200:2200)', real.Time(200:2200));
theta_sp = timeseries([0 0 1 1]'*0.3, [0 8 9 t_sim]); %timeseries(real.Data(9,200:2200)', real.Time(200:2200));
psi_sp = timeseries([0 0 1 1]'*3, [0 4 8 t_sim]); %timeseries(real.Data(10,200:2200)', real.Time(200:2200));
throttle_sp = timeseries([u_temp u_temp]', [0 t_sim]); %timeseries(real.Data(7,200:2200)', real.Time(200:2200));
%% simulate
model = 'PID_Harness';
simout = sim(model, 'Solver', 'ode4', 'StopTime', 't_sim', 'FixedStep', 'dt',...
    'SaveOutput', 'on', 'OutputSaveName', 'y', 'SaveFormat', 'StructureWithTime');
output = simout.get('y');

time = output.time;
Dr = getSimulinkOutput(model, 'Dr', output);
Phi = getSimulinkOutput(model, 'Phi', output);
DPhi = getSimulinkOutput(model, 'DPhi', output);

%% Plot
figure(1); clf(1);
plot(time, Dr(:,1),'b',time, Dr(:,2), 'r', time, Dr(:,3), 'm');
title('Velocity')
ylabel('Velocity [m/s]');
legend('x','y','z');

figure(2); clf(2);
subplot(311);hold('on')
plot(time, Phi(:,1));
plot(phi_sp.Time, phi_sp.Data);
title('Angular position')
ylabel('Position [Rad]');
legend('phi','SP');

subplot(312); hold('on');
plot(time,Phi(:,2));
plot(theta_sp.Time, theta_sp.Data());
ylabel('Position [Rad]');
legend('theta', 'SP');

subplot(313); hold('on');
plot(time,Phi(:,3));
plot(psi_sp.Time, psi_sp.Data);
ylabel('Position [Rad]');
xlabel('time [s]');
legend('psi','SP');

figure(3); clf(3);
subplot(311);
plot(time,DPhi(:,1));
title('Angular velocity')
ylabel('Velocity [Rad/s]');
legend('Dphi');

subplot(312);
plot(time,DPhi(:,2));
ylabel('Velocity [Rad/s]');
legend('Dtheta');

subplot(313); hold('on');
plot(time,DPhi(:,3));
ylabel('Velocity [Rad/s]');
xlabel('time [s]');
legend('Dpsi');


% %% Plot
% figure(1); clf(1);
% plot(time, Dr(:,1),'b',time, Dr(:,2), 'r', time, Dr(:,3), 'm');
% title('Velocity')
% ylabel('Velocity [m/s]');
% legend('x','y','z');
% 
% figure(2); clf(2);
% subplot(311);hold('on')
% plot(time, Phi(:,1));
% plot(real.Time(200:2200), real.Data(1,200:2200));
% plot(phi_sp.Time, phi_sp.Data, 'g');
% xlim([3 30]);
% title('Angular position')
% ylabel('Position [Rad]');
% legend('phi, modeled', 'phi, real','SP');
% 
% subplot(312); 
% hold('on');
% plot(time,Phi(:,2));
% plot(real.Time, real.Data(3,:));
% plot(theta_sp.Time, theta_sp.Data(), 'g');
% xlim([3 30]);
% ylabel('Position [Rad]');
% legend('theta, modeled', 'theta, real','SP');
% 
% subplot(313); hold('on');
% plot(time,DPhi(:,3));
% plot(real.Time, real.Data(6,:));
% plot(psi_sp.Time, psi_sp.Data, 'g');
% xlim([3 30]);
% ylabel('Velocity [Rad/s]');
% xlabel('time [s]');
% legend('Dpsi modeled', 'Dpsi, real','SP');
% % plot(time,Phi(:,3));
% % ylabel('Position [Rad]');
% % xlabel('time [s]');
% 
% figure(3); clf(3);
% subplot(311);
% plot(time,DPhi(:,1));
% title('Angular velocity')
% ylabel('Velocity [Rad/s]');
% legend('Dphi');
% 
% subplot(312);
% plot(time,DPhi(:,2));
% ylabel('Velocity [Rad/s]');
% legend('Dtheta');
% 
% subplot(313); hold('on');
% plot(time,DPhi(:,3));
% plot(psi_sp.Time, psi_sp.Data);
% plot(real.Time, real.Data(6,:));
% ylabel('Velocity [Rad/s]');
% xlabel('time [s]');
% legend('Dpsi');
% legend('psi','SP');