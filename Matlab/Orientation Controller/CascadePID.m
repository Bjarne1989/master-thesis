%clear
load('parameters.mat');
m = params.m;
L = params.L;
g = params.g;
I_x = params.I_x;
I_y = params.I_y;
I_z = params.I_z;
M_c = params.M_c;
T_c = params.T_c*1;
F_c = params.F_c;
t_delay = params.t_delay;
t_tau = params.t_tau;
u_offset = params.u_offset;
u_max = params.u_max;

%% variance
Q_phi = params.Q_phi;
Q_theta = params.Q_theta;
Q_psi = params.Q_psi;

R_phi = params.R_phi;
R_theta = params.R_theta;
R_psi = params.R_psi;

%% Controller values

%Velocity controller
Dphi_PID(1) = 12.3;%5.5;
Dphi_PID(2) = 1.2;%0.7;
Dphi_PID(3) = 2.7;
Dtheta_PID(1) = Dphi_PID(1);
Dtheta_PID(2) = Dphi_PID(2);
Dtheta_PID(3) = Dphi_PID(3);
Dpsi_PID(1) = 55;%30;
Dpsi_PID(2) = 0.5;%0.001;
Dpsi_PID(3) = 0.1;

satPhi = 30;
satTheta = 30;
satPsi = 40;

%Position controller
phi_PID(1) = 7.5;
phi_PID(2) = 0;
phi_PID(3) = 0;
theta_PID(1) = phi_PID(1);
theta_PID(2) = phi_PID(2);
theta_PID(3) = phi_PID(3);
psi_PID(1) = 2;
psi_PID(2) = 0;
psi_PID(3) = 0;

satDphi = 50;
satDtheta = 50;
satDpsi = 50;

upper_limit = 600;
lower_limit = 120;

params.cascade.Dphi_PID = Dphi_PID;
params.cascade.Dtheta_PID = Dtheta_PID;
params.cascade.Dpsi_PID = Dpsi_PID;

params.cascade.satPhi = satPhi;
params.cascade.satTheta = satTheta;
params.cascade.satPsi = satPsi;

params.cascade.phi_PID = phi_PID;
params.cascade.theta_PID = theta_PID;
params.cascade.psi_PID = psi_PID;

params.cascade.satDphi = satDphi;
params.cascade.satDtheta = satDtheta;
params.cascade.satDpsi = satDpsi;
params.cascade.upper_limit = upper_limit;
params.cascade.lower_limit = lower_limit;


%% Save paramters
save('D:\Dropbox\Skole\10.Semester\Matlab\Common\parameters.mat','params')
%%
dt = 0.015;
t_sim = 30;
%% Test data
u_temp = m*g/(4*F_c) + 10;
phi_sp = timeseries([0 0 1 1 -1 -1 0 0]'*0, [0 2 2.5 3 3.5 4.5 5 t_sim]);
theta_sp = timeseries([0 0 1 1 -1 -1 0 0]'*0.4, [0 15 15.5 21.5 22.5 27 27.5 t_sim]);
psi_sp = timeseries([0 0 3 3]'*0, [0 15 15 t_sim]);
Dpsi_SP = timeseries([0 0 1 1 0 0 0 -1 -1 0 0]'*0.5, [0 6 7 8 9 20 20 21 22 23 t_sim]);
throttle_sp = timeseries([150 u_temp u_temp]', [0 5 t_sim]);

Phi_0 = [0;0;0];

dist = timeseries([0 1 1]'*0, [0 5 t_sim]);  
%% simulate
model = 'CascadePID_Harness';
simout = sim(model, 'Solver', 'ode4', 'StopTime', 't_sim', 'FixedStep', 'dt',...
    'SaveOutput', 'on', 'OutputSaveName', 'y', 'SaveFormat', 'StructureWithTime');
output = simout.get('y');

time = output.time;
Dr = getSimulinkOutput(model, 'Dr', output);
Phi = getSimulinkOutput(model, 'Phi', output);
DPhi = getSimulinkOutput(model, 'DPhi', output);
u_Phi = getSimulinkOutput(model, 'u_Phi', output);
u_prop = getSimulinkOutput(model, 'u_prop', output);

%% Plot
figure(1); clf(1);
plot(time, Dr(:,1),'b',time, Dr(:,2), 'r', time, Dr(:,3), 'm');
title('Velocity')
ylabel('Velocity [m/s]');
legend('x','y','z');

figure(2); clf(2);
subplot(311);hold('on')
plot(time, Phi(:,1));
plot(phi_sp.Time, phi_sp.Data);
title('Angular position')
ylabel('Position [Rad]');
legend('mod. phi','SP');

subplot(312); hold('on');
plot(time,Phi(:,2));
plot(theta_sp.Time, theta_sp.Data());
ylabel('Position [Rad]');
legend('mod. theta', 'SP');

subplot(313); hold('on');
plot(time,Phi(:,3));
plot(psi_sp.Time, psi_sp.Data);
plot(Dpsi_SP.Time, Dpsi_SP.Data);
ylabel('Position [Rad]');
xlabel('time [s]');
legend('mod. psi','SP');

figure(3); clf(3);
subplot(311); hold('on');
plot(time,DPhi(:,1));
plot(time, u_Phi(:,1));
title('Angular velocity')
ylabel('Velocity [Rad/s]');
legend('mod. Dphi', 'mod. SP');

subplot(312); hold('on');
plot(time,DPhi(:,2));
plot(time, u_Phi(:,2));
ylabel('Velocity [Rad/s]');
legend('mod. Dtheta',  'mod. SP');

subplot(313); hold('on');
plot(time,DPhi(:,3));
plot(time, u_Phi(:,3));
ylabel('Velocity [Rad/s]');
xlabel('time [s]');
legend('mod. Dpsi', 'mod. SP');

figure(4); clf(4);
hold('on');
title('Modelled propellar power')
plot(time, u_prop);
ylabel('Power 0-1000');
legend('FL', 'FR', 'RL', 'RR');