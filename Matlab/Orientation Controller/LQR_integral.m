%clear

load('parameters.mat');
m = params.m;
L = params.L;
g = params.g;
I_x = params.I_x;
I_y = params.I_y;
I_z = params.I_z;
M_c = params.M_c;
T_c = params.T_c*3.5;
F_c = params.F_c;
t_delay = params.t_delay;
t_tau = params.t_tau;
t_tau = 0.01;
u_offset = params.u_offset;
u_max = params.u_max;

%% variance
Q_phi = params.Q_phi;
Q_theta = params.Q_theta;
Q_psi = params.Q_psi;

R_phi = params.R_phi;
R_theta = params.R_theta;
R_psi = params.R_psi;

%% Controller values
syms Dphi Dtheta Dpsi phi theta psi Iphi Itheta Ipsi
syms u_x u_y u_z

wi_ib = [Dphi Dtheta Dpsi]';

Euler = params.E_b_inv;
DPhi = Euler*wi_ib;

T = [L*F_c/sqrt(2)*u_x; L*F_c/sqrt(2)*u_y; T_c*u_z];
DDPhi = inv(M_c)*T - inv(M_c)*cross(wi_ib, M_c*wi_ib);

Dint = [phi theta psi];

jacob = [Dint(1); DPhi(1); DDPhi(1); Dint(2); DPhi(2); DDPhi(2); Dint(3); DPhi(3); DDPhi(3)];

A = jacobian(jacob, [Iphi phi Dphi Itheta theta Dtheta Ipsi psi Dpsi]);
B = double(jacobian(jacob, [u_x u_y u_z]));
A = double(subs(A, [phi, Dphi, theta, Dtheta, psi, Dpsi], [0, 0, 0, 0, 0, 0]));
C = diag([1 1 1 1 1 1 1 1 1]);

%% Controller tuning

Q_int_pitch = 0.2;%6/20;
Q_pos_pitch = 85;
Q_vel_pitch = 60;

Q_int_roll = Q_int_pitch;
Q_pos_roll = Q_pos_pitch;
Q_vel_roll = Q_vel_pitch;

Q_int_yaw = 0.01;%3/200;
Q_pos_yaw = 50;
Q_vel_yaw = 0.1;

R_rollPitch = 0.5;%1/30;
R_yaw = 0.4;%1/300;

Q = diag([Q_int_pitch Q_pos_pitch Q_vel_pitch...
          Q_int_roll Q_pos_roll Q_vel_roll...
          Q_int_yaw Q_pos_yaw Q_vel_yaw]);
R = diag([R_rollPitch R_rollPitch R_yaw]);

K = lqr(A, B, Q, R)

upper_limit = 600;
lower_limit = 120;
%%
dt = 0.015;
t_sim = 30;
%% Test data

u_temp = m*g/(4*F_c) + 10;
phi_sp = timeseries([0 0 1 1 -1 -1 0 0]'*0.4, [0 9 9.5 10 10.5 11.5 12 t_sim]);
theta_sp = timeseries([0 0 1 1 -1 -1 0 0]'*0.4, [0 15 15.5 21.5 22.5 27 27.5 t_sim]);
psi_sp = timeseries([0 0 1 1]'*1, [0 20 20 t_sim]);
Dpsi_SP = timeseries([0 0 1 1 0 0]'*0.5, [0 1 2 5 6 t_sim]);
throttle_sp = timeseries([u_temp u_temp]', [0 t_sim]);

Phi_0 = [0;0;0];

dist = timeseries([1 1]'*0, [0 t_sim]);  

%% simulate

model = 'LQR_integral_Harness';
simout = sim(model, 'Solver', 'ode4', 'StopTime', 't_sim', 'FixedStep', 'dt',...
    'SaveOutput', 'on', 'OutputSaveName', 'y', 'SaveFormat', 'StructureWithTime');
output = simout.get('y');

time = output.time;
Dr = getSimulinkOutput(model, 'Dr', output);
Phi = getSimulinkOutput(model, 'Phi', output);
DPhi = getSimulinkOutput(model, 'DPhi', output);
u_prop = getSimulinkOutput(model, 'u_prop', output);


%% Plot

figure(1); clf(1);
plot(time, Dr(:,1),'b',time, Dr(:,2), 'r', time, Dr(:,3), 'm');
title('Velocity')
ylabel('Velocity [m/s]');
legend('x','y','z');

figure(2); clf(2);
subplot(311);hold('on')
plot(time, Phi(:,1));
plot(phi_sp.Time, phi_sp.Data);
title('Angular position')
ylabel('Position [Rad]');
legend('mod. phi','SP');

subplot(312); hold('on');
plot(time,Phi(:,2));
plot(theta_sp.Time, theta_sp.Data());
ylabel('Position [Rad]');
legend('mod. theta', 'SP');

subplot(313); hold('on');
plot(time,Phi(:,3));
plot(psi_sp.Time, psi_sp.Data);
ylabel('Position [Rad]');
xlabel('time [s]');
legend('mod. psi','SP');

figure(3); clf(3);
subplot(311); hold('on');
plot(time,DPhi(:,1));
title('Angular velocity')
ylabel('Velocity [Rad/s]');
legend('mod. Dphi');

subplot(312); hold('on');
plot(time,DPhi(:,2));
ylabel('Velocity [Rad/s]');
legend('mod. Dtheta');

subplot(313); hold('on');
plot(time,DPhi(:,3));
plot(Dpsi_SP.Time, Dpsi_SP.Data);
ylabel('Velocity [Rad/s]');
xlabel('time [s]');
legend('mod. Dpsi', 'SP');

figure(4); clf(4);
hold('on');
title('Modelled propellar power')
plot(time, u_prop);
ylabel('Power 0-1000');
legend('FL', 'FR', 'RL', 'RR');